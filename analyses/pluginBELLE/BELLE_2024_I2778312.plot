BEGIN PLOT /BELLE_2024_I2778312/d01-x01-y01
Title=Pion helicity angle in $B^-\to D^0\pi^-\pi^0$
XLabel=$\cos\theta_p$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_p$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2024_I2778312/d01-x01-y02
Title=$\pi^-\pi^0$ mass in $B^-\to D^0\pi^-\pi^0$
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
