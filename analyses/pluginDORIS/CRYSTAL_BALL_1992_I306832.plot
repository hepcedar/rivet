BEGIN PLOT /CRYSTAL_BALL_1992_I306832/d01-x01-y01
Title=Cross section for $e^+e^-\to\Upsilon(1S)\to\mu^+\mu^-$
XLabel=$\sqrt{s}$ [MeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1992_I306832/d02-x01-y01
Title=Cross section for $e^+e^-\to\Upsilon(2S)\to\mu^+\mu^-$
XLabel=$\sqrt{s}$ [MeV]
YLabel=$\sigma$ [pb]
LogY=0
ConnectGaps=1
END PLOT
