BEGIN PLOT /SND_2024_I2809929/d01-x01-y01
Title=$\sigma(e^+e^-\to n\bar{n})$
XLabel=$E_\mathrm{beam}$ [MeV]
YLabel=$\sigma(e^+e^-\to n\bar{n})$ [nb]
ConnectGaps=1
LogY=0
END PLOT
