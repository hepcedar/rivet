BEGIN PLOT /BESIII_2024_I2802333/d01-x01-y01
Title=Cross section for $e^+e^-\to K^-\bar\Xi^+\Lambda^0$
XLabel=$\sqrt{s}$  [GeV]
YLabel=$\sigma(e^+e^-\to K^-\bar\Xi^+\Lambda^0)$ [fb]
ConnectGaps=1
LogY=0
END PLOT

BEGIN PLOT /BESIII_2024_I2802333/d02-x01-y01
Title=Cross section for $e^+e^-\to K^-\bar\Xi^+\Sigma^0$
XLabel=$\sqrt{s}$  [GeV]
YLabel=$\sigma(e^+e^-\to  K^-\bar\Xi^+\Sigma^0)$ [fb]
ConnectGaps=1
LogY=0
END PLOT
