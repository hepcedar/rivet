BEGIN PLOT /BESIII_2022_I2099144
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2099144/d02-x01-y01
Title=$\alpha_\psi$ for $\psi(2S)\to \Xi^-\bar\Xi^+$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2022_I2099144/d02-x01-y02
Title=$\Delta\Phi$ for $\psi(2S)\to \Xi^-\bar\Xi^+$
YLabel=$\Delta\Phi$
END PLOT
BEGIN PLOT /BESIII_2022_I2099144/d02-x01-y03
Title=$\alpha_\Xi$ for $\Xi\to\Lambda\pi^-$
YLabel=$\alpha_\Xi$
YMax=0
YMin=-0.8
END PLOT
BEGIN PLOT /BESIII_2022_I2099144/d02-x01-y04
Title=$\bar{\alpha}_\Xi$ for $\bar\Xi\to\bar\Lambda\pi^+$
YLabel=$\bar{\alpha}_\Xi$
END PLOT

BEGIN PLOT /BESIII_2022_I2099144/T
LogY=0
XLabel=$\cos\theta_\Xi$
END PLOT

BEGIN PLOT /BESIII_2022_I2099144/cTheta
Title=Cross section vs polar angle
LogY=0
XLabel=$\cos\theta_\Xi$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}\cos\theta_\Xi$
END PLOT

BEGIN PLOT /BESIII_2022_I2099144/T1
Title=$T_1$ for $\Xi^-\to \Lambda\pi^-$
YLabel=$T_1$
END PLOT
BEGIN PLOT /BESIII_2022_I2099144/T2
Title=$T_2$ for $\Xi^-\to \Lambda\pi^-$
YLabel=$T_2$
END PLOT
BEGIN PLOT /BESIII_2022_I2099144/T3
Title=$T_3$ for $\Xi^-\to \Lambda\pi^-$
YLabel=$T_3$
END PLOT
BEGIN PLOT /BESIII_2022_I2099144/T4
Title=$T_4$ for $\Xi^-\to \Lambda\pi^-$
YLabel=$T_4$
END PLOT
BEGIN PLOT /BESIII_2022_I2099144/T5
Title=$T_5$ for $\Xi^-\to \Lambda\pi^-$
YLabel=$T_5$
END PLOT
