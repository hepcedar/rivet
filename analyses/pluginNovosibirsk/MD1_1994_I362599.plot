BEGIN PLOT /MD1_1994_I362599/d01-x01-y01
Title=Scaled momentum spectrum for $\Lambda,\bar{\Lambda}$ at the $\Upsilon(1S)$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}x$
XLabel=$x$
END PLOT
BEGIN PLOT /MD1_1994_I362599/d02-x01-y01
Title=$\Lambda^0,\bar{\Lambda}^0$ multiplicity in $\Upsilon(1S)$ decay
YLabel=$\langle N_{\Lambda^0,\bar{\Lambda}^0}\rangle$
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /MD1_1994_I362599/d03-x01
Title=$\Lambda^0,\bar{\Lambda}^0$ multiplicity in the continuum
YLabel=$\langle N_{\Lambda^0,\bar{\Lambda}^0}\rangle$
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /MD1_1994_I362599/d04-x01-y01
Title=$\Xi^-,\bar{\Xi}^+$ multiplicity in $\Upsilon(1S)$ decay
YLabel=$\langle N_{\Xi^-,\bar{\Xi}^+}\rangle$
LogY=0
ConnectGaps=1
END PLOT
