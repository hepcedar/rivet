#PARTICLE LEVEL, NORMALIZED VALUES

# BEGIN PLOT /CMS_2018_I1703993/d123-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^l(\mathrm{leading})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^l(\mathrm{leading})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d127-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^l(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^l(\mathrm{trailing})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d115-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^l\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^l}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d119-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^{\bar{l}}\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^{\bar{l}}}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d131-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_l$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\eta_l}$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d135-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{\bar{l}}$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\eta_{\bar{l}}}$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d139-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{l}\;(\mathrm{leading})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\eta_{l}\;(\mathrm{leading})}$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d143-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{l}\;(\mathrm{trailing})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\eta_{l}\;(\mathrm{trailing})}$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d147-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(l\bar{l})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}(l\bar{l})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d151-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\mathrm{M}(l\bar{l})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dM(l\bar{l})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d155-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\Delta\phi(l\bar{l})\;[\mathrm{Radian}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\Delta\phi(l\bar{l})}\;[\mathrm{Radian}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d159-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\Delta\eta(l\bar{l})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\Delta\eta(l\bar{l})}$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d187-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$N_{jets}$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dN_{jets}}$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d171-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{b}\;(\mathrm{leading})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\eta_{b}\;(\mathrm{leading})}$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d175-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{b}\;(\mathrm{trailing})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\eta_{b}\;(\mathrm{trailing})}$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d179-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(b\bar{b})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}(b\bar{b})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d183-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$M(b\bar{b})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dM(b\bar{b})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d15-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^{\bar{t}}\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^{\bar{t}}}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d23-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^t(\mathrm{leading})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{leading})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d31-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^t(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{trailing})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d55-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(\bar{t})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(\bar{t})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d63-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(t)(\mathrm{leading})$ 
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(t)(\mathrm{leading})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d71-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(t)(\mathrm{trailing})$ 
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(t)(\mathrm{trailing})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d103-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\Delta y(t,\bar{t})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\Delta y(t,\bar{t})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d39-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(\mathrm{t})(t\bar{t}\;\mathrm{RF}) [\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t})(t\bar{t}\;\mathrm{RF})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d163-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^b(\mathrm{leading})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^b(\mathrm{leading})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d167-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^b(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^b(\mathrm{trailing})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d07-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(\mathrm{t}) [\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d47-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(\mathrm{t})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(\mathrm{t})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d79-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(\mathrm{t}\bar{\mathrm{t}})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d87-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(\mathrm{t}\bar{\mathrm{t}})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(\mathrm{t}\bar{\mathrm{t}})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d95-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$M(\mathrm{t}\bar{\mathrm{t}})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dM(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d111-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\Delta\phi(\mathrm{t},\bar{\mathrm{t}})\;[\mathrm{Radian}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\,\Delta\phi(\mathrm{t},\bar{\mathrm{t}})}\;[\mathrm{Radian}^{-1}]$
#END PLOT


#PARTICLE LEVEL, ABSOLTE VALUES

# BEGIN PLOT /CMS_2018_I1703993/d121-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^l(\mathrm{leading})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^l(\mathrm{leading})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d125-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^l(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^l(\mathrm{trailing})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d113-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^l\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^l}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d117-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^{\bar{l}}\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^{\bar{l}}}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d129-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_l$
YLabel=$\frac{d\sigma}{d\eta_l}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d133-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{\bar{l}}$
YLabel=$\frac{d\sigma}{d\eta_{\bar{l}}}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d137-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{l}\;(\mathrm{leading})$
YLabel=$\frac{d\sigma}{d\eta_{l}\;(\mathrm{leading})}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d141-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{l}\;(\mathrm{trailing})$
YLabel=$\frac{d\sigma}{d\eta_{l}\;(\mathrm{trailing})}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d145-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(l\bar{l})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}(l\bar{l})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d149-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\mathrm{M}(l\bar{l})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dM(l\bar{l})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d153-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\Delta\phi(l\bar{l})$
YLabel=$\frac{d\sigma}{d\Delta\phi(l\bar{l})}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d157-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\Delta\eta(l\bar{l})$
YLabel=$\frac{d\sigma}{d\Delta\eta(l\bar{l})}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d185-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$N_{jets}$
YLabel=$\frac{d\sigma}{dN_{jets}}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d169-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{b}\;(\mathrm{leading})$
YLabel=$\frac{d\sigma}{d\eta_{b}\;(\mathrm{leading})}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d173-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\eta_{b}\;(\mathrm{trailing})$
YLabel=$\frac{d\sigma}{d\eta_{b}\;(\mathrm{trailing})}\;[\mathrm{pb}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d177-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(b\bar{b})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}(b\bar{b})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d181-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$M(b\bar{b})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dM(b\bar{b})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d13-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^{\bar{t}}\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^{\bar{t}}}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d21-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^t(\mathrm{leading})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{leading})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d29-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^t(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{trailing})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d27-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^t(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{trailing})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d53-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(\bar{t})$
YLabel=$\frac{d\sigma}{dy(\bar{t})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d61-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(t)(\mathrm{leading})$ 
YLabel=$\frac{d\sigma}{dy(t)(\mathrm{leading})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d69-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(t)(\mathrm{trailing})$ 
YLabel=$\frac{d\sigma}{dy(t)(\mathrm{trailing})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d101-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\Delta y(t,\bar{t})$
YLabel=$\frac{d\sigma}{d\Delta y(t,\bar{t})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d37-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(\mathrm{t})(t\bar{t}\;\mathrm{RF}) [\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t})(t\bar{t}\;\mathrm{RF})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d161-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^b(\mathrm{leading})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^b(\mathrm{leading})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d165-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}^b(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^b(\mathrm{trailing})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d05-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(\mathrm{t}) [\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d45-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(\mathrm{t})$
YLabel=$\frac{d\sigma}{dy(\mathrm{t})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d77-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$p_\mathrm{T}(\mathrm{t}\bar{\mathrm{t}})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d85-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$y(\mathrm{t}\bar{\mathrm{t}})$
YLabel=$\frac{d\sigma}{dy(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d93-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$M(\mathrm{t}\bar{\mathrm{t}})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dM(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d109-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, particle level
XLabel=$\Delta\phi(\mathrm{t},\bar{\mathrm{t}})$
YLabel=$\frac{d\sigma}{d\,\Delta\phi(\mathrm{t},\bar{\mathrm{t}})}\;[\mathrm{pb}]$
#END PLOT


#PARTON LEVEL, NORMALIZED VALUES
 
# BEGIN PLOT /CMS_2018_I1703993/d03-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}(\mathrm{t}) [\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d11-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}^{\bar{t}}\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^{\bar{t}}}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d19-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}^t(\mathrm{leading})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{leading})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d27-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}^t(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{trailing})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d35-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}(\mathrm{t})(t\bar{t}\;\mathrm{RF}) [\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t})(t\bar{t}\;\mathrm{RF})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d43-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(\mathrm{t})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(\mathrm{t})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d51-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(\bar{t})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(\bar{t})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d59-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(t)(\mathrm{leading})$ 
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(t)(\mathrm{leading})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d67-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(t)(\mathrm{trailing})$ 
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(t)(\mathrm{trailing})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d75-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}(\mathrm{t}\bar{\mathrm{t}})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{GeV}^{-1}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d83-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(\mathrm{t}\bar{\mathrm{t}})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dy(\mathrm{t}\bar{\mathrm{t}})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d91-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$M(\mathrm{t}\bar{\mathrm{t}})\;[\mathrm{GeV}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{dM(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{GeV}^{-1}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d99-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$\Delta y(t,\bar{t})$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\Delta y(t,\bar{t})}$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d107-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$\Delta\phi(\mathrm{t},\bar{\mathrm{t}})\;[\mathrm{Radian}]$
YLabel=$\frac{1}{\sigma}\frac{d\sigma}{d\,\Delta\phi(\mathrm{t},\bar{\mathrm{t}})}\;[\mathrm{Radian}^{-1}]$
#END PLOT


#PARTON LEVEL, ABSOLUTE VALUES

# BEGIN PLOT /CMS_2018_I1703993/d01-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}(\mathrm{t}) [\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d09-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}^{\bar{t}}\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^{\bar{t}}}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d17-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}^t(\mathrm{leading})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{leading})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d25-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}^t(\mathrm{trailing})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}^t(\mathrm{trailing})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d33-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}(\mathrm{t})(t\bar{t}\;\mathrm{RF}) [\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t})(t\bar{t}\;\mathrm{RF})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d41-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(\mathrm{t})$
YLabel=$\frac{d\sigma}{dy(\mathrm{t})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d49-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(\bar{t})$
YLabel=$\frac{d\sigma}{dy(\bar{t})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d57-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(t)(\mathrm{leading})$ 
YLabel=$\frac{d\sigma}{dy(t)(\mathrm{leading})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d65-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(t)(\mathrm{trailing})$ 
YLabel=$\frac{d\sigma}{dy(t)(\mathrm{trailing})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d73-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$p_\mathrm{T}(\mathrm{t}\bar{\mathrm{t}})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dp_\mathrm{T}(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{pb}/\mathrm{GeV}]$
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d81-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$y(\mathrm{t}\bar{\mathrm{t}})$
YLabel=$\frac{d\sigma}{dy(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d89-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$M(\mathrm{t}\bar{\mathrm{t}})\;[\mathrm{GeV}]$
YLabel=$\frac{d\sigma}{dM(\mathrm{t}\bar{\mathrm{t}})}\;[\mathrm{pb}/\mathrm{GeV}]$
LogY=1
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d97-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$\Delta y(t,\bar{t})$
YLabel=$\frac{d\sigma}{d\Delta y(t,\bar{t})}\;[\mathrm{pb}]$
LogY=0
#END PLOT

# BEGIN PLOT /CMS_2018_I1703993/d105-x01-y01
Title=CMS, 13 TeV, $t\bar{t}$ dilepton, parton level
XLabel=$\Delta\phi(\mathrm{t},\bar{\mathrm{t}})$
YLabel=$\frac{d\sigma}{d\,\Delta\phi(\mathrm{t},\bar{\mathrm{t}})}\;[\mathrm{pb}]$
#END PLOT