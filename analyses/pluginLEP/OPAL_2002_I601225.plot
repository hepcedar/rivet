# BEGIN PLOT /OPAL_2002_I601225/d01-x01-y01
Title=Charged multiplicity as a function of energy in $b$ events
XLabel=$E_\mathrm{CMS}/GeV$
YLabel=$\langle n\rangle_{b\bar b}$
FullRange=1
ConnectGaps=1
XMin=128
XMax=210
LogY=0
# END PLOT
# BEGIN PLOT /OPAL_2002_I601225/d01-x01-y02
Title=Charged multiplicity as a function of energy in $c$ events
XLabel=$E_\mathrm{CMS}/GeV$
YLabel=$\langle n\rangle_{c\bar c}$
FullRange=1
LogY=0
ConnectGaps=1
XMin=128
XMax=210
# END PLOT
# BEGIN PLOT /OPAL_2002_I601225/d01-x01-y03
Title=Charged multiplicity as a function of energy in $uds$ events
XLabel=$E_\mathrm{CMS}/GeV$
YLabel=$\langle n\rangle_{l\bar l}$
FullRange=1
LogY=0
ConnectGaps=1
XMin=128
XMax=210
# END PLOT
# BEGIN PLOT /OPAL_2002_I601225/d01-x01-y04
Title=Difference in Charged multiplicity as a function of energy between $b$ and $uds$ events
XLabel=$E_\mathrm{CMS}/GeV$
YLabel=$\delta_{bl}$
FullRange=1
LogY=0
ConnectGaps=1
XMin=128
XMax=210
# END PLOT
