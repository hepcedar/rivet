BEGIN PLOT /BESIII_2020_I1784442
Title=Cross section for $e^+e^-\to\eta J/\psi$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\eta J/\psi)$ [pb]
LogY=0
ConnectGaps=1
END PLOT
