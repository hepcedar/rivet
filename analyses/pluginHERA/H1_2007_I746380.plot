# BEGIN PLOT /H1_2007_I746380/d01-x01-y01
Title=DIS
YLabel=$\mathrm{d}\sigma / \mathrm{d} z_{\mathbb{P}}$
XLabel=$z_{\mathbb{P}}$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d02-x01-y01
Title=DIS
YLabel=$\mathrm{d}\sigma / \mathrm{d} \mathrm{log}_{10}x_{\mathbb{P}}$
XLabel=$\mathrm{log}_{10}x_{\mathbb{P}}$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d03-x01-y01
Title=DIS
YLabel=$\mathrm{d}\sigma / \mathrm{d} W$
XLabel=$W$ [GeV]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d04-x01-y01
Title=DIS
YLabel=$\mathrm{d}\sigma / \mathrm{d} Q^2$
XLabel=$Q^2$ [GeV$^2$]
LogY=0
LogX=1
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d05-x01-y01
Title=DIS
YLabel=$\mathrm{d}\sigma / \mathrm{d} E_T^{*\,\mathrm{jet\,1}}$
XLabel=$E_T^{*\,\mathrm{jet\,1}}$ [GeV]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d06-x01-y01
Title=DIS
YLabel=$\mathrm{d}\sigma / \mathrm{d}\left<\eta_{\mathrm{jet}}^{\mathrm{lab}}\right>$
XLabel=$\left<\eta_{\mathrm{jet}}^{\mathrm{lab}}\right>$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d07-x01-y01
Title=DIS
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\Delta\eta_{\mathrm{jet}}^{*}|$
XLabel=$|\Delta\eta_{\mathrm{jet}}^{*}|$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d08-x01-y01
Title=Photoproduction
YLabel=$\mathrm{d}\sigma / \mathrm{d} z_{\mathbb{P}}$
XLabel=$z_{\mathbb{P}}$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d09-x01-y01
Title=Photoproduction
YLabel=$\mathrm{d}\sigma / \mathrm{d} x_{\gamma}$
XLabel=$x_{\gamma}$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d10-x01-y01
Title=Photoproduction
YLabel=$\mathrm{d}\sigma / \mathrm{d} \mathrm{log}_{10}x_{\mathbb{P}}$
XLabel=$\mathrm{log}_{10}x_{\mathbb{P}}$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d11-x01-y01
Title=Photoproduction
YLabel=$\mathrm{d}\sigma / \mathrm{d} W$
XLabel=$W$ [GeV]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d12-x01-y01
Title=Photoproduction
YLabel=$\mathrm{d}\sigma / \mathrm{d} E_T^{*\,\mathrm{jet\,1}}$
XLabel=$E_T^{*\,\mathrm{jet\,1}}$ [GeV]
LogY=1
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d13-x01-y01
Title=Photoproduction
YLabel=$\mathrm{d}\sigma / \mathrm{d}<\eta_{\mathrm{jet}}^{\mathrm{lab}}>$
XLabel=$\left<\eta_{\mathrm{jet}}^{\mathrm{lab}}\right>$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d14-x01-y01
Title=Photoproduction
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\Delta\eta_{\mathrm{jet}}^{*}|$
XLabel=$|\Delta\eta_{\mathrm{jet}}^{*}|$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2007_I746380/d15-x01-y01
Title=Photoproduction
YLabel=$\mathrm{d}\sigma / \mathrm{d}M_{12}$
XLabel=$M_{12}$ [GeV$^2$]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT
