# BEGIN PLOT /UA5_1982_I176647/d02-x01-y01
Title=Mean charged multiplicity for $pp$ collisions at $\sqrt{s} = 53~\mathrm{GeV}$
XLabel=$\sqrt{s}$ / GeV
YLabel=$\langle N_\mathrm{ch} \rangle$
# END PLOT

# BEGIN PLOT /UA5_1982_I176647/d02-x01-y02
Title=Mean charged multiplicity for $p\bar{p}$ collisions at $\sqrt{s} = 53~\mathrm{GeV}$
XLabel=$\sqrt{s}$ / GeV
YLabel=$\langle N_\mathrm{ch} \rangle$
# END PLOT

# BEGIN PLOT /UA5_1982_I176647/d03-x01-y01
Title=Pseudorapidity for $pp$ collisions at $\sqrt{s} = 53~\mathrm{GeV}$
XLabel=$\eta$
YLabel=$\mathrm{d}N_\mathrm{ch}/\mathrm{d}\eta$
# END PLOT

# BEGIN PLOT /UA5_1982_I176647/d04-x01-y01
Title=Pseudorapidity for $p\bar{p}$ collisions at $\sqrt{s} = 53~\mathrm{GeV}$
XLabel=$\eta$
YLabel=$\mathrm{d}N_\mathrm{ch}/\mathrm{d}\eta$
# END PLOT
