Name: BESIII_2017_I1505590
Year: 2017
Summary: $\chi_{c2}$ decays to $K^+K^-\pi^0$, $K^0_SK^\pm\pi^\mp$ and $\pi^+\pi^-\pi^0$
Experiment: BESIII
Collider: BEPC
InspireID: 1505590
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 96 (2017) 11, 111102
RunInfo: Any process producing chi_c2
Description:
  'Mass distributions in $\chi_{c2}$ decays to $K^+K^-\pi^0$, $K^0_SK^\pm\pi^\mp$ and $\pi^+\pi^-\pi^0$. The data was read from the plots in the paper and is not corrected for efficiency and acceptance.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2016dda
BibTeX: '@article{BESIII:2016dda,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Observation of the helicity-selection-rule suppressed decay of the $\chi_{c2}$ charmonium state}",
    eprint = "1612.07398",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.96.111102",
    journal = "Phys. Rev. D",
    volume = "96",
    number = "11",
    pages = "111102",
    year = "2017"
}
'
