# BEGIN PLOT /LHCF_2020_I1783943/d01-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV in $\eta>10.75$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d02-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV in $10.06<\eta<10.75$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d03-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV in $9.65<\eta<10.06$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d04-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV in $8.99<\eta<9.21$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d05-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV in $8.80<\eta<8.99$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d06-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV in $8.65<\eta<8.80$
XLabel=Energy [GeV]
YLabel=d$\sigma/$d$E$ [mb$/$GeV]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d07-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV
XLabel=$\eta$
YLabel=dE/d$\eta$ [GeV]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d08-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV
XLabel=$\eta$
YLabel=d$\sigma$/d$\eta$ [mb]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d09-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV
XLabel=k
YLabel=$\sigma$ [mb]
LogY=0
# END PLOT

# BEGIN PLOT /LHCF_2020_I1783943/d10-x01-y01
Title=pp $\to$ nX (or $\mathrm{\bar{n}X}$) at $\sqrt{s}=13\,$TeV
XLabel=
YLabel=$<1-k>$
LogY=0
YMin=0.3
YMax=0.7
# END PLOT
