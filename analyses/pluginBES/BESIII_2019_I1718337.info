Name: BESIII_2019_I1718337
Year: 2019
Summary:  Cross section for $e^+e^-\to$ proton and antiproton between 2. and 3.8 GeV using ISR
Experiment: BESIII
Collider: BEPC II
InspireID: 1718337
Status:   VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:1902.00665
RunInfo: e+ e- to hadrons
NeedCrossSection: yes
Beams: [e+, e-]
Energies: [2.000, 2.025, 2.05, 2.075, 2.1, 2.125, 2.15, 2.175, 2.2,
           2.225, 2.25, 2.275, 2.3, 2.35, 2.4, 2.45, 2.5, 2.55, 2.6,
           2.65, 2.7, 2.75, 2.8, 2.85, 2.9, 2.95, 3.0, 3.2, 3.4, 3.6, 3.8]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $e^+e^-\to p\bar{p}$ for energies between 2. and 3.8 GeV using initial-state radiation.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords: []
BibKey: Ablikim:2019njl
BibTeX: '@article{Ablikim:2019njl,
      author         = "Ablikim, M. and others",
      title          = "{Study of $e^+ e^- \to p \bar p$ via initial state
                        radiation at BESIII}",
      collaboration  = "BESIII",
      year           = "2019",
      eprint         = "1902.00665",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      SLACcitation   = "%%CITATION = ARXIV:1902.00665;%%"
}'
