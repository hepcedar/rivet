BEGIN PLOT /BESIII_2017_I1621266/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $J/\psi\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1621266/d01-x01-y02
Title=$\pi^+\eta^\prime$ mass distribution in $J/\psi\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^+\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2017_I1621266/dalitz_Jpsi
Title=Dalitz plot for $J/\psi\to \pi^+\pi^-\eta^\prime$
XLabel=$m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^-\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\eta^\prime}/{\rm d}m^2_{\pi^-\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2017_I1621266/d01-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $\psi(2S)\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2017_I1621266/d01-x01-y04
Title=$\pi^+\eta^\prime$ mass distribution in $\psi(2S)\to \pi^+\pi^-\eta^\prime$
XLabel=$m_{\pi^+\eta^\prime}$ [GeV]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2017_I1621266/dalitz_psi2S
Title=Dalitz plot for $\psi(2S)\to \pi^+\pi^-\eta^\prime$
XLabel=$m^2_{\pi^+\eta^\prime}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^-\eta^\prime}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\eta^\prime}/{\rm d}m^2_{\pi^-\eta^\prime}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
