BEGIN PLOT /LHCB_2023_I2633007/d01
Title=Differential branching ratio w.r.t $q^2$ for $\Lambda_b^0\to \Lambda(1520)^0\mu^+\mu^-$
XLabel=$q^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\mathcal{B}/\text{d}q^2\times 10^8$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
