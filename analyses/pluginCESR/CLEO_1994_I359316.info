Name: CLEO_1994_I359316
Year: 1994
Summary: $e^+e^-\to e^+e^-\chi_{c2}$ via intermediate photons at 10.58 GeV
Experiment: CLEO
Collider: CESR
InspireID: 359316
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 50 (1994) 4265-4271, 1994.
RunInfo:  e+ e- > e+e- chi_c2 via photon photon -> chi_c2
Beams: [e+, e-]
Energies: [10.58]
Description:
  'Measurement of the cross section for the production of $\chi_{c2}$ in photon-photon
   collisions, i.e. $e^+e^-\to \gamma\gamma e^+e^-$ followed by $\gamma\gamma\to\chi_{c2}$,
   by the CLEO experiment at 10.58 GeV.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CLEO:1993icq
BibTeX: '@article{CLEO:1993icq,
    author = "Shelkov, V. and others",
    collaboration = "CLEO",
    title = "{Measurement of two photon production of the chi(c2)}",
    reportNumber = "CLNS-93-1250",
    doi = "10.1103/PhysRevD.50.4265",
    journal = "Phys. Rev. D",
    volume = "50",
    pages = "4265--4271",
    year = "1994"
}
'
