# BEGIN PLOT /E735_1998_I480349/d01-x01-y01
Title=Charged multiplicity at $\sqrt{s} = 1800~\mathrm{GeV}$
XLabel=$N_\mathrm{ch}$
YLabel=$\mathrm{Probability}~(N_\mathrm{ch})$
# END PLOT
