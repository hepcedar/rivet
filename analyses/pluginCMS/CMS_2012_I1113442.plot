BEGIN PLOT /CMS_2012_I1113442/d01-x01-y01
YLabel=$\sigma(\Lambda_b^0)\times\text{Br}(\Lambda^0_b\to J/\psi\Lambda^0)$ [nb]
Title=Cross Section for $\Lambda_b^0$ ($p_\perp>10$\,GeV, $|y|<2$)
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I1113442/d01-x01-y02
YLabel=$\sigma(\Lambda_b^0)/\sigma(\bar{\Lambda}_b^0)$
Title=$\Lambda_b^0$ vs $\bar{\Lambda}_b^0$ ratio ($p_\perp>10$\,GeV, $|y|<2$)
LogY=0
END PLOT

BEGIN PLOT /CMS_2012_I1113442/d02-x01-y01
YLabel=$\text{d}\sigma(\Lambda_b^0)/\text{d}p_\perp\times\text{Br}(\Lambda^0_b\to J/\psi\Lambda^0)$ [pb/GeV]
XLabel=$p_\perp$ [GeV]
Title=Cross Section for $\Lambda_b^0$ ($p_\perp>10$\,GeV, $|y|<2$)
END PLOT
BEGIN PLOT /CMS_2012_I1113442/d02-x01-y02
YLabel=$\sigma(\Lambda_b^0)/\sigma(\bar{\Lambda}_b^0)$
XLabel=$p_\perp$ [GeV]
Title=$\Lambda_b^0$ vs $\bar{\Lambda}_b^0$ ratio ($p_\perp>10$\,GeV, $|y|<2$)
LogY=0
END PLOT

BEGIN PLOT /CMS_2012_I1113442/d03-x01-y01
YLabel=$\text{d}\sigma(\Lambda_b^0)/\text{d}y\times\text{Br}(\Lambda^0_b\to J/\psi\Lambda^0)$ [pb]
XLabel=$y$
Title=Cross Section for $\Lambda_b^0$ ($p_\perp>10$\,GeV, $|y|<2$)
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I1113442/d03-x01-y02
YLabel=$\sigma(\Lambda_b^0)/\sigma(\bar{\Lambda}_b^0)$
XLabel=$y$ [GeV]
Title=$\Lambda_b^0$ vs $\bar{\Lambda}_b^0$ ratio ($y>10$\,GeV, $|y|<2$)
LogY=0
END PLOT
