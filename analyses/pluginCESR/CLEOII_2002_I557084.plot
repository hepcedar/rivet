BEGIN PLOT /CLEOII_2002_I557084/d01-x01-y01
Title=$D^{*0}\pi^+$ mass in  $\bar{B}^0\to D^{*0}\pi^+\pi^+\pi^-\pi^-$
XLabel=$m_{D^{*0}\pi^+}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D^{*0}\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2002_I557084/d01-x01-y02
Title=$D^{*0}\pi^-$ mass in  $\bar{B}^0\to D^{*0}\pi^+\pi^+\pi^-\pi^-$
XLabel=$m_{D^{*0}\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D^{*0}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2002_I557084/d02-x01-y01
Title=$\pi^+\pi^-\pi^-$ mass in  $\bar{B}^0\to D^{*0}\pi^+\pi^+\pi^-\pi^-$ ($2.3<m_{D^{*0}\pi^+}2.6\,$GeV)
XLabel=$m_{\pi^+\pi^-\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2002_I557084/d03-x01-y01
Title=Differential branching ratio for $\bar{B}^0\to D^{*0}2\pi^+2\pi^-$
XLabel=$m^2_{2\pi^+2\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\mathcal{B}(B\to D^*\ell\nu)\mathrm{d}\mathcal{B}/\mathrm{d}m^2_{2\pi^+2\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2002_I557084/d03-x01-y02
Title=Differential branching ratio for $\bar{B}^0\to D^{*+}\pi^+2\pi^-\pi^0$
XLabel=$m^2_{\pi^+2\pi^-\pi^0}$ [$\mathrm{GeV}^2$]
YLabel=$1/\mathcal{B}(B\to D^*\ell\nu)\mathrm{d}\mathcal{B}/\mathrm{d}m^2_{\pi^+2\pi^-\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
