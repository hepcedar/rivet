BEGIN PLOT /ATLAS_2013_I1204994/d01-x01-y01
LogY=0
YLabel = Br($\Upsilon \rightarrow \mu^{+}\mu^{-}$) $\times \sigma$ [nb]
Title=Total cross section for $p_\perp<70$\,GeV and $|y|<2.3$
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d02
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ ($0<y<0.75$, fiducial) 
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ ($1.2<y<2.25$, fiducial) 
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d03
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ ($0<y<0.75$, fiducial) 
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ ($1.2<y<2.25$, fiducial) 
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d04
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ ($0<y<0.75$, fiducial) 
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d04-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ ($1.2<y<2.25$, fiducial) 
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d05-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(1S)$ (fiducial)
XLabel = $|y|$
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d05-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(2S)$ (fiducial)
XLabel = $|y|$
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d05-x01-y03
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(3S)$ (fiducial)
XLabel = $|y|$
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d06
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d06-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ ($0<y<0.75$, inclusive)) 
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d06-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ ($1.2<y<2.25$, inclusive)) 
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d07
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d07-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ ($0<y<0.75$, inclusive)) 
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d07-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ ($1.2<y<2.25$, inclusive)) 
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d08
XLabel=$p_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [fb/GeV]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d08-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ ($0<y<0.75$, inclusive)) 
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d08-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ ($1.2<y<2.25$, inclusive)) 
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d09-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(1S)$
XLabel = $|y|$
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d09-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(2S)$
XLabel = $|y|$
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d09-x01-y03
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(3S)$
XLabel = $|y|$
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d10
XLabel=$p_\perp$ [GeV]
YLabel=$\Upsilon(2S)/\Upsilon(1S)$
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d10-x01-y01
Title=Ratio for $\Upsilon(2S)$ to $\Upsilon(1S)$ ($0<y<0.75$, inclusive)) 
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d10-x01-y02
Title=Ratio for $\Upsilon(2S)$ to $\Upsilon(1S)$ ($1.2<y<2.25$, inclusive)) 
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d11
XLabel=$p_\perp$ [GeV]
YLabel=$\Upsilon(3S)/\Upsilon(1S)$
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d11-x01-y01
Title=Ratio for $\Upsilon(3S)$ to $\Upsilon(1S)$ ($0<y<0.75$, inclusive)) 
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d11-x01-y02
Title=Ratio for $\Upsilon(3S)$ to $\Upsilon(1S)$ ($1.2<y<2.25$, inclusive)) 
END PLOT

BEGIN PLOT /ATLAS_2013_I1204994/d12-x01-y01
Title=Ratio for $\Upsilon(2S)$ to $\Upsilon(1S)$
XLabel = $|y|$
YLabel=$\Upsilon(2S)/\Upsilon(1S)$
END PLOT
BEGIN PLOT /ATLAS_2013_I1204994/d12-x01-y02
Title=Ratio for $\Upsilon(3S)$ to $\Upsilon(1S)$
XLabel = $|y|$
YLabel=$\Upsilon(3S)/\Upsilon(1S)$
END PLOT
