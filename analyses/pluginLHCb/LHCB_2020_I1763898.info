Name: LHCB_2020_I1763898
Year: 2020
Summary: Prompt and non-prompt $\eta_c$ production at 13 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1763898
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Eur.Phys.J.C 80 (2020) 3, 191
RunInfo: hadronic events with eta_c and J/psi production
Beams: [p+, p+]
Energies: [13000]
Description:
  'Measurement of the differential cross section with respect to $p_\perp$ of $\eta_c$ production at 13 TeV by the LHCb collaboration.'
ValidationInfo:
  'Herwig 7 event'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2019zaj
BibTeX: '@article{LHCb:2019zaj,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Measurement of the $\eta_c(1S)$ production cross-section in $pp$ collisions at $\sqrt{s} = 13$ TeV}",
    eprint = "1911.03326",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "LHCb-PAPER-2019-024, CERN-EP-2019-214",
    doi = "10.1140/epjc/s10052-020-7733-0",
    journal = "Eur. Phys. J. C",
    volume = "80",
    number = "3",
    pages = "191",
    year = "2020"
}
'
