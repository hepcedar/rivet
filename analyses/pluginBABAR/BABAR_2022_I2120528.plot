BEGIN PLOT /BABAR_2022_I2120528
LogY=0
ConnectGaps=1
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
END PLOT

BEGIN PLOT /BABAR_2022_I2120528/d01-x01-y01
Title=Cross Section for $e^+e^-\to K^+K^-3\pi^0$
END PLOT
BEGIN PLOT /BABAR_2022_I2120528/d02-x01-y01
Title=Cross Section for $e^+e^-\to K^0_SK^\pm\pi^\mp2\pi^0$
END PLOT1
BEGIN PLOT /BABAR_2022_I2120528/d03-x01-y01
Title=Cross Section for $e^+e^-\to K^0_SK^\pm\pi^\mp\pi^+\pi^-$
END PLOT
BEGIN PLOT /BABAR_2022_I2120528/d04-x01-y01
Title=Cross Section for $e^+e^-\to K^+K^-\eta$
END PLOT
BEGIN PLOT /BABAR_2022_I2120528/d05-x01-y01
Title=Cross Section for $e^+e^-\to \phi\eta$
END PLOT
BEGIN PLOT /BABAR_2022_I2120528/d06-x01-y01
Title=Cross Section for $e^+e^-\to K^{*+}K^{*-}\pi^0$
END PLOT
BEGIN PLOT /BABAR_2022_I2120528/d07-x01-y01
Title=Cross Section for $e^+e^-\to K^0_SK^{*0}\pi^0\pi^0$
END PLOT
BEGIN PLOT /BABAR_2022_I2120528/d08-x01-y01
Title=Cross Section for $e^+e^-\to K^{*\pm}K^\mp2\pi^0$
END PLOT
BEGIN PLOT /BABAR_2022_I2120528/d09-x01-y01
Title=Cross Section for $e^+e^-\to K^0_SK^\pm\rho^\mp\pi^0$
END PLOT
BEGIN PLOT /BABAR_2022_I2120528/d10-x01-y01
Title=Cross Section for $e^+e^-\to f_1(1285)\pi^+\pi^-$
END PLOT
