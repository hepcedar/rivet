BEGIN PLOT /ARGUS_1988_I262713
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1988_I262713/d01-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*+}K^{*-}$
END PLOT
BEGIN PLOT /ARGUS_1988_I262713/d02-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S\pi^+\pi^-$
END PLOT
BEGIN PLOT /ARGUS_1988_I262713/d03-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^+\pi^-\pi^0$ + c.c.
END PLOT
BEGIN PLOT /ARGUS_1988_I262713/d04-x01-y01
Title=Cross section for $\gamma\gamma\to K^0_SK^+\pi^-\pi^0$ + c.c. (non-resonant)
END PLOT
