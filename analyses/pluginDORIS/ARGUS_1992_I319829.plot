BEGIN PLOT /ARGUS_1992_I319829/d01-x01-y01
Title=Cross Section for $D^{\pm}_s$ times $\mathcal{B}(D_s^+\to\phi\pi^+)$ (continuum)
YLabel=$\sigma(D^\pm)\times\mathcal{B}(D_s^+\to\phi\pi^+)$ [pb]
LogY=0
END PLOT

BEGIN PLOT /ARGUS_1992_I319829/d02-x01-y01
Title=Scaled momentum spectrum for $D^+_s$ production (continuum)
YLabel=$1/N\mathrm{d}N/\mathrm{d}x_p$
XLabel=$x_p$
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1992_I319829/d03-x01-y01
Title=Scaled momentum spectrum for $D^+_s$ production ($\Upsilon(4S)$)
YLabel=$1/N\mathrm{d}N/\mathrm{d}x_p$
XLabel=$x_p$
LogY=0
END PLOT
