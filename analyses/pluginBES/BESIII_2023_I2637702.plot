BEGIN PLOT /BESIII_2023_I2637702/
XLabel=$\sqrt{s}$ [GeV]
LogY=0
ConnectGaps=1
END PLOT

BEGIN PLOT /BESIII_2023_I2637702/d0[1,2]-x01-y01
Title=$\eta$ parameter for $e^+e^-\to\Lambda^0\bar\Lambda^0$
YLabel=$\eta$
END PLOT
BEGIN PLOT /BESIII_2023_I2637702/d0[1,2]-x01-y02
Title=Phase for $e^+e^-\to\Lambda^0\bar\Lambda^0$
YLabel=$\Delta\Phi$
END PLOT
BEGIN PLOT /BESIII_2023_I2637702/d0[1,2]-x01-y03
Title=$R_\psi$ parameter for $e^+e^-\to\Lambda^0\bar\Lambda^0$
YLabel=$R_\psi$
END PLOT
