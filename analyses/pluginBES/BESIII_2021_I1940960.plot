BEGIN PLOT /BESIII_2021_I1940960/d01-x01-y01
Title=$\sigma(e^+e^-\to \Sigma^0\bar{\Sigma}^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \Sigma^0\bar{\Sigma}^0)$/pb
ConnectGaps=1
LogY=0
END PLOT
