BEGIN PLOT /ARGUS_1992_I339268/d01-x01-y01
Title=$3\pi$ mas distribution in $\tau^-\to\pi^-\pi^-\pi^+\nu_\tau$
XLabel=$m_{3\pi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{3\pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1992_I339268/d03-x01-y01
LogY=0
END PLOT
