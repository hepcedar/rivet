# BEGIN PLOT /ATLAS_2023_I2648096/*
XTwosidedTicks=1
YTwosidedTicks=1
LogY=1
Title=
RatioPlotYMax=1.02
RatioPlotYMin=0.98
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d06
XLabel=Lepton $p_\mathrm{T}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d42
XLabel=Lepton $p_\mathrm{T}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d09
LogY=0
YMax=20000
XLabel=Lepton $|\eta|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\eta|$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d45
LogY=0
XLabel=Lepton $|\eta|$
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d}|\eta|$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d21
XLabel=Dilepton $p_\mathrm{T}^{e\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}^{e\mu}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d57
XLabel=Dilepton $p_\mathrm{T}^{e\mu}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} p_\mathrm{T}^{e\mu}$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d15
XLabel=Dilepton $m^{e\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m^{e\mu}$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d51
XLabel=Dilepton $m^{e\mu}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} m^{e\mu}$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d27
XLabel=Dilepton $|y^{e\mu}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d} |y^{e\mu}|$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d63
XLabel=Dilepton $|y^{e\mu}|$
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} |y^{e\mu}|$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d24
XLabel=Dilepton $\Delta \phi^{e\mu}$ [rad]
YLabel=$\mathrm{d}\sigma / \mathrm{d} \Delta \phi^{e\mu}$ [fb/rad]
LogY=0
YMax=6000
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d60
XLabel=Dilepton $\Delta \phi^{e\mu}$ [rad]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} \Delta \phi^{e\mu}$ [1/rad]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d18
XLabel=Dilepton $p_\mathrm{T}^{e} + p_\mathrm{T}^{\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} ( p_\mathrm{T}^{e} + p_\mathrm{T}^{\mu} )$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d56
XLabel=Dilepton $p_\mathrm{T}^{e} + p_\mathrm{T}^{\mu}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} ( p_\mathrm{T}^{e} + p_\mathrm{T}^{\mu} )$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d12
XLabel=Dilepton $E^{e} + E^{\mu}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} ( E^{e} + E^{\mu} )$ [fb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d48
XLabel=Dilepton $E^{e} + E^{\mu}$ [GeV]
YLabel=$1 / \sigma \; \mathrm{d}\sigma / \mathrm{d} ( E^{e} + E^{\mu} )$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d39
LogY=1
XLabel=Lepton $\Delta \phi^{e\mu} \times E^{e} + E^{\mu}$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}\Delta \phi^{e\mu} \mathrm{d} ( E^{e} + E^{\mu} )$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d75
LogY=1
XLabel=Lepton $\Delta \phi^{e\mu} \times E^{e} + E^{\mu}$
YLabel=$1 / \sigma \; \mathrm{d}^2\sigma / \mathrm{d}\Delta \phi^{e\mu} \mathrm{d} ( E^{e} + E^{\mu} )$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d30
LogY=1
XLabel=Dilepton $|y^{e\mu}|\times m^{e\mu}$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}|y^{e\mu}|\mathrm{d}m^{e\mu}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d66
LogY=1
XLabel=Dilepton $|y^{e\mu}|\times m^{e\mu}$
YLabel=$1 / \sigma \; \mathrm{d}^2\sigma / \mathrm{d}|y^{e\mu}|\mathrm{d}m^{e\mu}$
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d33
LogY=1
XLabel=Dilepton $\Delta \phi^{e\mu}\times m^{e\mu}$
YLabel=$\mathrm{d}^2\sigma / \mathrm{d}\Delta \phi^{e\mu}\mathrm{d}m^{e\mu}$ [fb]
# END PLOT

# BEGIN PLOT /ATLAS_2023_I2648096/d69
LogY=1
XLabel=Dilepton $\Delta \phi^{e\mu}\times m^{e\mu}$
YLabel=$1 / \sigma \; \mathrm{d}^2\sigma / \mathrm{d}\Delta \phi{e\mu}\mathrm{d}m^{e\mu}$
# END PLOT
