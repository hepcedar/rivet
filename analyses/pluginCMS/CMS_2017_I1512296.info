Name: CMS_2017_I1512296
Year: 2017
Summary: Prompt and non-prompt J$/\psi$ production at 5.02 TeV
Experiment: CMS
Collider: LHC
InspireID: 1512296
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Eur.Phys.J.C 77 (2017) 4, 269
 - arXiv:1702.01462 [nucl-ex]
 - CMS-HIN-14-009
RunInfo: hadronic events with J/psi production
Beams: [p+, p+]
Energies: [5020]
Description:
  'Measurement of the double differential cross section for prompt and non-prompt J$/\psi$ production at 5.02 TeV by the CMS collaboration. Only the proton-proton results are implemented.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: CMS:2017exb
BibTeX: '@article{CMS:2017exb,
    author = "Sirunyan, Albert M and others",
    collaboration = "CMS",
    title = "{Measurement of prompt and nonprompt $\mathrm{J}/{\psi }$ production in $\mathrm {p}\mathrm {p}$ and $\mathrm {p}\mathrm {Pb}$ collisions at $\sqrt{s_{\mathrm {NN}}} =5.02\,\text {TeV} $}",
    eprint = "1702.01462",
    archivePrefix = "arXiv",
    primaryClass = "nucl-ex",
    reportNumber = "CMS-HIN-14-009, CERN-EP-2017-009",
    doi = "10.1140/epjc/s10052-017-4828-3",
    journal = "Eur. Phys. J. C",
    volume = "77",
    number = "4",
    pages = "269",
    year = "2017"
}
'
