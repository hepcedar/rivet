BEGIN PLOT /BELLE_2023_I2660525
LogY=0
END PLOT

BEGIN PLOT /BELLE_2023_I2660525/d01-x01-y01
YLabel=$\mathcal{B}(B\to D^\pm_sX)$ [$\%$]
Title=Branching ratio for $B\to D^\pm_sX$
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d01-x01-y02
YLabel=$\mathcal{B}(B\to D^0/\bar{D}^0X)$ [$\%$]
Title=Branching ratio for $B\to D^0/\bar{D}^0X$
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d01-x01-y03
YLabel=$\mathcal{B}(\Upsilon(5S)\to D^\pm_sX)$ [$\%$]
Title=Branching ratio for $\Upsilon(5S)\to D^\pm_sX$
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d01-x01-y04
YLabel=$\mathcal{B}(\Upsilon(5S)\to D^0/\bar{D}^0X)$ [$\%$]
Title=Branching ratio for $\Upsilon(5S)\to D^0/\bar{D}^0X$
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d01-x01-y05
YLabel=$f_s$ [$\%$]
Title=$B_s0$ production fraction in $\Upsilon(5S)$ decays
END PLOT

BEGIN PLOT /BELLE_2023_I2660525/d02
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d02-x01-y01
Title=Cross section for $e^+e^-\to b\bar{b} \to D_s^\pm X$
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d02-x01-y02
Title=Cross section for $e^+e^-\to b\bar{b} \to D^0/\bar{D}^0 X$
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d02-x01-y03
Title=Cross section for $e^+e^-\to B\bar{B}X$
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d02-x01-y04
Title=Cross section for $e^+e^-\to B^0_S\bar{B}^0_SX$ times $\mathcal{B}(B_S^0\to D_s^\pm X)$
END PLOT

BEGIN PLOT /BELLE_2023_I2660525/d03
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_p$
XLabel=$x_p$
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d03-x01-y01
Title=$D_s^\pm$ scaled momentum spectrum in $\Upsilon(5S)$ decay
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d03-x01-y02
Title=$D^0$ scaled momentum spectrum in $\Upsilon(5S)$ decay
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d03-x01-y03
Title=$D_s^\pm$ scaled momentum spectrum in $\Upsilon(4S)$ decay
END PLOT
BEGIN PLOT /BELLE_2023_I2660525/d03-x01-y04
Title=$D^0$ scaled momentum spectrum in $\Upsilon(4S)$ decay
END PLOT
