BEGIN PLOT /BELLE_2005_I677625/
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d01-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to p\bar{p})$ [nb]
LogY=1
END PLOT

BEGIN PLOT /BELLE_2005_I677625/d02-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.075<\sqrt{s}<2.5$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d02-x01-y02
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.5<\sqrt{s}<3.0$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d02-x01-y03
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $3.0<\sqrt{s}<4.0$ GeV
END PLOT

BEGIN PLOT /BELLE_2005_I677625/d03-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.075<\sqrt{s}<2.1$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d03-x01-y02
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.1<\sqrt{s}<2.2$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d03-x01-y03
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.2<\sqrt{s}<2.3$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d04-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.3<\sqrt{s}<2.4$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d04-x01-y02
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.4<\sqrt{s}<2.5$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d04-x01-y03
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.5<\sqrt{s}<2.6$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d05-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.6<\sqrt{s}<2.7$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d05-x01-y02
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.7<\sqrt{s}<2.8$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d05-x01-y03
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $2.8<\sqrt{s}<2.9$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d06-x01-y01
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $3.1<\sqrt{s}<3.5$ GeV
END PLOT
BEGIN PLOT /BELLE_2005_I677625/d06-x01-y02
Title=Cross section for $\gamma\gamma\to p\bar{p}$ with $3.5<\sqrt{s}<4.0$ GeV
END PLOT
