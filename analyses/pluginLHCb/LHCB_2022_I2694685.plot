# BEGIN PLOT /LHCB_2022_I2694685/d0(1|2)-x01-y0[1-9][0-9]?
XLabel=$p_\mathrm{T}$ [$\mathrm{GeV}/c$]
YLabel=$\mathrm{d}^2\sigma/(\mathrm{d}p_\mathrm{T}\,\mathrm{d}y^\ast)$ [$\mathrm{mb}/(\mathrm{GeV}/c)$]
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y01
Title=Prompt $D^0$, $1.50 < y^\ast < 1.75$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y02
Title=Prompt $D^0$, $1.75 < y^\ast < 2.00$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y03
Title=Prompt $D^0$, $2.00 < y^\ast < 2.25$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y04
Title=Prompt $D^0$, $2.25 < y^\ast < 2.50$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y05
Title=Prompt $D^0$, $2.50 < y^\ast < 2.75$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y06
Title=Prompt $D^0$, $2.75 < y^\ast < 3.00$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y07
Title=Prompt $D^0$, $3.00 < y^\ast < 3.25$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y08
Title=Prompt $D^0$, $3.25 < y^\ast < 3.50$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y09
Title=Prompt $D^0$, $3.50 < y^\ast < 3.75$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d01-x01-y10
Title=Prompt $D^0$, $3.75 < y^\ast < 4.00$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y01
Title=Prompt $D^0$, $-2.75 < y^\ast < -2.50$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y02
Title=Prompt $D^0$, $-3.00 < y^\ast < -2.75$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y03
Title=Prompt $D^0$, $-3.25 < y^\ast < -3.00$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y04
Title=Prompt $D^0$, $-3.50 < y^\ast < -3.25$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y05
Title=Prompt $D^0$, $-3.75 < y^\ast < -3.50$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y06
Title=Prompt $D^0$, $-4.00 < y^\ast < -3.75$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y07
Title=Prompt $D^0$, $-4.25 < y^\ast < -4.00$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y08
Title=Prompt $D^0$, $-4.50 < y^\ast < -4.25$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y09
Title=Prompt $D^0$, $-4.75 < y^\ast < -4.50$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d02-x01-y10
Title=Prompt $D^0$, $-5.00 < y^\ast < -4.75$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d0(3|4)-x01-y0[1-5]
XLabel=$p_\mathrm{T}$ [$\mathrm{GeV}/c$]
YLabel=$R_{p\mathrm{Pb}}$
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d03-x01-y01
Title=Prompt $D^0$, $2.5 < y^\ast < 4.0$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d03-x01-y02
Title=Prompt $D^0$, $2.0 < y^\ast < 2.5$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d03-x01-y03
Title=Prompt $D^0$, $2.5 < y^\ast < 3.0$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d03-x01-y04
Title=Prompt $D^0$, $3.0 < y^\ast < 3.5$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d03-x01-y05
Title=Prompt $D^0$, $3.5 < y^\ast < 4.0$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d04-x01-y01
Title=Prompt $D^0$, $-4.0 < |y^\ast| < -2.5$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d04-x01-y02
Title=Prompt $D^0$, $-4.5 < |y^\ast| < -4.0$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d04-x01-y03
Title=Prompt $D^0$, $-4.0 < |y^\ast| < -3.5$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d04-x01-y04
Title=Prompt $D^0$, $-3.5 < |y^\ast| < -3.0$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d04-x01-y05
Title=Prompt $D^0$, $-3.0 < |y^\ast| < -2.5$
# END PLOT

# BEGIN PLOT /LHCB_2022_I2694685/d05-x01-y01
Title=Prompt $D^0$, $2.5 < |y^\ast| < 4.0$
XLabel=$p_\mathrm{T}$ [$\mathrm{GeV}/c$]
YLabel=$R_\mathrm{FB}$
LogY=0
# END PLOT
