# BEGIN PLOT /LHCB_2021_I1913240/*
XLabel=$p_\mathrm{T}$ [$\mathrm{GeV}/c$]
YLabel=$\mathrm{d}^2\sigma/(\mathrm{d}\eta\,\mathrm{d}p_\mathrm{T})$ [$\mathrm{mb}/(\mathrm{GeV}/c)$]
LogX=1
XMax=10.0
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d01-x01-y01
Title=$pp$, prompt charged particles, $2.0 < \eta < 2.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d01-x01-y02
Title=$pp$, prompt charged particles, $2.5 < \eta < 3.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d01-x01-y03
Title=$pp$, prompt charged particles, $3.0 < \eta < 3.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d01-x01-y04
Title=$pp$, prompt charged particles, $3.5 < \eta < 4.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d01-x01-y05
Title=$pp$, prompt charged particles, $4.0 < \eta < 4.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d01-x01-y06
Title=$pp$, prompt charged particles, $4.5 < \eta < 4.8$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d02-x01-y01
Title=$p\mathrm{Pb}$, prompt charged particles, $1.6 < \eta < 2.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d02-x01-y02
Title=$p\mathrm{Pb}$, prompt charged particles, $2.0 < \eta < 2.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d02-x01-y03
Title=$p\mathrm{Pb}$, prompt charged particles, $2.5 < \eta < 3.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d02-x01-y04
Title=$p\mathrm{Pb}$, prompt charged particles, $3.0 < \eta < 3.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d02-x01-y05
Title=$p\mathrm{Pb}$, prompt charged particles, $3.5 < \eta < 4.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d02-x01-y06
Title=$p\mathrm{Pb}$, prompt charged particles, $4.0 < \eta < 4.3$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d03-x01-y01
Title=$p\mathrm{Pb}$, prompt charged particles, $-5.2 < \eta < -4.8$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d03-x01-y02
Title=$p\mathrm{Pb}$, prompt charged particles, $-4.8 < \eta < -4.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d03-x01-y03
Title=$p\mathrm{Pb}$, prompt charged particles, $-4.5 < \eta < -4.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d03-x01-y04
Title=$p\mathrm{Pb}$, prompt charged particles, $-4.0 < \eta < -3.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d03-x01-y05
Title=$p\mathrm{Pb}$, prompt charged particles, $-3.5 < \eta < -3.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d03-x01-y06
Title=$p\mathrm{Pb}$, prompt charged particles, $-3.0 < \eta < -2.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d04-x01-y01
Title=$R_{p\mathrm{Pb}}$ in  $2.0 < \eta < 2.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d04-x01-y02
Title=$R_{p\mathrm{Pb}}$ in  $2.5 < \eta < 3.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d04-x01-y03
Title=$R_{p\mathrm{Pb}}$ in  $3.0 < \eta < 3.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d04-x01-y04
Title=$R_{p\mathrm{Pb}}$ in  $3.5 < \eta < 4.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d04-x01-y05
Title=$R_{p\mathrm{Pb}}$ in  $4.0 < \eta < 4.3$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d05-x01-y01
Title=$R_{p\mathrm{Pb}}$ in  $-4.8 < \eta < -4.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d05-x01-y02
Title=$R_{p\mathrm{Pb}}$ in  $-4.5 < \eta < -4.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d05-x01-y03
Title=$R_{p\mathrm{Pb}}$ in  $-4.0 < \eta < -3.5$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d05-x01-y04
Title=$R_{p\mathrm{Pb}}$ in  $-3.5 < \eta < -3.0$
# END PLOT

# BEGIN PLOT /LHCB_2021_I1913240/d05-x01-y05
Title=$R_{p\mathrm{Pb}}$ in  $-3.0 < \eta < -2.5$
# END PLOT

