# BEGIN PLOT /CDF_2009_I856131/d01-x01-y01
Title=Total XS for $66 < M_{ee}/\mathrm{GeV} < 116$
XLabel=$\sqrt{s}$
YLabel=$\sigma$ [pb]
LogY=0
YMin=150
YMax=350
XMin=1959.5
XMax=1960.5
# END PLOT

# BEGIN PLOT /CDF_2009_I856131/d02-x01-y01
Title=$e^+ e^-$ pair rapidity
XLabel=$|y_{e^+ e^-}|$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [pb]
LogY=0
# END PLOT
