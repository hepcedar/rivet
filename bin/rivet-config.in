#! /usr/bin/env bash
## -*- sh -*-
## @configure_input@

tmp=$(echo $* | grep -E -- '--\<help\>|-\<h\>')
if test $# -eq 0 || test -n "$tmp"; then
    echo "rivet-config: configuration tool for the Rivet generator validation system"
    echo "              http://projects.hepforge.org/rivet/"
    echo
    echo "Usage: $( basename $0 ) [--help|-h] | "
    echo "           [--{prefix,datadir,libdir,includedir}] | "
    echo "           [--{cppflags,ldflags,ldlibs}] | "
    echo "           [--version]"
    echo "Options:"
    echo "  --help | -h   : show this help message"
    echo
    echo "  --prefix      : show the installation prefix (cf. autoconf)"
    echo "  --includedir  : show the path to the directory containing the Rivet headers"
    echo "  --libdir      : show the path to the directory containing the Rivet libraries"
    echo "  --datadir     : show the path to the directory containing Rivet data"
    echo "  --pythonpath  : show the path(s) to the directory containing Rivet's Python package"
    echo
    echo "  --guess-prefix: try to use the runtime prefix, rather than that set at install-time"
    echo
    echo "  --cxx         : returns a compiler string matching that used to build Rivet"
    echo "  --cflags|--cppflags : returns a Rivet '-I' string for insertion into CPPFLAGS"
    echo "  --ldflags     : returns a Rivet '-L' string for insertion into LDFLAGS"
    echo "  --libs        : returns a Rivet '-L/-l' string for insertion into LIBS or LIBADD"
    echo
    echo "  --version     : returns Rivet release version number"
    if [[ $# -eq 0 ]]; then exit 1; else exit 0; fi
fi


## These variables need to exist
## Note no use of $DESTDIR... we ignore it so that destdir can be used
## for temp installs later copied to /
tmp=$( echo "$*" | grep -E -- '--\<guess-prefix\>')
if [[ -n "$tmp" ]]; then
    bindir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
    prefix=$(dirname $bindir)
    exec_prefix=$prefix
else
    prefix=@prefix@
    exec_prefix=@exec_prefix@
fi
datarootdir=@datarootdir@


OUT=""

## "Atomic" build info
tmp=$( echo "$*" | grep -E -- '--\<cxx\>')
test -n "$tmp" && OUT="$OUT @RIVETCXX@"

tmp=$( echo "$*" | grep -E -- '--\<prefix\>')
test -n "$tmp" && OUT="$OUT @prefix@"

tmp=$( echo "$*" | grep -E -- '--\<includedir\>')
test -n "$tmp" && OUT="$OUT @includedir@"

tmp=$( echo "$*" | grep -E -- '--\<libdir\>')
test -n "$tmp" && OUT="$OUT @libdir@"

tmp=$( echo "$*" | grep -E -- '--\<datadir\>')
test -n "$tmp" && OUT="$OUT @datadir@/@PACKAGE_TARNAME@"

tmp=$( echo "$*" | grep -E -- '--\<pythonpath|pydir\>')
test -n "$tmp" && OUT="$OUT @RIVET_PYTHONPATH@"


## "Pre-rolled" build info
tmp=$( echo "$*" | grep -E -- '--\<cflags\>|--\<cxxflags\>')
test -n "$tmp" && OUT="$OUT @RIVETCXXFLAGS@"

tmp=$( echo "$*" | grep -E -- '--\<cppflags\>')
test -n "$tmp" && OUT="$OUT @RIVETCPPFLAGS@"
if test -n "$tmp"; then
    irivet="-I@includedir@"
    test -n "$irivet" && OUT="$OUT ${irivet}"
    ihepmc="-I@HEPMCINCPATH@"
    test -n "$ihepmc" && OUT="$OUT ${ihepmc}"
    iyoda="@YODACPPFLAGS@"
    test -n "$iyoda" && OUT="$OUT ${iyoda}"
    ifastjet="@FASTJETCPPFLAGS@"
    test -n "$ifastjet" && OUT="$OUT ${ifastjet}"
    ifjcontrib="@FJCONTRIBCPPFLAGS@"
    test -n "$ifjcontrib" && OUT="$OUT ${ifjcontrib}"
    iyamlcpp="@YAML_CPP_CPPFLAGS@"
    test -n "$iyamlcpp" && OUT="$OUT ${iyamlcpp}"
fi

tmp=$( echo "$*" | grep -E -- '--\<ldflags\>|--\<libs\>')
if test -n "$tmp"; then
    lrivet="@libdir@"
    test -n "$lrivet" && OUT="$OUT -L${lrivet}"
    llibs="@RIVETLIBS@"
    test -n "$llibs" && OUT="$OUT ${llibs}"
    lldflags="@RIVETLDFLAGS@"
    test -n "$lldflags" && OUT="$OUT ${lldflags}"
    lhepmc="-L@HEPMCLIBPATH@ @HEPMCLIBNAME@"
    test -n "$lhepmc" && OUT="$OUT ${lhepmc}"
    lyoda="@YODALDFLAGS@ @YODALIBS@"
    test -n "$lyoda" && OUT="$OUT ${lyoda}"
    lyaml="@YAML_CPP_LDFLAGS@"
    test -n "$lyaml" && OUT="$OUT ${lyaml}"
    lfastjet="@FASTJETLIBADD@"
    test -n "$lfastjet" && OUT="$OUT ${lfastjet}"
fi

tmp=$( echo "$*" | grep -E -- '--\<libs\>|--\<ldadd\>')
test -n "$tmp" && OUT="$OUT -lRivet"

OUT=`echo $OUT | xargs -n1 | sort -u | xargs`


## Version
tmp=$( echo "$*" | grep -E -- '--\<version\>')
test -n "$tmp" && echo @PACKAGE_VERSION@ && exit 0

echo $OUT
