BEGIN PLOT /LHCB_2013_I1242869/d01-x01-y01
Title=Ratio of Prompt $\chi_{c2}$ to $\chi_{c1}$ as function of $p_\perp$ J/$\psi$ in $J/\psi\gamma$ decay 
XLabel=$p_\perp^{J/\psi}$ [GeV]
YLabel =$N(\chi_{c2})/N(\chi_{c1})$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2013_I1242869/d02-x01-y01
Title=Ratio of Prompt $\chi_{c}$ rates
YLabel=Ratio
LogY=0
END PLOT
