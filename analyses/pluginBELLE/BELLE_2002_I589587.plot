BEGIN PLOT /BELLE_2002_I589587/d01-x01-y01
Title=$\pi^\pm\pi^0$ mass in in $B^0\to\rho^\pm\pi^\mp$
XLabel=$m_{\pi^\pm\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^\pm\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I589587/d01-x01-y02
Title=$\pi^+\pi^-$ mass in in $B^+\to\rho^0\pi^+$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I589587/d02-x01-y01
Title=Helicity angle in $B^0\to\rho^\pm\pi^\mp$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I589587/d02-x01-y02
Title=Helicity angle in $B^+\to\rho^0\pi^+$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT