Name: BELLE_2006_I658082
Year: 2006
Summary: Mass distributions in $B^-\to\Lambda_c^+\bar{p}\pi^-$
Experiment: BELLE
Collider: KEKB
InspireID: 658082
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 97 (2006) 242001
Description:
'Measurement of mass distributions in $B^-\to\Lambda_c^+\bar{p}\pi^-$. The mass distributions were read from figure 2, which is not corrected,
 while the corrected helicity distribution was read from figure 3.'
ValidationInfo:
  'Herwig7 events using EvtGen for B decays'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Belle:2004dmq
BibTeX: '@article{Belle:2004dmq,
    author = "Gabyshev, N. and others",
    collaboration = "Belle",
    title = "{Study of decay mechanisms in B- ---\ensuremath{>} Lambda/c+ anti-p pi- decays and observation of low-mass structure in the Lambda/c+ anti-p system}",
    eprint = "hep-ex/0409005",
    archivePrefix = "arXiv",
    reportNumber = "BELLE-PREPRINT-2006-31, KEK-PREPRINT-2006-48",
    doi = "10.1103/PhysRevLett.97.242001",
    journal = "Phys. Rev. Lett.",
    volume = "97",
    pages = "242001",
    year = "2006"
}
'
