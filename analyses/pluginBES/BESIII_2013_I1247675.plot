BEGIN PLOT /BESIII_2013_I1247675/d01-x01-y01
Title=Cross section for $e^+e^-\to Z_c(4025)^\pm\pi^\mp\to(D^*D^*)^\pm\pi^\mp$
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1247675/d02-x01-y01
Title=Mass distribution for $D^{*0}D^{*-}$ at $\sqrt{s}=4.26$ GeV
XLabel=$m_{D^{*0}D^{*-}}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{D^{*0}D^{*-}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
