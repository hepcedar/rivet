BEGIN PLOT /ALICE_2021_I1898832/d01-x01-y01
Title=J/$\psi$ transverse momentum $|y| < 0.9$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /ALICE_2021_I1898832/d02-x01-y01
Title=J/$\psi$ rapidity $|y| < 0.9$
XLabel=$y^{J/\psi}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [$\mu$b]
LogY=0
END PLOT
BEGIN PLOT /ALICE_2021_I1898832/d03-x01-y01
Title=J/$\psi$ rapidity $|y| < 0.9$
XLabel=$y^{J/\psi}$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [$\mu$b]
LogY=0
END PLOT