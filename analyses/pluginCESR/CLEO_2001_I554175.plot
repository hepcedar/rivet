BEGIN PLOT /CLEO_2001_I554175/d01-x01-y01
Title=Differential branching ratio for $\bar{B}^0\to D^{*+}\pi^+\pi^-\pi^-\pi^0$
XLabel=$m_{\pi^+\pi^-\pi^-\pi^0}$ [GeV]
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}m_{\pi^+\pi^-\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I554175/d02-x01-y01
Title=$\omega\pi^-$ mass for $\bar{B}^0\to D^{*+}\omega\pi^-$
XLabel=$m_{\omega\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\omega\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I554175/d03-x01-y01
Title=$\omega\pi^-$ mass for $B^-\to D^{*0}\omega\pi^-$
XLabel=$m_{\omega\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\omega\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I554175/d04-x01-y01
Title=$D^*$ decay angle for $\bar{B}^0\to D^{*+}\omega\pi^-$
XLabel=$\cos\theta_{D^*}$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_{D^*}$
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I554175/d05-x01-y01
Title=$\omega$ decay plane angle for $\bar{B}^0\to D^{*+}\omega\pi^-$
XLabel=$\cos\theta_\omega$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_\omega$
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I554175/d06-x01-y01
Title=$\omega\pi^-$ mass for $\bar{B}\to D\omega\pi^-$
XLabel=$m_{\omega\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\omega\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /CLEO_2001_I554175/d07-x01-y01
Title=Decay angle for $\omega\pi^-$ for $B^-\to D\omega\pi^-$
XLabel=$\cos\theta_A$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_A$
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I554175/d07-x01-y02
Title=$\omega$ decay plane angle for $B^-\to D\omega\pi^-$
XLabel=$\cos\theta_\omega$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_\omega$
LogY=0
END PLOT
BEGIN PLOT /CLEO_2001_I554175/d07-x01-y03
Title=Angle between decay planes for $B^-\to D\omega\pi^-$
XLabel=$\chi$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$
LogY=0
END PLOT

BEGIN PLOT /CLEO_2001_I554175/d08-x01-y01
Title=$\omega\pi^-$ mass for $\bar{B}\to D^{(*)}\omega\pi^-$
XLabel=$m_{\omega\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\omega\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
