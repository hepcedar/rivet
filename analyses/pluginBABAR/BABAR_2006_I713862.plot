BEGIN PLOT /BABAR_2006_I713862/d01-x01-y01
Title=Differential branching ratio for $\bar{B}^0\to D^{*+}\omega\pi^-$
XLabel=$m^2_{\omega\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$1/\mathcal{B}(B\to D^*\ell\nu)\mathrm{d}\mathcal{B}/\mathrm{d}m^2_{\omega\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I713862/d02-x01-y01
Title=$D^{*+}\pi^-$ mass for $\bar{B}^0\to D^{*+}\omega\pi^-$
XLabel=$m_{D^{*+}\pi^-}$ [GeV]
YLabel=$\frac1/\Gamma\mathrm{d}\mathcal{\Gamma}/\mathrm{d}m_{D^{*+}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BABAR_2006_I713862/d03-x01-y01
Title=$D^{*+}$ longitudinal polarization for $\bar{B}^0\to D^{*+}\omega\pi^-$
XLabel=$m_X$ [GeV]
YLabel=$f_L$
LogY=0
END PLOT
