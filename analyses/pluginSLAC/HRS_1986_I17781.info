Name: HRS_1986_I17781
Year: 1986
Summary: Spectra for $\Lambda^0$ production at 29 GeV
Experiment: HRS
Collider: PEP
InspireID: 17781
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.Lett. 56 (1986) 1346
RunInfo: e+ e- to hadrons
Beams: [e+, e-]
Energies: [29]
Description:
  'Measurement of the $\Lambda^0$ and spectra at 29 GeV by the HRS experiment.
   In addition to the spectra the rapidity of the $\Lambda^0$ is measured with respect to the thrust axis.'
ValidationInfo:
  'Herwig 7 event'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: Baringer:1986jd
BibTeX: '@article{Baringer:1986jd,
    author = "Baringer, Philip S. and others",
    title = "{$\Lambda$ Production in $e^+ e^-$ Annihilations at 29-{GeV}: A Comparison With Lund Model Predictions}",
    reportNumber = "ANL-HEP-PR-85-121, IUHEE-75, UM-HE-85-28, PU-85-555",
    doi = "10.1103/PhysRevLett.56.1346",
    journal = "Phys. Rev. Lett.",
    volume = "56",
    pages = "1346",
    year = "1986"
}'
