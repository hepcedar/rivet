// -*- C++ -*-
#ifndef RIVET_RivetHDF5_HH
#define RIVET_RivetHDF5_HH
#include "Rivet/Config/RivetCommon.hh"
#include "Rivet/Tools/RivetPaths.hh"

#include "Rivet/Config/RivetConfig.hh"
#ifdef WITH_YODA_HIGHFIVE
#include "YODA/highfive/H5File.hpp"
#else
#include "highfive/H5File.hpp"
#define YODA_H5 HighFive
#endif

namespace Rivet {
  namespace H5 {


    using namespace YODA_H5;


    /// Read HDF5 file @a filename
    inline YODA_H5::File readFile(const string& filename) {
      const string filepath = findAnalysisDataFile(filename);
      if (filepath.empty()) throw IOError("Failed to load HDF5 file " + filename);
      try {
        return YODA_H5::File(filepath, YODA_H5::File::ReadOnly);
      } catch (...) {
        throw IOError("Failed to load HDF5 file " + filename);
      }
    }


    /// Read HDF5 data from dataset @a dsname in file @a filename, into the provided @a rtndata container
    template <typename T>
    inline bool readData(const string& filename, const string& dsname, T& rtndata) {
      try {
        YODA_H5::File h5file = readFile(filename);
        DataSet dataset = h5file.getDataSet(dsname);
        dataset.read(rtndata);
      } catch (...) {
        return false;
      }
      return true;
    }


    /// Read HDF5 data from dataset @a dsname in file @a filename, into a newly constructed container
    template <typename T>
    inline T readData(const string& filename, const string& dsname) {
      T rtn;
      readData(filename, dsname, rtn);
      return rtn;
    }


  }
}

#endif
