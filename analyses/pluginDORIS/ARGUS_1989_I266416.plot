BEGIN PLOT /ARGUS_1989_I266416
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1989_I266416/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-\pi^0\pi^0$
END PLOT
BEGIN PLOT /ARGUS_1989_I266416/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \rho^+\rho^-$
END PLOT
BEGIN PLOT /ARGUS_1989_I266416/d03-x01-y01
Title=Cross section for $\gamma\gamma\to \rho^\pm\pi^\mp\pi^0$
END PLOT
BEGIN PLOT /ARGUS_1989_I266416/d04-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-\pi^0\pi^0$ (non-resonant)
END PLOT
