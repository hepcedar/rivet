// -*- C++ -*-
#ifndef RIVET_Particle_FHH
#define RIVET_Particle_FHH

#include "Rivet/Tools/RivetSTL.hh"

namespace Rivet {


  /// @name Particle declarations
  /// @{

  // Forward declarations
  class Particle;
  class Particles;


  /// @name Particle function/functor declarations
  /// @{

  /// std::function instantiation for functors taking a Particle and returning a bool
  using ParticleSelector = function<bool(const Particle&)>;

  /// std::function instantiation for functors taking two Particles and returning a bool
  using ParticleSorter = function<bool(const Particle&, const Particle&)>;

  /// @}


  /// @name PdgId declarations
  /// @{

  /// Typedefs for a PDG ID code.
  typedef int PdgId;
  //typedef PdgId PID; //< can't do this, because it's also a (sub)namespace

  /// Typedef for a pair of particle names.
  typedef std::pair<PdgId, PdgId> PdgIdPair;

  /// @}


  /// Enum to enable different orderings for particles
  enum class ObjOrdering { ENERGY, ETA, ET };


}

#endif
