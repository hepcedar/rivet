BEGIN PLOT /BELLE_2022_I2512112/d01-x01-y01
Title=$\bar{D}^0\pi^-$ mass in $B^0\to \bar{D}^0\pi^- \ell^+\nu_\ell$
XLabel=$m_{\bar{D}^0\pi^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\bar{D}^0\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2512112/d01-x01-y02
Title=$D^-\pi^+$ mass in $B^0\to D^-\pi^+ \ell^+\nu_\ell$
XLabel=$m_{D^-\pi^+}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2022_I2512112/d02-x01-y01
Title=$\bar{D}^{*0}\pi^-$ mass in $B^0\to \bar{D}^{*0}\pi^- \ell^+\nu_\ell$
XLabel=$m_{\bar{D}^{*0}\pi^-}-m(\bar{D}^{*0})$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\bar{D}^{*0}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2512112/d02-x01-y02
Title=$D^{*-}\pi^+$ mass in $B^0\to D^{*-}\pi^+ \ell^+\nu_\ell$
XLabel=$m_{D^{*-}\pi^+}-m(\bar{D}^{*-})$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D^{*-}\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2022_I2512112/d03-x01-y01
Title=$D^-\pi^+\pi^-$ mass in $B^0\to D^-\pi^+\pi^- \ell^+\nu_\ell$
XLabel=$m_{D^-\pi^+\pi^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{D^-\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2512112/d03-x01-y02
Title=$\bar{D}^0\pi^+\pi^-$ mass in $B^0\to \bar{D}^0\pi^+\pi^- \ell^+\nu_\ell$
XLabel=$m_{\bar{D}^0\pi^+\pi^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\bar{D}^0\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
