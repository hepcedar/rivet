BEGIN PLOT /CLEOIII_2004_I627327/d01-x01-y01
Title=$\pi^+K^-$ mass distribution in $\Xi_c^0\to p K^-K^-\pi^+$
XLabel=$m_{\pi^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
