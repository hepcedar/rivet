BEGIN PLOT /BELLE_2016_I1408873
LogY=0
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d01-x01-y01
Title=Inclusive $\chi_{c1}$ spectrum in $B$ decays
XLabel=$p^*_{\chi_{c1}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p^*_{\chi_{c1}}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d01-x01-y02
Title=Inclusive $\chi_{c2}$ spectrum in $B$ decays
XLabel=$p^*_{\chi_{c2}}$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p^*_{\chi_{c2}}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BELLE_2016_I1408873/d02-x01-y01
Title=$K^+\pi^-$ mass spectrum in $B^0\to\chi_{c1}K^+\pi^-$
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d02-x02-y01
Title=$\chi_{c1}\pi^-$ mass spectrum in $B^0\to\chi_{c1}K^+\pi^-$
XLabel=$m_{\chi_{c1}\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c1}\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BELLE_2016_I1408873/d02-x01-y02
Title=$K^+\pi^-$ mass spectrum in $B^0\to\chi_{c2}K^+\pi^-$
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d02-x02-y02
Title=$\chi_{c2}\pi^-$ mass spectrum in $B^0\to\chi_{c2}K^+\pi^-$
XLabel=$m_{\chi_{c2}\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c2}\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BELLE_2016_I1408873/d02-x01-y03
Title=$K^0_S\pi^+$ mass spectrum in $B^+\to\chi_{c1}K^0_S\pi^+$
XLabel=$m_{K^0_S\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d02-x02-y03
Title=$\chi_{c1}\pi^+$ mass spectrum in $B^+\to\chi_{c1}K^0_S\pi^+$
XLabel=$m_{\chi_{c1}\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c1}\pi^+}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BELLE_2016_I1408873/d02-x01-y04
Title=$K^0_S\pi^+$ mass spectrum in $B^+\to\chi_{c2}K^0_S\pi^+$
XLabel=$m_{K^0_S\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^+}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d02-x02-y04
Title=$\chi_{c2}\pi^+$ mass spectrum in $B^+\to\chi_{c2}K^0_S\pi^+$
XLabel=$m_{\chi_{c2}\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c2}\pi^+}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BELLE_2016_I1408873/d02-x01-y05
Title=$K^+\pi^0$ mass spectrum in $B^+\to\chi_{c1}K^+\pi^0$
XLabel=$m_{K^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d02-x02-y05
Title=$\chi_{c1}\pi^0$ mass spectrum in $B^+\to\chi_{c1}K^+\pi^0$
XLabel=$m_{\chi_{c1}\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c1}\pi^0}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BELLE_2016_I1408873/d03-x01-y01
Title=$\chi_{c1}\pi^+\pi^-$ mass spectrum in $B^+\to\chi_{c1}K^+\pi^+\pi^+$
XLabel=$m_{\chi_{c1}\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c1}\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d03-x02-y01
Title=$\chi_{c1}\pi^\pm$ mass spectrum in $B^+\to\chi_{c1}K^+\pi^+\pi^+$
XLabel=$m_{\chi_{c1}\pi^\pm}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c1}\pi^\pm}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d03-x01-y02
Title=$\chi_{c2}\pi^+\pi^-$ mass spectrum in $B^+\to\chi_{c2}K^+\pi^+\pi^+$
XLabel=$m_{\chi_{c2}\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c2}\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d03-x02-y02
Title=$\chi_{c2}\pi^\pm$ mass spectrum in $B^+\to\chi_{c2}K^+\pi^+\pi^+$
XLabel=$m_{\chi_{c2}\pi^\pm}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\chi_{c2}\pi^\pm}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BELLE_2016_I1408873/d04-x01-y01
Title=$K^+\pi^+\pi^-$ mass spectrum in $B^+\to\chi_{c1}K^+\pi^+\pi^+$
XLabel=$m_{K^+\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d04-x02-y01
Title=$K^+\pi^-$ mass spectrum in $B^+\to\chi_{c1}K^+\pi^+\pi^+$
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d04-x03-y01
Title=$\pi^+\pi^-$ mass spectrum in $B^+\to\chi_{c1}K^+\pi^+\pi^+$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT

BEGIN PLOT /BELLE_2016_I1408873/d04-x01-y02
Title=$K^+\pi^+\pi^-$ mass spectrum in $B^+\to\chi_{c2}K^+\pi^+\pi^+$
XLabel=$m_{K^+\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d04-x02-y02
Title=$K^+\pi^-$ mass spectrum in $B^+\to\chi_{c2}K^+\pi^+\pi^+$
XLabel=$m_{K^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BELLE_2016_I1408873/d04-x03-y02
Title=$\pi^+\pi^-$ mass spectrum in $B^+\to\chi_{c2}K^+\pi^+\pi^+$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
END PLOT
