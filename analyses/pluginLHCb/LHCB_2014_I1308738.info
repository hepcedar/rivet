Name: LHCB_2014_I1308738
Year: 2014
Summary:  Fraction of $\Upsilon(1,2,3S)$ from $\chi_b(1,2,3P)$ decays at 7 and 8 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1308738
Status: VALIDATED
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Eur.Phys.J.C 74 (2014) 3092, 2014
RunInfo: chi_b(1,2,3P) and Upsilon(1,2,3S)
Beams: [p+, p+]
Energies: [7000,8000]
Options:
 - ENERGY=7000,8000
Description:
  'Measurement of the fraction of $\Upsilon(1,2,3S)$ from $\chi_b(1,2,3P)$ decays at 7 and 8 TeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2014ngh
BibTeX: '@article{LHCb:2014ngh,
    author = "Aaij, Roel and others",
    collaboration = "LHCb",
    title = "{Study of $\chi _{{\mathrm {b}}}$ meson production in $\mathrm {p} $ $\mathrm {p} $ collisions at $\sqrt{s}=7$ and $8{\mathrm {\,TeV}} $ and observation of the decay $\chi _{{\mathrm {b}}}\mathrm {(3P)} \rightarrow \Upsilon \mathrm {(3S)} {\gamma } $}",
    eprint = "1407.7734",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2014-178, LHCB-PAPER-2014-031",
    doi = "10.1140/epjc/s10052-014-3092-z",
    journal = "Eur. Phys. J. C",
    volume = "74",
    number = "10",
    pages = "3092",
    year = "2014"
}
'
