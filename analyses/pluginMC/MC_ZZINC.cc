// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/DileptonFinder.hh"
#include "Rivet/Projections/VetoedFinalState.hh"

namespace Rivet {


  /// @brief MC validation analysis for Z[ee]Z[mumu] events
  class MC_ZZINC : public Analysis {
  public:

    /// Default constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(MC_ZZINC);


    /// @name Analysis methods
    /// @{

    /// Book histograms
    void init() {
      // set FS cuts from input options
      const double etaecut = getOption<double>("ABSETAEMAX", 3.5);
      const double ptecut = getOption<double>("PTEMIN", 25.);
      Cut cut_e = Cuts::abseta < etaecut && Cuts::pT > ptecut*GeV;
      DileptonFinder zeefinder(91.2*GeV, 0.2, cut_e && Cuts::abspid == PID::ELECTRON,
                               Cuts::massIn(65*GeV, 115*GeV));
      declare(zeefinder, "ZeeFinder");

      VetoedFinalState zmminput;
      zmminput.addVetoOnThisFinalState(zeefinder);

      // set FS cuts from input options
      const double etamucut = getOption<double>("ABSETAMUMAX", 3.5);
      const double ptmucut = getOption<double>("PTMUMIN", 25.);
      Cut cut_mu = Cuts::abseta < etamucut && Cuts::pT > ptmucut*GeV;
      DileptonFinder zmmfinder(PromptFinalState(zmminput), 91.2*GeV, 0.2, cut_mu &&
                               Cuts::abspid == PID::MUON, Cuts::massIn(65*GeV, 115*GeV));
      declare(zmmfinder, "ZmmFinder");

      // Properties of the pair momentum
      double sqrts = sqrtS()>0. ? sqrtS() : 14000.;
      book(_h_ZZ_pT ,"ZZ_pT", logspace(100, 1.0, 0.5*sqrts/GeV));
      book(_h_ZZ_pT_peak ,"ZZ_pT_peak", 25, 0.0, 25.0);
      book(_h_ZZ_eta ,"ZZ_eta", 40, -7.0, 7.0);
      book(_h_ZZ_phi ,"ZZ_phi", 25, 0.0, TWOPI);
      book(_h_ZZ_m ,"ZZ_m", logspace(100, 150.0, 180.0 + 0.25*sqrts/GeV));

      // Correlations between the ZZ
      book(_h_ZZ_dphi ,"ZZ_dphi", 25, 0.0, PI);  /// @todo non-linear?
      book(_h_ZZ_deta ,"ZZ_deta", 25, -7.0, 7.0);
      book(_h_ZZ_dR ,"ZZ_dR", 25, 0.5, 7.0);
      book(_h_ZZ_dpT ,"ZZ_dpT", logspace(100, 1.0, 0.5*sqrts/GeV));
      book(_h_ZZ_costheta_planes ,"ZZ_costheta_planes", 25, -1.0, 1.0);

      // Properties of the Z bosons
      book(_h_Z_pT ,"Z_pT", logspace(100, 10.0, 0.25*sqrts/GeV));
      book(_h_Z_eta ,"Z_eta", 70, -7.0, 7.0);

      // Properties of the leptons
      book(_h_Zl_pT ,"Zl_pT", logspace(100, 30.0, 0.1*sqrts/GeV));
      book(_h_Zl_eta ,"Zl_eta", 40, -3.5, 3.5);

      // Correlations between the opposite charge leptons
      book(_h_ZeZm_dphi ,"ZeZm_dphi", 25, 0.0, PI);
      book(_h_ZeZm_deta ,"ZeZm_deta", 25, -5.0, 5.0);
      book(_h_ZeZm_dR ,"ZeZm_dR", 25, 0.5, 5.0);
      book(_h_ZeZm_m ,"ZeZm_m", 100, 0.0, 300.0);

    }



    /// Do the analysis
    void analyze(const Event& e) {
      const DileptonFinder& zeefinder = apply<DileptonFinder>(e, "ZeeFinder");
      if (zeefinder.bosons().size() != 1) vetoEvent;
      const DileptonFinder& zmmfinder = apply<DileptonFinder>(e, "ZmmFinder");
      if (zmmfinder.bosons().size() != 1) vetoEvent;

      // Z momenta
      const FourMomentum& zee = zeefinder.bosons()[0].momentum();
      const FourMomentum& zmm = zmmfinder.bosons()[0].momentum();
      const FourMomentum zz = zee + zmm;
      // Lepton momenta
      const FourMomentum& ep = zeefinder.constituents()[0].momentum();
      const FourMomentum& em = zeefinder.constituents()[1].momentum();
      const FourMomentum& mp = zmmfinder.constituents()[0].momentum();
      const FourMomentum& mm = zmmfinder.constituents()[1].momentum();

      _h_ZZ_pT->fill(zz.pT()/GeV);
      _h_ZZ_pT_peak->fill(zz.pT()/GeV);
      _h_ZZ_eta->fill(zz.eta());
      _h_ZZ_phi->fill(zz.phi());
      if (zz.mass2() > 0.0) ///< @todo Protection still needed?
        _h_ZZ_m->fill(zz.mass()/GeV);

      _h_ZZ_dphi->fill(deltaPhi(zee, zmm));
      _h_ZZ_deta->fill(zee.eta()-zmm.eta());
      _h_ZZ_dR->fill(deltaR(zee,zmm));
      _h_ZZ_dpT->fill(fabs(zee.pT()-zmm.pT()));

      const Vector3 crossZee = ep.p3().cross(em.p3());
      const Vector3 crossZmm = mp.p3().cross(mm.p3());
      const double costheta = crossZee.dot(crossZmm)/crossZee.mod()/crossZmm.mod();
      _h_ZZ_costheta_planes->fill(costheta);

      _h_Z_pT->fill(zee.pT()/GeV);
      _h_Z_pT->fill(zmm.pT()/GeV);
      _h_Z_eta->fill(zee.eta());
      _h_Z_eta->fill(zmm.eta());

      _h_Zl_pT->fill(ep.pT()/GeV);
      _h_Zl_pT->fill(em.pT()/GeV);
      _h_Zl_pT->fill(mp.pT()/GeV);
      _h_Zl_pT->fill(mm.pT()/GeV);
      _h_Zl_eta->fill(ep.eta());
      _h_Zl_eta->fill(em.eta());
      _h_Zl_eta->fill(mp.eta());
      _h_Zl_eta->fill(mm.eta());

      _h_ZeZm_dphi->fill(deltaPhi(ep, mm));
      _h_ZeZm_deta->fill(ep.eta()-mm.eta());
      _h_ZeZm_dR->fill(deltaR(ep, mm));
      const FourMomentum epmm = ep + mm;
      const double m_epmm = (epmm.mass2() > 0) ? epmm.mass() : 0; ///< @todo Protection still needed?
      _h_ZeZm_m->fill(m_epmm/GeV);
    }


    /// Finalize
    void finalize() {
      const double s = crossSection()/picobarn/sumOfWeights();
      scale(_h_ZZ_pT, s);
      scale(_h_ZZ_pT_peak, s);
      scale(_h_ZZ_eta, s);
      scale(_h_ZZ_phi, s);
      scale(_h_ZZ_m, s);
      scale(_h_ZZ_dphi, s);
      scale(_h_ZZ_deta, s);
      scale(_h_ZZ_dR, s);
      scale(_h_ZZ_dpT, s);
      scale(_h_ZZ_costheta_planes, s);
      scale(_h_Z_pT, s);
      scale(_h_Z_eta, s);
      scale(_h_Zl_pT, s);
      scale(_h_Zl_eta, s);
      scale(_h_ZeZm_dphi, s);
      scale(_h_ZeZm_deta, s);
      scale(_h_ZeZm_dR, s);
      scale(_h_ZeZm_m, s);
    }

    /// @}


  private:

    /// @name Histograms
    /// @{
    Histo1DPtr _h_ZZ_pT;
    Histo1DPtr _h_ZZ_pT_peak;
    Histo1DPtr _h_ZZ_eta;
    Histo1DPtr _h_ZZ_phi;
    Histo1DPtr _h_ZZ_m;
    Histo1DPtr _h_ZZ_dphi;
    Histo1DPtr _h_ZZ_deta;
    Histo1DPtr _h_ZZ_dR;
    Histo1DPtr _h_ZZ_dpT;
    Histo1DPtr _h_ZZ_costheta_planes;
    Histo1DPtr _h_Z_pT;
    Histo1DPtr _h_Z_eta;
    Histo1DPtr _h_Zl_pT;
    Histo1DPtr _h_Zl_eta;
    Histo1DPtr _h_ZeZm_dphi;
    Histo1DPtr _h_ZeZm_deta;
    Histo1DPtr _h_ZeZm_dR;
    Histo1DPtr _h_ZeZm_m;
    /// @}

  };



  RIVET_DECLARE_PLUGIN(MC_ZZINC);

}
