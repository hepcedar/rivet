BEGIN PLOT /BESIII_2019_I1692688
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{e^+e^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1692688/d01-x01-y01
Title=$e^+e^-$ mass distribution in $J/\psi\to\eta^\prime e^+e^-$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1692688/d01-x01-y02
Title=$e^+e^-$ mass distribution in $J/\psi\to\eta^\prime e^+e^-$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
LogY=0
END PLOT