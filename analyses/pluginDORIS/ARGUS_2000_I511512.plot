BEGIN PLOT /ARGUS_2000_I511512
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d03-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}$ (5 partial wave)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d04-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}$ (3 partial wave)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d05-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}$ (1 partial wave)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d09-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}$ (using $K_S^0K_S^0\pi^0\pi^0$)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d06-x01-y02
Title=Cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}$ (using $K_S^0K^+\pi^0\pi^0$+c.c.)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d10-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}$ (combined)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d06-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*+}K^{*-}$ (using $K_S^0K^+\pi^-\pi^0$+c.c.)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d08-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*+}K^{*-}$ (using $K_S^0K_S^0\pi^+\pi^-$+c.c.)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d09-x01-y02
Title=Cross section for $\gamma\gamma\to K^{*+}K^{*-}$ (using $K^+K^-\pi^0\pi^0$)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d10-x01-y02
Title=Cross section for $\gamma\gamma\to K^{*+}K^{*-}$ (combined)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d03-x01-y03
Title=Cross section for $\gamma\gamma\to \rho\phi$ (5 partial wave)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d04-x01-y03
Title=Cross section for $\gamma\gamma\to \rho\phi$ (3 partial wave)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d05-x01-y03
Title=Cross section for $\gamma\gamma\to \rho\phi$ (1 partial wave)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d03-x01-y02
Title=Cross section for $\gamma\gamma\to K^{*0}K^-\pi^+$ + c.c. (5 partial wave)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d04-x01-y02
Title=Cross section for $\gamma\gamma\to K^{*0}K^-\pi^+$ + c.c. (3 partial wave)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d05-x01-y02
Title=Cross section for $\gamma\gamma\to K^{*0}K^-\pi^+$ + c.c. (1 partial wave)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d07-x01-y01
Title=Cross section for $\gamma\gamma\to K^{*+}K^0_S\pi^-$ + c.c. (using $K_S^0K^+\pi^-\pi^0$)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d08-x01-y02
Title=Cross section for $\gamma\gamma\to K^{*+}K^0_S\pi^-$ + c.c. (using $K_S^0K_S^0\pi^+\pi^-$)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d07-x01-y02
Title=Cross section for $\gamma\gamma\to K^{*+}K^-\pi^0$ + c.c. (using $K_S^0K^+\pi^-\pi^0$+c.c.)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d07-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^+\pi^-\pi^0$ + c.c. (using $K_S^0K^+\pi^-\pi^0$+c.c.)
END PLOT
BEGIN PLOT /ARGUS_2000_I511512/d08-x01-y03
Title=Cross section for $\gamma\gamma\to K^0_SK^0_S\pi^+\pi^-$ (using $K_S^0K_S^0\pi^+\pi^-$+c.c.)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d03-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-\pi^+\pi^-$ (5 partial wave)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d04-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-\pi^+\pi^-$ (3 partial wave)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d05-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-\pi^+\pi^-$ (1 partial wave)
END PLOT

BEGIN PLOT /ARGUS_2000_I511512/d11-x01-y01
Title=Cross section ratio $\gamma\gamma\to K^{*+}K^{*-}$ over $\gamma\gamma\to K^{*0}\bar{K}^{*0}$
YLabel=$\sigma(\gamma\gamma\to K^{*+}K^{*-})/\sigma(\gamma\gamma\to K^{*0}\bar{K}^{*0})$
END PLOT
