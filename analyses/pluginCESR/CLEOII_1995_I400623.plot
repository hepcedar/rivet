BEGIN PLOT /CLEOII_1995_I400623/d01-x01-y01
Title=Spectral function for $\tau\to\pi^-\pi^-\pi^+\pi^0\nu_\tau$
XLabel=$q$ [GeV]
YLabel=$v_1(q)$
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1995_I400623/d01-x01-y02
Title=Spectral function for $\tau\to\omega\pi^-\nu_\tau$
XLabel=$q$ [GeV]
YLabel=$v_1(q)$
LogY=0
END PLOT
