BEGIN PLOT /CMS_2022_I2080534/d01-x01-y01
Title=CMS, 13 TeV, EWK WW
XLabel=Cross section
XCustomMajorTicks=1	parton	2	fiducial
YLabel=$\sigma^\mathrm{fid}$ [fb/GeV]
END PLOT
