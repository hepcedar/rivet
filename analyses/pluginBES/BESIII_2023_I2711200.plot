BEGIN PLOT /BESIII_2023_I2711200/d01-x01-y01
Title=$\sigma(e^+e^-\to \eta\pi^+\pi^-)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \eta\pi^+\pi^-)$/pb
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2711200/d02-x01-y01
Title=$\sigma(e^+e^-\to \eta\rho^0)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \eta\rho^0)$/pb
LogY=0
ConnectGaps=1
END PLOT
BEGIN PLOT /BESIII_2023_I2711200/d03-x01-y01
Title=$\sigma(e^+e^-\to a_2\pi)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to a_2\pi)$/pb
LogY=0
ConnectGaps=1
END PLOT
