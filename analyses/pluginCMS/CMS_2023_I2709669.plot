BEGIN PLOT /CMS_2023_I2709669/d01-x01-y01
Title=CMS, 13 TeV, WW$\gamma$, $e\mu$ channel
XLabel=Fiducial cross section
YLabel=$\sigma ^{fid}$ [fb/GeV]
END PLOT
