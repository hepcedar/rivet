BEGIN PLOT /BESIII_2019_I1711896/d01-x01-y01
Title=$\Lambda^0\pi^+$ mass distribution in $\Lambda_c^+\to\Lambda^0\eta\pi^+$
XLabel=$m_{\Lambda^0\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda^0\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1711896/dalitz
Title=Dalitz plot for   $\Lambda_c^+\to\Lambda^0\eta\pi^+$
XLabel=$m^2_{\Lambda^0\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\Lambda^0\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\Lambda^0\pi^+}/{\rm d}m^2_{\Lambda^0\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
