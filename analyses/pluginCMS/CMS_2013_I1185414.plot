BEGIN PLOT /CMS_2013_I1185414/
YMin=-1.
YMax=1.
LogY=0
XLabel=$p_\perp$ [GeV]
END PLOT

BEGIN PLOT /CMS_2013_I1185414/d01
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d02
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d03
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d04
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d05
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d06
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d07
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d08
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d09
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d10
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d11
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d12
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d13
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d14
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d15
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d16
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d17
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d18
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d19
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d20
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d21
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d22
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d23
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\Upsilon(1S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d24
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\Upsilon(1S)$ ($0.6<|y|<1.2$)
END PLOT

BEGIN PLOT /CMS_2013_I1185414/d25
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d26
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d27
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d28
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d29
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d30
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d31
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d32
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d33
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d34
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d35
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d36
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d37
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d38
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d39
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d40
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d41
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d42
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d43
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d44
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d45
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d46
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d47
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\Upsilon(2S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d48
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\Upsilon(2S)$ ($0.6<|y|<1.2$)
END PLOT

BEGIN PLOT /CMS_2013_I1185414/d49
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d50
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in CS frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d51
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d52
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in CS frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d53
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d54
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in CS frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d55
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d56
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in CS frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d57
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d58
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in HX frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d59
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d60
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in HX frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d61
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d62
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in HX frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d63
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d64
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in HX frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d65
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d66
YLabel=$\lambda_\theta$
Title=$\lambda_\theta$ in PX frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d67
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d68
YLabel=$\lambda_\phi$
Title=$\lambda_\phi$ in PX frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d69
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d70
YLabel=$\lambda_{\theta\phi}$
Title=$\lambda_{\theta\phi}$ in PX frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d71
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\Upsilon(3S)$ ($|y|<0.6$)
END PLOT
BEGIN PLOT /CMS_2013_I1185414/d72
YLabel=$\tilde{\lambda}$
Title=$\tilde{\lambda}$ in PX frame for $\Upsilon(3S)$ ($0.6<|y|<1.2$)
END PLOT
