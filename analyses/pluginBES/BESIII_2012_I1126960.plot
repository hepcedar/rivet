BEGIN PLOT /BESIII_2012_I1126960/d01-x01-y01
Title=Cross section for $e^+e^-\to\eta J/\psi$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\eta J/\psi)$ [pb]
LogY=0
END PLOT
