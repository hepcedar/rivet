BEGIN PLOT /SND_2023_I2670980/d01-x01-y01
Title=$\sigma(e^+e^-\to \eta\gamma)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \eta\gamma)$ [pb]
LogY=1
ConnectGaps=1
END PLOT
