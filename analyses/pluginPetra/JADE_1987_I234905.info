Name: JADE_1987_I234905
Year: 1987
Summary: Measurement of $R$ for energies between 12 and 46.47 GeV
Experiment: JADE
Collider: PETRA
InspireID: 234905
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rept. 148 (1987) 67, 1987
RunInfo: e+ e- to hadrons and e+ e- to mu+ mu- (for normalization)
NeedCrossSection: yes
Beams: [e-, e+]
Energies: [12.0, 14.04, 22.0, 25.01, 27.66, 29.93, 30.38, 31.29, 33.89,
           34.5, 35.01, 35.45, 36.38, 40.32, 41.18, 42.55, 43.53, 44.41, 45.59, 46.47]
Description:
  'Measurement of $R$ in $e^+e^-$ collisions for energies between 12 and 46.47 GeV.
   The individual hadronic and muonic cross sections are also outputted to the yoda file
   so that ratio $R$ can be recalculated if runs are combined.'
Keywords: []
BibKey: Naroska:1986si
BibTeX: '@article{Naroska:1986si,
      author         = "Naroska, B.",
      title          = "{E+ e- Physics with the JADE Detector at PETRA}",
      journal        = "Phys. Rept.",
      volume         = "148",
      year           = "1987",
      pages          = "67",
      doi            = "10.1016/0370-1573(87)90031-7",
      reportNumber   = "DESY-86-113",
      SLACcitation   = "%%CITATION = PRPLC,148,67;%%"
}'
