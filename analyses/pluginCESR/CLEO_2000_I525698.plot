BEGIN PLOT /CLEO_2000_I525698/d01-x01-y01
Title=$K^-\pi^+\pi^-$ mass distribution in $\tau^-\to K^-\pi^+\pi^-\nu_\tau$
XLabel=$m_{K^-\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2000_I525698/d01-x01-y02
Title=$K^-\pi^+$ mass distribution in $\tau^-\to K^-\pi^+\pi^-\nu_\tau$
XLabel=$m_{K^-\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2000_I525698/d01-x01-y03
Title=$\pi^+\pi^-$ mass distribution in $\tau^-\to K^-\pi^+\pi^-\nu_\tau$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
