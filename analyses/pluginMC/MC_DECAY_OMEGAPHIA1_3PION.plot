BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/hist0
Title=$\pi^0\pi^0$ mass distribution in $a_1^0\to3\pi^0$ decays
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/dalitz0
Title=Dalitz plot for $a_1^0\to3\pi^0$ decays
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$m_{\pi^0\pi^0}$ [GeV]
ZLabel=$1/\Gamma{\rm d}\Gamma/{\rm d}m_{\pi^0\pi^0}/{\rm d}m_{\pi^0\pi^0}$ [${\rm GeV}^{-2}$]
END PLOT

BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/hist1A
Title=$\pi^0\pi^0$ mass distribution in $a_1^+\to\pi^+2\pi^0$ decays
XLabel=$m_{\pi^0\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^0\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/hist1B
Title=$\pi^0\pi^+$ mass distribution in $a_1^+\to\pi^+2\pi^0$ decays
XLabel=$m_{\pi^0\pi^+}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^0\pi^+}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/dalitz1
Title=Dalitz plot for $a_1^+\to\pi^+2\pi^0$ decays
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$m_{\pi^+\pi^0}$ [GeV]
ZLabel=$1/\Gamma{\rm d}\Gamma/{\rm d}m_{\pi^+\pi^0}/{\rm d}m_{\pi^+\pi^0}$ [${\rm GeV}^{-2}$]
END PLOT

BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/hist2A
Title=$\pi^+\pi^-$ mass distribution in $a_1^0\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/hist2B
Title=$\pi^+\pi^0$ mass distribution in $a_1^0\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/hist2C
Title=$\pi^-\pi^0$ mass distribution in $a_1^0\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/dalitz2
Title=Dalitz plot for $a_1^0\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^-\pi^0}$ [GeV]
YLabel=$m_{\pi^+\pi^0}$ [GeV]
ZLabel=$1/\Gamma{\rm d}\Gamma/{\rm d}m_{\pi^+\pi^0}/{\rm d}m_{\pi^+\pi^0}$ [${\rm GeV}^{-2}$]
END PLOT

BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/hist3A
Title=$\pi^+\pi^+$ mass distribution in $a_1^+\to2\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^+}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/hist3B
Title=$\pi^-\pi^+$ mass distribution in $a_1^+\to2\pi^+\pi^-$ decays
XLabel=$m_{\pi^-\pi^+}$ [GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\pi^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/dalitz3
Title=Dalitz plot for $a_1^+\to2\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$m_{\pi^+\pi^-}$ [GeV]
ZLabel=$1/\Gamma{\rm d}\Gamma/{\rm d}m_{\pi^+\pi^0}/{\rm d}m_{\pi^+\pi^0}$ [${\rm GeV}^{-2}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/xhist_1
Title=$x$ distribution in $\omega\to\pi^0\pi^+\pi^-$ decays
XLabel=$x$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} x$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/yhist_1
Title=$y$ distribution in $\omega\to\pi^0\pi^+\pi^-$ decays
XLabel=$y$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} y$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/mminus_1
Title=$\pi^-\pi^0$ mass distribution in $\omega\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^-\pi^0}$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} m_{\pi^-\pi^0}$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/mplus_1
Title=$\pi^+\pi^0$ mass distribution in $\omega\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^0}$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} m_{\pi^+\pi^0}$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/m0_1
Title=$\pi^+\pi^-$ mass distribution in $\omega\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^-}$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} m_{\pi^+\pi^-}$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/dalitz_1
Title=Dalitz plot for $\omega\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$m_{\pi^+-\pi^0}$ [GeV]
ZLabel=$1/\Gamma{\rm d}\Gamma/{\rm d} m_{\pi^+\pi^0}/{\rm d} m_{\pi^-\pi^0}$ [$\rm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/xhist_2
Title=$x$ distribution in $\phi\to\pi^0\pi^+\pi^-$ decays
XLabel=$x$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} x$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/yhist_2
Title=$y$ distribution in $\phi\to\pi^0\pi^+\pi^-$ decays
XLabel=$y$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} y$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/mminus_2
Title=$\pi^-\pi^0$ mass distribution in $\phi\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^-\pi^0}$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} m_{\pi^-\pi^0}$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/mplus_2
Title=$\pi^+\pi^0$ mass distribution in $\phi\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^0}$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} m_{\pi^+\pi^0}$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/m0_2
Title=$\pi^+\pi^-$ mass distribution in $\phi\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^-}$ [MeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d} m_{\pi^+\pi^-}$ [$\mathrm{MeV}^{-1}$]
END PLOT
BEGIN PLOT /MC_DECAY_OMEGAPHIA1_3PION/dalitz_2
Title=Dalitz plot for $\phi\to\pi^0\pi^+\pi^-$ decays
XLabel=$m_{\pi^+\pi^0}$ [GeV]
YLabel=$m_{\pi^+-\pi^0}$ [GeV]
ZLabel=$1/\Gamma{\rm d}\Gamma/{\rm d} m_{\pi^+\pi^0}/{\rm d} m_{\pi^-\pi^0}$ [$\rm{GeV}^{-2}$]
END PLOT
