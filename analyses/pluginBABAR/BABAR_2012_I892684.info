Name: BABAR_2012_I892684
Year: 2012
Summary: Cross sections for $e^+e^-\to$ $K^+K^-\pi^+\pi^-$, $K^+K^-\pi^0\pi^0$, and $K^+K^-K^+K^-$ between 1.28 and 5.0 GeV
Experiment: BABAR
Collider: PEP-II
InspireID: 892684
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
- Phys.Rev. D86 (2012) 012008, 2012
RunInfo: e+e- to hadrons
NeedCrossSection: yes
Beams: [e+,e-]
Energies: [1.2875, 1.3125, 1.3375, 1.3625, 1.3875, 1.4125, 1.4375, 1.4625, 1.4875, 1.5, 1.5125, 1.5375, 1.54, 1.5625,
           1.58, 1.5875, 1.6125, 1.62, 1.6375, 1.66, 1.6625, 1.6875, 1.7, 1.7125, 1.7375, 1.74, 1.7625, 1.78, 1.7875,
           1.8125, 1.82, 1.8375, 1.86, 1.8625, 1.8875, 1.9, 1.9125, 1.9375, 1.94, 1.9625, 1.98, 1.9875, 2.0125, 2.02,
           2.0375, 2.06, 2.0625, 2.0875, 2.1, 2.1125, 2.1375, 2.14, 2.1625, 2.18, 2.1875, 2.2125, 2.22, 2.2375, 2.26,
           2.2625, 2.2875, 2.3, 2.3125, 2.32, 2.3375, 2.34, 2.3625, 2.38, 2.3875, 2.4, 2.4125, 2.42, 2.4375, 2.46,
           2.4625, 2.48, 2.4875, 2.5, 2.5125, 2.5375, 2.54, 2.5625, 2.58, 2.5875, 2.6, 2.6125, 2.62, 2.6375, 2.66,
           2.6625, 2.6875, 2.7, 2.7125, 2.7375, 2.74, 2.76, 2.7625, 2.78, 2.7875, 2.8125, 2.82, 2.8375, 2.86, 2.8625,
           2.8875, 2.9, 2.9125, 2.92, 2.9375, 2.94, 2.9625, 2.98, 2.9875, 3.0125, 3.02, 3.0375, 3.06, 3.0625, 3.08,
           3.0875, 3.1, 3.1125, 3.1375, 3.14, 3.1625, 3.18, 3.1875, 3.2125, 3.22, 3.2375, 3.24, 3.26, 3.2625, 3.2875,
           3.3, 3.3125, 3.3375, 3.34, 3.3625, 3.38, 3.3875, 3.4, 3.4125, 3.42, 3.4375, 3.46, 3.4625, 3.4875, 3.5,
           3.5125, 3.5375, 3.54, 3.5625, 3.58, 3.5875, 3.6125, 3.62, 3.6375, 3.66, 3.6625, 3.6875, 3.7, 3.7125, 3.7375,
           3.74, 3.7625, 3.78, 3.7875, 3.8125, 3.82, 3.8375, 3.86, 3.8625, 3.8875, 3.9, 3.9125, 3.9375, 3.94, 3.9625,
           3.98, 3.9875, 4.0125, 4.02, 4.0375, 4.0625, 4.0875, 4.1125, 4.1375, 4.1625, 4.1875, 4.2125, 4.2375, 4.2625,
           4.2875, 4.3125, 4.3375, 4.3625, 4.3875, 4.4125, 4.4375, 4.4625, 4.4875, 4.5125, 4.5375, 4.5625, 4.5875,
           4.6125, 4.6375, 4.6625, 4.6875, 4.7125, 4.7375, 4.7625, 4.7875, 4.8125, 4.8375, 4.8625, 4.8875, 4.9125, 4.9375, 4.9625, 4.9875]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for  $e^+e^- \to$  $K^+K^-\pi^+\pi^-$, $K^+K^-\pi^0\pi^0$ and $K^+K^-K^+K^-$ via radiative return,
   including the identification of $K^{*0}$, $\phi$ and $f_0(980)$ mesons for energies between 1.28 and 5.0 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
Keywords:
BibKey: Lees:2011zi
BibTeX: '@article{Lees:2011zi,
      author         = "Lees, J. P. and others",
      title          = "{Cross Sections for the Reactions e+e- --&gt; K+ K- pi+pi-,
                        K+ K- pi0pi0, and K+ K- K+ K- Measured Using Initial-State
                        Radiation Events}",
      collaboration  = "BaBar",
      journal        = "Phys. Rev.",
      volume         = "D86",
      year           = "2012",
      pages          = "012008",
      doi            = "10.1103/PhysRevD.86.012008",
      eprint         = "1103.3001",
      archivePrefix  = "arXiv",
      primaryClass   = "hep-ex",
      reportNumber   = "SLAC-PUB-14403",
      SLACcitation   = "%%CITATION = ARXIV:1103.3001;%%"
}'
