Name: ARGUS_2000_I511512
Year: 2000
Summary: $\gamma\gamma\to K^{*0}\bar{K}^{*0}\ \text{and}\ K^{*+}K^{*-}$ between 1.5 and 3.0 GeV
Experiment: ARGUS
Collider: DORIS
InspireID: 511512
Status: VALIDATED
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Eur.Phys.J.C 16 (2000) 435-444
RunInfo: gamma gamma to hadrons, K0S and pi0 mesons must be set stable
Beams: [gamma, gamma]
Energies: []
Description:
  'Measurement of the differential cross section for $\gamma\gamma\to K^{*0}\bar{K}^{*0}\ \text{and} K^{*+}K^{*-}$ for $1.5 \text{GeV} < W < 3.0 \text{GeV}$. The cross section is measured as a function of the centre-of-mass energy of the photonic collision using a range of final states.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: ARGUS:1999xhg
BibTeX: '@article{ARGUS:1999xhg,
    author = "Albrecht, H. and others",
    collaboration = "ARGUS",
    title = "{Measurement of K* anti-K* production in two photon interactions}",
    reportNumber = "DESY-99-184",
    doi = "10.1007/s100520000408",
    journal = "Eur. Phys. J. C",
    volume = "16",
    pages = "435--444",
    year = "2000"
}
'
