BEGIN PLOT /CRYSTAL_BALL_1990_I294492
LogY=0
YLabel=$\text{d}\sigma/\text{d}|\cos\theta|$ [nb]
XLabel=$|\cos\theta|$
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1990_I294492/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $|\cos\theta|<0.8$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^0\pi^0)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1990_I294492/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $0.27<\sqrt{s}<0.5$ GeV
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1990_I294492/d02-x01-y02
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $0.5<\sqrt{s}<0.7$ GeV
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1990_I294492/d02-x01-y03
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $0.7<\sqrt{s}<0.9$ GeV
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1990_I294492/d02-x01-y04
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $0.9<\sqrt{s}<1.1$ GeV
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1990_I294492/d02-x01-y05
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $1.1<\sqrt{s}<1.3$ GeV
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1990_I294492/d02-x01-y06
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $1.3<\sqrt{s}<1.7$ GeV
END PLOT
BEGIN PLOT /CRYSTAL_BALL_1990_I294492/d02-x01-y07
Title=Cross section for $\gamma\gamma\to \pi^0\pi^0$ with $1.1<\sqrt{s}<1.5$ GeV
END PLOT
