BEGIN PLOT /BELLE_2004_I658085/d
XLabel=$m_{n\pi}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{n\pi}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I658085/d01-x01-y01
Title=Mass of pion system in $B^0\to D^{*-}2\pi^+\pi^-$
END PLOT
BEGIN PLOT /BELLE_2004_I658085/d01-x01-y02
Title=Mass of pion system in $B^+\to D^{*-}3\pi^+\pi^-$
END PLOT
BEGIN PLOT /BELLE_2004_I658085/d01-x01-y03
Title=Mass of pion system in $B^0\to D^{*-}3\pi^+2\pi^-$
END PLOT
BEGIN PLOT /BELLE_2004_I658085/d01-x01-y04
Title=Mass of pion system in $B^+\to \bar{D}^{*0}2\pi^+\pi^-$
END PLOT
BEGIN PLOT /BELLE_2004_I658085/d01-x01-y05
Title=Mass of pion system in $B^0\to \bar{D}^{*0}2\pi^+2\pi^-$
END PLOT
BEGIN PLOT /BELLE_2004_I658085/d01-x01-y06
Title=Mass of pion system in $B^+\to \bar{D}^{*0}3\pi^+2\pi^-$
END PLOT
