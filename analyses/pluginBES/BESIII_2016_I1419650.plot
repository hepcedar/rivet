BEGIN PLOT /BESIII_2016_I1419650/d01-x01-y01
Title=$\phi\phi$ mass distribution in $J/\psi\to\gamma\phi\phi$
XLabel=$m_{\phi\phi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\phi\phi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1419650/d01-x01-y02
Title=$\cos\theta_\gamma$ distribution in $J/\psi\to\gamma\phi\phi$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1419650/d01-x01-y03
Title=$\cos\theta_\phi$ distribution in $J/\psi\to\gamma\phi\phi$
XLabel=$\cos\theta_\phi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\phi$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1419650/d01-x01-y04
Title=$\cos\theta_K$ distribution in $J/\psi\to\gamma\phi\phi$
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_K$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2016_I1419650/d01-x01-y05
Title=$\chi$ distribution in $J/\psi\to\gamma\phi\phi$
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$
LogY=0
END PLOT
