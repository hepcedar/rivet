BEGIN PLOT /LHCB_2024_I2824757
LogY=0
END PLOT


BEGIN PLOT /LHCB_2024_I2824757/d01
YLabel=$\alpha$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x0[1-8]-y04
YLabel=$A_\alpha$
END PLOT

BEGIN PLOT /LHCB_2024_I2824757/d01-x01-y01
Title=Decay asymmetry parameter $\alpha$ for  $\Lambda^0_b\to \Lambda^+_c\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x01-y02
Title=Decay asymmetry  $\alpha$ for  $\bar\Lambda^0_b\to \bar\Lambda^-_c\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x01-y03
Title=Decay asymmetry  $\alpha$ for  $\Lambda^0_b\to \Lambda^+_c\pi^-$ (average)
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x01-y04
Title=Asymmetry of decay parameters $A_\alpha$ for  $\Lambda^0_b\to \Lambda^+_c\pi^-$
END PLOT

BEGIN PLOT /LHCB_2024_I2824757/d01-x02-y01
Title=Decay asymmetry parameter $\alpha$ for  $\Lambda^0_b\to \Lambda^+_cK^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x02-y02
Title=Decay asymmetry  $\alpha$ for  $\bar\Lambda^0_b\to \bar\Lambda^-_cK^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x02-y03
Title=Decay asymmetry  $\alpha$ for  $\Lambda^0_b\to \Lambda^+_cK^-$ (average)
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x02-y04
Title=Asymmetry of decay parameters $A_\alpha$ for  $\Lambda^0_b\to \Lambda^+_cK^-$
END PLOT



BEGIN PLOT /LHCB_2024_I2824757/d01-x03-y01
Title=Decay asymmetry parameter $\alpha$ for  $\Lambda^+_c\to\Lambda^0\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x03-y02
Title=Decay asymmetry  $\alpha$ for  $\bar\Lambda^-_c\to\bar\Lambda^0\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x03-y03
Title=Decay asymmetry  $\alpha$ for  $\Lambda^+_c\to\Lambda^0\pi^+$(average)
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x03-y04
Title=Asymmetry of decay parameters $A_\alpha$ for  $\Lambda^+_c\to\Lambda^0\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x04-y01
Title=Decay asymmetry parameter $\alpha$ for  $\Lambda^+_c\to\Lambda^0K^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x04-y02
Title=Decay asymmetry  $\alpha$ for  $\bar\Lambda^-_c\to\bar\Lambda^0K^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x04-y03
Title=Decay asymmetry  $\alpha$ for  $\Lambda^+_c\to\Lambda^0K^+$(average)
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x04-y04
Title=Asymmetry of decay parameters $A_\alpha$ for  $\Lambda^+_c\to\Lambda^0K^+$
END PLOT


BEGIN PLOT /LHCB_2024_I2824757/d01-x05-y01
Title=Decay asymmetry parameter $\alpha$ for  $\Lambda^+_c\to pK^0_S$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x05-y02
Title=Decay asymmetry  $\alpha$ for  $\bar\Lambda^-_c\to \bar{p}K^0_S$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x05-y03
Title=Decay asymmetry  $\alpha$ for  $\Lambda^+_c\to pK^0_S$(average)
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x05-y04
Title=Asymmetry of decay parameters $A_\alpha$ for  $\Lambda^+_c\to pK^0_S$
END PLOT


BEGIN PLOT /LHCB_2024_I2824757/d01-x06-y01
Title=Decay asymmetry parameter $\alpha$ for  $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x06-y02
Title=Decay asymmetry  $\alpha$ for  $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x06-y03
Title=Decay asymmetry  $\alpha$ for  $\Lambda^0\to p\pi^-$ (average)
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d01-x06-y04
Title=Asymmetry of decay parameters $A_\alpha$ for  $\Lambda^0\to p\pi^-$
END PLOT

BEGIN PLOT /LHCB_2024_I2824757/d02-x0[1,2]
YLabel=$\beta$
BEGIN PLOT /LHCB_2024_I2824757/d02-x01-y01
Title=Decay asymmetry parameter $\beta$ for  $\Lambda_c^+\to \Lambda^0\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d02-x01-y02
Title=Decay asymmetry parameter $\beta$ for  $\Lambda_c^+\to \Lambda^0K^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d02-x02-y01
Title=Decay asymmetry parameter $\beta$ for  $\bar\Lambda_c^-\to \bar\Lambda^0\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d02-x02-y02
Title=Decay asymmetry parameter $\beta$ for  $\bar\Lambda_c^-\to \bar\Lambda^0K^-$
END PLOT

BEGIN PLOT /LHCB_2024_I2824757/d02-x0[3,4]
YLabel=$\gamma$
BEGIN PLOT /LHCB_2024_I2824757/d02-x03-y01
Title=Decay asymmetry parameter $\gamma$ for  $\Lambda_c^+\to \Lambda^0\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d02-x03-y02
Title=Decay asymmetry parameter $\gamma$ for  $\Lambda_c^+\to \Lambda^0K^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d02-x04-y01
Title=Decay asymmetry parameter $\gamma$ for  $\bar\Lambda_c^-\to \bar\Lambda^0\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/d02-x04-y02
Title=Decay asymmetry parameter $\gamma$ for  $\bar\Lambda_c^-\to \bar\Lambda^0K^-$
END PLOT

BEGIN PLOT /LHCB_2024_I2824757/h_cos_1
XLabel=$\cos\theta_1$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_1$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_0_0_0
Title=$\cos\theta_1$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0\pi^+) \pi^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_0_0_1
Title=$\cos\theta_1$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0 K^+) \pi^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_0_0_2
Title=$\cos\theta_1$ for $\Lambda_b^0\to \Lambda_c^+(\to pK^0_S) \pi^-$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_0_1_0
Title=$\cos\theta_1$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0\pi^+) K^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_0_1_1
Title=$\cos\theta_1$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0 K^+) K^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_0_1_2
Title=$\cos\theta_1$ for $\Lambda_b^0\to \Lambda_c^+(\to pK^0_S) K^-$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_1_0_0
Title=$\cos\theta_1$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0\pi^-) \pi^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_1_0_1
Title=$\cos\theta_1$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0 K^-) \pi^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_1_0_2
Title=$\cos\theta_1$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to \bar{p}K^0_S) \pi^+$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_1_1_0
Title=$\cos\theta_1$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0\pi^-) K^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_1_1_1
Title=$\cos\theta_1$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0 K^-) K^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_1_1_1_2
Title=$\cos\theta_1$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to \bar{p}K^0_S) K^+$ 
END PLOT

BEGIN PLOT /LHCB_2024_I2824757/h_cos_2
XLabel=$\cos\theta_2$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_2$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_0_0_0
Title=$\cos\theta_2$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0\pi^+) \pi^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_0_0_1
Title=$\cos\theta_2$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0 K^+) \pi^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_0_0_2
Title=$\cos\theta_2$ for $\Lambda_b^0\to \Lambda_c^+(\to pK^0_S) \pi^-$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_0_1_0
Title=$\cos\theta_2$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0\pi^+) K^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_0_1_1
Title=$\cos\theta_2$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0 K^+) K^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_0_1_2
Title=$\cos\theta_2$ for $\Lambda_b^0\to \Lambda_c^+(\to pK^0_S) K^-$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_1_0_0
Title=$\cos\theta_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0\pi^-) \pi^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_1_0_1
Title=$\cos\theta_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0 K^-) \pi^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_1_0_2
Title=$\cos\theta_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to \bar{p}K^0_S) \pi^+$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_1_1_0
Title=$\cos\theta_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0\pi^-) K^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_1_1_1
Title=$\cos\theta_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0 K^-) K^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_cos_2_1_1_2
Title=$\cos\theta_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to \bar{p}K^0_S) K^+$ 
END PLOT

BEGIN PLOT /LHCB_2024_I2824757/h_phi_2
XLabel=$\phi_2$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\phi_2$ [$\mathrm{rad}^{-1}$]
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_0_0_0
Title=$\phi_2$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0\pi^+) \pi^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_0_0_1
Title=$\phi_2$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0 K^+) \pi^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_0_0_2
Title=$\phi_2$ for $\Lambda_b^0\to \Lambda_c^+(\to pK^0_S) \pi^-$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_0_1_0
Title=$\phi_2$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0\pi^+) K^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_0_1_1
Title=$\phi_2$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0 K^+) K^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_0_1_2
Title=$\phi_2$ for $\Lambda_b^0\to \Lambda_c^+(\to pK^0_S) K^-$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_1_0_0
Title=$\phi_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0\pi^-) \pi^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_1_0_1
Title=$\phi_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0 K^-) \pi^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_1_0_2
Title=$\phi_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to \bar{p}K^0_S) \pi^+$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_1_1_0
Title=$\phi_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0\pi^-) K^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_1_1_1
Title=$\phi_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0 K^-) K^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/h_phi_2_1_1_2
Title=$\phi_2$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to \bar{p}K^0_S) K^+$ 
END PLOT

BEGIN PLOT /LHCB_2024_I2824757/p_cos_12
YLabel=$\langle \cos\theta_1\cos\theta_2\rangle$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_0_0_0
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0\pi^+) \pi^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_0_0_1
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0 K^+) \pi^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_0_0_2
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\Lambda_b^0\to \Lambda_c^+(\to pK^0_S) \pi^-$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_0_1_0
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0\pi^+) K^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_0_1_1
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\Lambda_b^0\to \Lambda_c^+(\to\Lambda^0 K^+) K^-$ with $\Lambda^0\to p\pi^-$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_0_1_2
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\Lambda_b^0\to \Lambda_c^+(\to pK^0_S) K^-$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_1_0_0
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0\pi^-) \pi^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_1_0_1
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0 K^-) \pi^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_1_0_2
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to \bar{p}K^0_S) \pi^+$ 
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_1_1_0
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0\pi^-) K^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_1_1_1
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to\bar\Lambda^0 K^-) K^+$ with $\bar\Lambda^0\to \bar{p}\pi^+$
END PLOT
BEGIN PLOT /LHCB_2024_I2824757/p_cos_12_1_1_2
Title=$\langle \cos\theta_1\cos\theta_2\rangle$ for $\bar\Lambda_b^0\to \bar\Lambda_c^-(\to \bar{p}K^0_S) K^+$ 
END PLOT
