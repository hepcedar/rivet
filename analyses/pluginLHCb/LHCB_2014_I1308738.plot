BEGIN PLOT /LHCB_2014_I1308738
LogY=0
END PLOT
BEGIN PLOT /LHCB_2014_I1308738/d01-x01-y01
Title=Fraction of $\Upsilon(1S)$ from $\chi_{b(0,1,2)}(1P)\to\gamma \Upsilon(1S)$ at 7 TeV
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
END PLOT
BEGIN PLOT /LHCB_2014_I1308738/d04-x01-y01
Title=Fraction of $\Upsilon(1S)$ from $\chi_{b(0,1,2)}(1P)\to\gamma \Upsilon(1S)$ at 8 TeV
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
END PLOT

BEGIN PLOT /LHCB_2014_I1308738/d01-x01-y02
Title=Fraction of $\Upsilon(1S)$ from $\chi_{b(0,1,2)}(2P)\to\gamma \Upsilon(1S)$ at 7 TeV
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
END PLOT
BEGIN PLOT /LHCB_2014_I1308738/d04-x01-y02
Title=Fraction of $\Upsilon(1S)$ from $\chi_{b(0,1,2)}(2P)\to\gamma \Upsilon(1S)$ at 8 TeV
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
END PLOT

BEGIN PLOT /LHCB_2014_I1308738/d01-x01-y03
Title=Fraction of $\Upsilon(1S)$ from $\chi_{b(0,1,2)}(3P)\to\gamma \Upsilon(1S)$ at 7 TeV
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
END PLOT
BEGIN PLOT /LHCB_2014_I1308738/d04-x01-y03
Title=Fraction of $\Upsilon(1S)$ from $\chi_{b(0,1,2)}(3P)\to\gamma \Upsilon(1S)$ at 8 TeV
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
END PLOT

BEGIN PLOT /LHCB_2014_I1308738/d02-x01-y02
Title=Fraction of $\Upsilon(2S)$ from $\chi_{b(0,1,2)}(2P)\to\gamma \Upsilon(2S)$ at 7 TeV
XLabel=$p_\perp^{\Upsilon(2S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(2S)}$ [$\%$]
END PLOT
BEGIN PLOT /LHCB_2014_I1308738/d05-x01-y02
Title=Fraction of $\Upsilon(2S)$ from $\chi_{b(0,1,2)}(2P)\to\gamma \Upsilon(2S)$ at 8 TeV
XLabel=$p_\perp^{\Upsilon(2S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(2S)}$ [$\%$]
END PLOT

BEGIN PLOT /LHCB_2014_I1308738/d02-x01-y03
Title=Fraction of $\Upsilon(2S)$ from $\chi_{b(0,1,2)}(3P)\to\gamma \Upsilon(2S)$ at 7 TeV
XLabel=$p_\perp^{\Upsilon(2S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(2S)}$ [$\%$]
END PLOT
BEGIN PLOT /LHCB_2014_I1308738/d05-x01-y03
Title=Fraction of $\Upsilon(2S)$ from $\chi_{b(0,1,2)}(3P)\to\gamma \Upsilon(2S)$ at 8 TeV
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
END PLOT

BEGIN PLOT /LHCB_2014_I1308738/d03-x01-y03
Title=Fraction of $\Upsilon(3S)$ from $\chi_{b(0,1,2)}(3P)\to\gamma \Upsilon(3S)$ at 7 TeV
XLabel=$p_\perp^{\Upsilon(3S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(3S)}$ [$\%$]
END PLOT
BEGIN PLOT /LHCB_2014_I1308738/d06-x01-y03
Title=Fraction of $\Upsilon(3S)$ from $\chi_{b(0,1,2)}(3P)\to\gamma \Upsilon(3S)$ at 8 TeV
XLabel=$p_\perp^{\Upsilon(1S)}$ [GeV]
YLabel=$N_{\mathrm{feeddown}} /N_{\Upsilon(1S)}$ [$\%$]
END PLOT
