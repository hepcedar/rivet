BEGIN PLOT /LHCB_2013_I1238809/d01-x01-y01
Title=Cross section for $B^0$ production ($2<y<4.5$, $p_\perp<40$\,GeV
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d01-x01-y02
Title=Cross section for $B^+$ production ($2<y<4.5$, $p_\perp<40$\,GeV
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d01-x01-y03
Title=Cross section for $B^0_s$ production ($2<y<4.5$, $p_\perp<40$\,GeV
YLabel=$\sigma$  [$\mu$b]
END PLOT

BEGIN PLOT /LHCB_2013_I1238809/d0[2,3,4]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d02-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d02-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d02-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0$ ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2013_I1238809/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ ($2.0<y<2.5$) 
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d03-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d03-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d03-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^+$ ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2013_I1238809/d04-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0_s$ ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d04-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0_s$ ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d04-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0_s$ ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d04-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0_s$ ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d04-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $B^0_s$ ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2013_I1238809/d0[5,6,7]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d05-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $B^0$ 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d06-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $B^+$
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d07-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for xs$B^0_s$
END PLOT

BEGIN PLOT /LHCB_2013_I1238809/d[08,09,10]
XLabel=$y$ [GeV]
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ $\mu$
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d08-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $B^0$ 
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d09-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $B^+$
END PLOT
BEGIN PLOT /LHCB_2013_I1238809/d10-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $B^0_s$
END PLOT
