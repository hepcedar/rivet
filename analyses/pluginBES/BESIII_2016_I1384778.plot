BEGIN PLOT /BESIII_2016_I1384778/d01-x01-y05
Title=Assymetry for unlike over like sign pion pairs vs $z_1,z_2$
XLabel=$z_1\leftrightarrow z_2$
YLabel=$A_{UL}$
LogY=0
XCustomMajorTicks=1     $\begin{array}{cc}.2,.3\\.2,.3\end{array}{cc}$     2          $\begin{array}{cc}.2,.3\\.3,.5\end{array}{cc}$      3          $\begin{array}{cc}.2,.3\\.5,.9\end{array}{cc}$ 4     $\begin{array}{cc}.3,.5\\.3,.5\end{array}{cc}$     5          $\begin{array}{cc}.3,.5\\.5,.9\end{array}{cc}$      6          $\begin{array}{cc}.5,.9\\.5,.9\end{array}{cc}$ 
END PLOT

BEGIN PLOT /BESIII_2016_I1384778/d01-x01-y06
Title=Assymetry for unlike over all pion pairs vs $z_1,z_2$
XLabel=$z_1\leftrightarrow z_2$
YLabel=$A_{UC}$
LogY=0
XCustomMajorTicks=1     $\begin{array}{cc}.2,.3\\.2,.3\end{array}{cc}$     2          $\begin{array}{cc}.2,.3\\.3,.5\end{array}{cc}$      3          $\begin{array}{cc}.2,.3\\.5,.9\end{array}{cc}$ 4     $\begin{array}{cc}.3,.5\\.3,.5\end{array}{cc}$     5          $\begin{array}{cc}.3,.5\\.5,.9\end{array}{cc}$      6          $\begin{array}{cc}.5,.9\\.5,.9\end{array}{cc}$ 
END PLOT


BEGIN PLOT /BESIII_2016_I1384778/d02-x01-y04
Title=Assymetry for unlike over like sign pion pairs vs $p_\perp$
XLabel=$p_\perp$ [GeV]
YLabel=$A_{UL}$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2016_I1384778/d02-x01-y05
Title=Assymetry for unlike over all pion pairs vs $p_\perp$
XLabel=$p_\perp$ [GeV]
YLabel=$A_{UC}$
LogY=0
END PLOT
