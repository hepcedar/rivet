#Plot definitions for analysis LHCB_2015_I1333223

# BEGIN PLOT /LHCB_2015_I1333223/d01-x01-y01
#FullRange=1
#XMin=0.0
#YMin=0.0
XLabel=$\sqrt{s}$ [GeV]
Title=Inelastic cross-section in LHCb fiducial volume
YLabel=$\sigma_{\mathrm{inel}}$ [mb]
LogY=0
RatioPlot=1
# END PLOT


