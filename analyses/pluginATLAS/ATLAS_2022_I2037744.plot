# BEGIN PLOT /ATLAS_2022_I2037744/*
XTwosidedTicks=1
YTwosidedTicks=1
LogY=1
RatioPlotYMax=1.5
RatioPlotYMin=0.5
YLabelSep=6
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d02-x01-y01
LogY=0
XCustomMajorTicks=0.5   13TeV
XMinorTickMarks=0
YLabel=Inclusive fiducial cross-section [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d03-x01-y01
XLabel=$p^{t,h}_{\mathrm{T}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p^{t,h}_{\mathrm{T}}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d05-x01-y01
XLabel=$p^{t,h}_{\mathrm{T}}$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} p^{t,h}_{\mathrm{T}}$ [1/GeV]
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d06-x01-y01
XLabel=$p^{t,\ell}_{\mathrm{T}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p^{t,\ell}_{\mathrm{T}}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d08-x01-y01
XLabel=$p^{t,\ell}_{\mathrm{T}}$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} p^{t,\ell}_{\mathrm{T}}$ [1/GeV]
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d09-x01-y01
XLabel=$m^{t\bar{t}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m^{t\bar{t}}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d11-x01-y01
XLabel=$m^{t\bar{t}}$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} m^{t\bar{t}}$ [1/GeV]
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d12-x01-y01
XLabel=$|y^{t,h}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d} |y^{t,h}|$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d14-x01-y01
XLabel=$|y^{t,h}|$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} |y^{t,h}|$
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d15-x01-y01
XLabel=$|y^{t,\ell}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d} |y^{t,\ell}|$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d17-x01-y01
XLabel=$|y^{t,\ell}|$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} |y^{t,\ell}|$
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d18-x01-y01
XLabel=$|y^{t\bar{t}}|$
YLabel=$\mathrm{d}\sigma / \mathrm{d} |y^{t\bar{t}}|$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d20-x01-y01
XLabel=$|y^{t\bar{t}}|$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} |y^{t\bar{t}}|$
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d21-x01-y01
XLabel=$H_{\mathrm{T}}^{t\bar{t}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} H_{\mathrm{T}}^{t\bar{t}}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d23-x01-y01
XLabel=$H_{\mathrm{T}}^{t\bar{t}}$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} H_{\mathrm{T}}^{t\bar{t}}$ [1/GeV]
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d24-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(b_{\ell},t_{h})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(b_{\ell},t_{h})$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d26-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(b_{\ell},t_{h})$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(b_{\ell},t_{h})$
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d27-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t\bar{t}}$ [pb/GeV]
RatioPlotYMax=1.75
RatioPlotYMin=0.25
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d29-x01-y01
XLabel=$p_{\mathrm{T}}^{t\bar{t}}$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} p_{\mathrm{T}}^{t\bar{t}}$ [1/GeV]
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d30-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(t_{h},t_{\ell})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(t_{h},t_{\ell})$ [pb]
RatioPlotYMax=1.75
RatioPlotYMin=0.25
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d32-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(t_{h},t_{\ell})$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(t_{h},t_{\ell})$
RatioPlotYMax=1.75
RatioPlotYMin=0.25
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d33-x01-y01
XLabel=$H_{\mathrm{T}}^{t\bar{t}+\mathrm{jets}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} H_{\mathrm{T}}^{t\bar{t}+\mathrm{jets}}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d35-x01-y01
XLabel=$H_{\mathrm{T}}^{t\bar{t}+\mathrm{jets}}$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} H_{\mathrm{T}}^{t\bar{t}+\mathrm{jets}}$ [1/GeV]
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d36-x01-y01
XMinorTickMarks=0
XCustomMajorTicks=0.0 0 1.0 1 2.0 2 3.5 3-4 5.5 $>4$
XLabel=$N^{j}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} N^{j}$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d38-x01-y01
XMinorTickMarks=0
XCustomMajorTicks=0.0 0 1.0 1 2.0 2 3.5 3-4 5.5 $>4$
XLabel=$N^{j}$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} N^{j}$
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d39-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p^{j,1}_{\mathrm{T}}$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d41-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} p^{j,1}_{\mathrm{T}}$ [1/GeV]
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d42-x01-y01
XLabel=$m(j_{1},t_{h})$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} m(j_{1},t_{h})$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d44-x01-y01
XLabel=$m(j_{1},t_{h})$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} m(j_{1},t_{h})$ [1/GeV]
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d45-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h})$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d47-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h})$
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d48-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{2},t_{h})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(j_{2},t_{h})$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d50-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{2},t_{h})$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(j_{2},t_{h})$
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d51-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},j_{2})$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},j_{2})$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d53-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},j_{2})$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},j_{2})$
# END PLOT


# BEGIN PLOT /ATLAS_2022_I2037744/d54-x01-y01
XLabel=$p^{j,2}_{\mathrm{T}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} p^{j,2}_{\mathrm{T}}$ [pb/GeV]
RatioPlotYMax=1.75
RatioPlotYMin=0.25
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d56-x01-y01
XLabel=$p^{j,2}_{\mathrm{T}}$ [GeV]
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / \mathrm{d} p^{j,2}_{\mathrm{T}}$ [1/GeV]
RatioPlotYMax=1.75
RatioPlotYMin=0.25
# END PLOT




# BEGIN PLOT /ATLAS_2022_I2037744/d57-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$N^{j} = 1$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} N^{j})$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d58-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$N^{j} = 2$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} N^{j})$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d59-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$N^{j} \geq 3$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} N^{j})$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d60-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$N^{j} = 1$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} N^{j})$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d61-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$N^{j} = 2$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} N^{j})$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d62-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$N^{j} \geq 3$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} N^{j})$ [pb/GeV]
# END PLOT



# BEGIN PLOT /ATLAS_2022_I2037744/d69-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$355 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 398 \mathrm{GeV}$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [1/GeV${}^{2}$]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d70-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$398 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 496 \mathrm{GeV}$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [1/GeV${}^{2}$]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d71-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$496 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 2000 \mathrm{GeV}$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [1/GeV${}^{2}$]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d72-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$355 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 398 \mathrm{GeV}$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [pb/GeV${}^{2}$]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d73-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$398 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 496 \mathrm{GeV}$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [pb/GeV${}^{2}$]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d74-x01-y01
XLabel=$p^{j,1}_{\mathrm{T}}$ [GeV]
Title=$496 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 2000 \mathrm{GeV}$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} p^{j,1}_{\mathrm{T}} / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [pb/GeV${}^{2}$]
# END PLOT




# BEGIN PLOT /ATLAS_2022_I2037744/d81-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$355 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 398 \mathrm{GeV}$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d82-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$398 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 496 \mathrm{GeV}$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d83-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$496 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 2000 \mathrm{GeV})$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [1/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d84-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$355 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 398 \mathrm{GeV}$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d85-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$398 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 496 \mathrm{GeV}$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d86-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$496 \mathrm{GeV} < p^{t,h}_{\mathrm{T}} \leq 2000 \mathrm{GeV}$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} p^{t,h}_{\mathrm{T}})$ [pb/GeV]
# END PLOT



# BEGIN PLOT /ATLAS_2022_I2037744/d93-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$N^{j} = 1$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} N^{j})$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d94-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$N^{j} = 2)$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} N^{j})$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d95-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$N^{j} \geq 3$
YLabel=$1/\sigma \cdot \mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} N^{j})$
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d96-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$N^{j} = 1$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} N^{j})$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d97-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$N^{j} = 2$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} N^{j})$ [pb]
# END PLOT

# BEGIN PLOT /ATLAS_2022_I2037744/d98-x01-y01
XLabel=$\frac{\Delta\phi}{\pi}(j_{1},t_{h})$
Title=$N^{j} \geq 3$
YLabel=$\mathrm{d}\sigma / (\mathrm{d} \frac{\Delta\phi}{\pi}(j_{1},t_{h}) / \mathrm{d} N^{j})$ [pb]
# END PLOT


