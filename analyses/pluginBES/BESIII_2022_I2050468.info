Name: BESIII_2022_I2050468
Year: 2022
Summary: Cross section for $e^+e^-\to \pi^+\pi^-\psi_2(3823)(\to\gamma\chi_{c1})$ between 4.22 and 4.70 GeV
Experiment: BESIII
Collider: BEPC
InspireID: 2050468
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - arXiv:2203.05815 [hep-ex]
RunInfo: e+e- to hadrons, KS0 and pi0 should be set stable
NeedCrossSection: yes
Beams:  [e+,e-]
Energies :  [4.2263, 4.258, 4.2879, 4.3121, 4.3374, 4.3583, 4.3774, 4.3965, 4.4156, 4.4362,
             4.4671, 4.5271, 4.5745, 4.5995, 4.612, 4.6278, 4.6408, 4.6613, 4.6811, 4.6984]
Options:
 - ENERGY=*
Description:
   'Cross section for $e^+e^-\to \pi^+\pi^-\psi_2(3823)(\to\gamma\chi_{c1})$ between 4.22 and 4.70 GeV.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2022yga
BibTeX: '@article{BESIII:2022yga,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of resonance structures in $e^+e^-\to \pi^+\pi^-\psi_2(3823)$ and mass measurement of $\psi_2(3823)$}",
    eprint = "2203.05815",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    month = "3",
    year = "2022"
}
'
