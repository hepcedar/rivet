BEGIN PLOT /CMS_2020_I1738943/d01-x01-y01
Title=Inclusive $\Lambda_c^+$ differential cross section ($|y|<1$)
XLabel=$p_\perp$ [GeV]
YLabel=$\text{d}\sigma/\text{d}p_\perp$ [$\mu$b/GeV]
END PLOT

BEGIN PLOT /CMS_2020_I1738943/d04-x01-y01
Title=Inclusive $\Lambda_c^+$ tp $D^0$ ratio ($|y|<1$)
XLabel=$p_\perp$ [GeV]
YLabel=$\sigma(\lambda_c^+)/\sigma(D^0)$
END PLOT
