BEGIN PLOT /DELCO_1985_I205132/d01-x01-y01
Title=$D^{*\pm}$ spectrum at 29 GeV
XLabel=$z$
YLabel=$s\mathrm{d}\sigma/\mathrm{d}z$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /DELCO_1985_I205132/d01-x01-y02
Title=$D^{*\pm}$ spectrum at 29 GeV (non-bottom)
XLabel=$z$
YLabel=$s\mathrm{d}\sigma/\mathrm{d}z$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
LogY=0
END PLOT
BEGIN PLOT /DELCO_1985_I205132/d02-x01-y01
Title=$D^{*\pm}$ cross section at 29 GeV
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [nb]
LogY=0
END PLOT
