# BEGIN PLOT /ATLAS_2011_I896268/.*
FullRange=1
LogY=1
LogX=1
XLabel=$p^{J/\psi}_\mathrm{T}$ [GeV]
YLabel=Br(J/$\psi \to \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I896268/2.*
YLabel=Br(J/$\psi \to \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I896268/d14-x01-y01
Title=Non-Prompt J/$\psi$ $2.0 < |y| < 2.4$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d13-x01-y01
Title=Non-Prompt J/$\psi$ $1.5 < |y| < 2.0$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d12-x01-y01
Title=Non-Prompt J/$\psi$ $0.75 < |y| < 1.5$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d11-x01-y01
Title=Non-Prompt J/$\psi$ $|y| < 0.75$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I896268/d18-x01-y01
Title=Prompt J/$\psi$ $2.0 < |y| < 2.4$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d17-x01-y01
Title=Prompt J/$\psi$ $1.5 < |y| < 2.0$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d16-x01-y01
Title=Prompt J/$\psi$ $0.75 < |y| < 1.5$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d15-x01-y01
Title=Prompt J/$\psi$ $|y| < 0.75$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I896268/d20-x01-y01
Title=Inclusive J/$\psi$ $2.0 < |y| < 2.4$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d21-x01-y01
Title=Inclusive J/$\psi$ $1.5 < |y| < 2.0$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d22-x01-y01
Title=Inclusive J/$\psi$ $0.75 < |y| < 1.5$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I896268/d23-x01-y01
Title=Inclusive J/$\psi$ $|y| < 0.75$
# END PLOT
