BEGIN PLOT /BELLE_2012_I1114749/d01-x01-y01
Title=Cross section for $e^+e^-\to e^+e^-\pi^0$
XLabel=$Q^2$ [$\text{GeV}^2$]
YLabel=$\text{d}\sigma/\text{d}Q^2$ [$\text{fb}/\text{GeV}^2$]
END PLOT
