BEGIN PLOT /BELLE_2002_I566127/d01-x01-y01
Title=$K^+K^-$ mass distribution in $\Lambda_c^+\to \Sigma^+ K^+K^-$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{K^+K^-}$ [$\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I566127/d02-x01-y01
Title=$\Sigma^+K^-$ mass distribution in $\Lambda_c^+\to \Sigma^+ K^+K^-$
XLabel=$m_{\Sigma^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\Sigma^+K^-}$ [$\mathrm{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2002_I566127/d03-x01-y01
Title=$\Lambda^0K^0_S$ mass distribution in $\Lambda_c^+\to \Lambda^0 K^0_SK^+$
XLabel=$m_{\Lambda^0K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\Lambda^0K^0_S}$ [$\mathrm{GeV}$]
LogY=0
END PLOT
