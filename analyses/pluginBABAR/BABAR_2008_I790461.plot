BEGIN PLOT /BABAR_2008_I790461
LogY=0
END PLOT

BEGIN PLOT /BABAR_2008_I790461/d01-x01-y01
Title=$K^+K^-$ mass in $D_s^+\to K^+K^- e^+\nu_e$
XLabel=$m_{K^+K^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BABAR_2008_I790461/d02-x01-y01
Title=$e^+\nu_e$ mass squared in $D_s^+\to K^+K^- e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BABAR_2008_I790461/d02-x01-y02
Title=$\cos\theta_e$ in $D_s^+\to K^+K^- e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BABAR_2008_I790461/d02-x01-y03
Title=$\cos\theta_K$ in $D_s^+\to K^+K^- e^+\nu_e$
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT

BEGIN PLOT /BABAR_2008_I790461/d02-x01-y04
Title=$\chi$ in $D_s^+\to K^+K^- e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
