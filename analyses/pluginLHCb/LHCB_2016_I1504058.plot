# BEGIN PLOT /LHCB_2016_I1504058/*
XLabel=$\eta$
YLabel=$\mathrm{d}\sigma/\mathrm{d}\eta$ [$\mu\mathrm{b}$]
# END PLOT

# BEGIN PLOT /LHCB_2016_I1504058/d01-x01-y01
Title=$pp \to \mathrm{H}_\mathrm{b} \mathrm{X}$, $\sqrt{s} = 7\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2016_I1504058/d02-x01-y01
Title=$pp \to \mathrm{H}_\mathrm{b} \mathrm{X}$, $\sqrt{s} = 13\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2016_I1504058/d03-x01-y01
YLabel=$R_{13/7}(\mathrm{d}\sigma(pp \to \mathrm{H}_\mathrm{b} \mathrm{X})/\mathrm{d}\eta)$
Title=Cross-section ratio at $\sqrt{s} = 13$ and $7\,\mathrm{TeV}$
# END PLOT

