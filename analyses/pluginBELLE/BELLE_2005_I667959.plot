BEGIN PLOT /BELLE_2005_I667959/d01-x01-y01
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d01-x01-y02
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d01-x01-y03
Title=$K^+\pi^+$ mass in $B^+\to K^+\pi^+\pi^-$
XLabel=$m_{K^+\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d01-x02-y01
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d01-x02-y02
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667959/d02-x01-y01
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}<11\,\text{GeV}^2$)
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d02-x01-y02
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}>11\,\text{GeV}^2$)
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667959/d03-x01-y01
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}<5\,\text{GeV}^2$)
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d03-x01-y02
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($5<m^2_{\pi^+\pi^-}<10\,\text{GeV}^2$)
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d03-x01-y03
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($10<m^2_{\pi^+\pi^-}<15\,\text{GeV}^2$)
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d03-x01-y04
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($15<m^2_{\pi^+\pi^-}<20\,\text{GeV}^2$)
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d03-x01-y05
Title=$K^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($m^2_{\pi^+\pi^-}>20\text{GeV}^2$)
XLabel=$m_{K^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667959/d03-x02-y01
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($m^2_{K^+\pi^-}<5\,\text{GeV}^2$)
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d03-x02-y02
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($5<m^2_{K^+\pi^-}<10\,\text{GeV}^2$)
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d03-x02-y03
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($10<m^2_{K^+\pi^-}<15\,\text{GeV}^2$)
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d03-x02-y04
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($15<m^2_{K^+\pi^-}<20\,\text{GeV}^2$)
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d03-x02-y05
Title=$\pi^+\pi^-$ mass in $B^+\to K^+\pi^+\pi^-$ ($m^2_{K^+\pi^-}>20\,\text{GeV}^2$)
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667959/d04-x01-y01
Title=$K^+\pi^-$ helicity angle for $0.82<m_{K^+\pi^-}<0.97\,$GeV
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d04-x01-y02
Title=$K^+\pi^-$ helicity angle for $1.0<m_{K^+\pi^-}<1.76\,$GeV
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d04-x01-y03
Title=$\pi^+\pi^-$ helicity angle for $m_{\pi^+\pi^-}<0.9\,$GeV
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d04-x01-y04
Title=$\pi^+\pi^-$ helicity angle for $0.9<m_{\pi^+\pi^-}<1.06\,$GeV
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667959/d05-x01-y01
Title=Minimum $K^+K^-$ mass in $B^+\to K^+K^+K^-$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d05-x01-y02
Title=Maximum $K^+K^-$ mass in $B^+\to K^+K^+K^-$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d05-x01-y03
Title=$K^+K^+$ mass in $B^+\to K^+K^+K^-$
XLabel=$m_{K^+K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d05-x02-y01
Title=Minimum $K^+K^-$ mass in $B^+\to K^+K^+K^-$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d05-x02-y02
Title=Maximum $K^+K^-$ mass in $B^+\to K^+K^+K^-$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667959/d06-x01-y01
Title=Minimum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($m^2_{K^+K^-,\max}<5\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d06-x01-y02
Title=Minimum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($5<m^2_{K^+K^-,\max}<10\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d06-x01-y03
Title=Minimum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($10<m^2_{K^+K^-,\max}<15\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d06-x01-y04
Title=Minimum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($15<m^2_{K^+K^-,\max}<20\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d06-x01-y05
Title=Minimum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($m^2_{K^+K^-,\max}>20\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667959/d06-x02-y01
Title=Maximum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($m^2_{K^+K^-,\min}<1.05\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d06-x02-y02
Title=Maximum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($1.05<m^2_{K^+K^-,\min}<5\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d06-x02-y03
Title=Maximum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($5<m^2_{K^+K^-,\min}<10\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d06-x02-y04
Title=Maximum $K^+K^-$ mass in $B^+\to K^+K^+K^-$ ($m^2_{K^+K^-,\min}>10\,\text{GeV}^2$)
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2005_I667959/d07-x01-y01
Title=$K^+K^-$ helicity angle for $m^2_{K^+K^-,\min}<1.05\,\text{GeV}^2$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2005_I667959/d07-x01-y02
Title=$K^+K^-$ helicity angle for $1.05<m^2_{K^+K^-,\min}<3\,\text{GeV}^2$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
