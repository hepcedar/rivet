Name: BESII_2008_I802372
Year: 2008
Summary: Cross section for $D\bar{D}$ production near the $\psi(3770)$ resonance
Experiment: BESII
Collider: BEPC
InspireID: 802372
Status: VALIDATED NOHEPDATA SINGLEWEIGHT
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 668 (2008) 263-267
RunInfo: e+ e- -> hadrons
Beams: [e+, e-]
Energies : [3.747, 3.755, 3.76, 3.763, 3.765, 3.767, 3.769, 3.771, 3.773, 3.775, 3.777, 3.781, 3.786, 3.792]
Options:
 - ENERGY=*
Description:
  'Measurement of the cross section for $D\bar{D}$ production near the $\psi(3770)$ resonance by BESII.
   Beam energy must be specified as analysis option "ENERGY" when rivet-merging samples.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BES:2008rkr
BibTeX: '@article{BES:2008rkr,
    author = "Ablikim, M. and others",
    collaboration = "BES",
    title = "{Measurements of the line shapes of D anti-D production and the ratio of the production rates of D+ D- and D0 anti-D0 in e+ e- annihilation at psi(3770) resonance}",
    doi = "10.1016/j.physletb.2008.08.067",
    journal = "Phys. Lett. B",
    volume = "668",
    pages = "263--267",
    year = "2008"
}
'
