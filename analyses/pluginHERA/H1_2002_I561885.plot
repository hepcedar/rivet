BEGIN PLOT /H1_2002_I561885/d02-x01-y01
Title=$D^*$ production in DIS (H1)
LogY=0
XLabel=$W$ [GeV]
YLabel=$d\sigma (ep \rightarrow eD^* X)/dW$
END PLOT

BEGIN PLOT /H1_2002_I561885/d03-x01-y01
Title=$D^*$ production in DIS (H1)
LogY=1
LogX=1
XMin=1.
XLabel=$p_{tD^*}$ [GeV]
YLabel=$d\sigma (ep \rightarrow eD^* X)/d p_t$
END PLOT

BEGIN PLOT /H1_2002_I561885/d04-x01-y01
Title=$D^*$ production in DIS (H1)
LogY=0
XLabel=$\log_{10}(x)$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/dlog_{10} x$
END PLOT

BEGIN PLOT /H1_2002_I561885/d05-x01-y01
Title=$D^*$ production in DIS (H1)
LogY=0
XLabel=$\eta_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/d \eta $ [nb]
END PLOT

BEGIN PLOT /H1_2002_I561885/d06-x01-y01
Title=$D^*$ production in DIS (H1)
LogY=1
XLabel=$Q^2$ [GeV$^2$]
YLabel=$d\sigma (ep \rightarrow eD^* X)/dQ^2$
END PLOT

BEGIN PLOT /H1_2002_I561885/d07-x01-y01
Title=$D^*$ production in DIS (H1)
LogY=0
XLabel=$z_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/dz_{D^*}$
END PLOT

BEGIN PLOT /H1_2002_I561885/d08-x01-y01
Title=$D^*$ production in DIS (H1) $ -1.5 < \eta_{D^*} < -0.5 $
LogY=1
LogX=1
XLabel=$Q^2$ [GeV$^2$]
YLabel=$d\sigma (ep \rightarrow eD^* X)/dQ^2$
END PLOT
BEGIN PLOT /H1_2002_I561885/d08-x01-y02
Title=$D^*$ production in DIS (H1) $ -0.5 < \eta_{D^*} < 0.5 $
LogY=1
LogX=1
XLabel=$Q^2$ [GeV$^2$]
YLabel=$d\sigma (ep \rightarrow eD^* X)/dQ^2$
END PLOT
BEGIN PLOT /H1_2002_I561885/d08-x01-y03
Title=$D^*$ production in DIS (H1) $ 0.5 < \eta_{D^*} < 1.5 $
LogY=1
LogX=1
XLabel=$Q^2$ [GeV$^2$]
YLabel=$d\sigma (ep \rightarrow eD^* X)/dQ^2$
END PLOT

BEGIN PLOT /H1_2002_I561885/d09-x01-y01
Title=$D^*$ production in DIS (H1) $1.5 < p_{tD^*} < 4$ GeV
LogY=1
LogX=1
XLabel=$Q^2$ [GeV$^2$]
YLabel=$d\sigma (ep \rightarrow eD^* X)/dQ^2$
END PLOT
BEGIN PLOT /H1_2002_I561885/d09-x01-y02
Title=$D^*$ production in DIS (H1) $4 < p_{tD^*} < 10$ GeV
LogY=1
LogX=1
XLabel=$Q^2$ [GeV$^2$]
YLabel=$d\sigma (ep \rightarrow eD^* X)/dQ^2$
END PLOT


BEGIN PLOT /H1_2002_I561885/d10-x01-y01
Title=$D^*$ production in DIS (H1) $ z_D < 0.25 $
LogY=0
XLabel=$\eta_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/d\eta_{D^*}$
END PLOT
BEGIN PLOT /H1_2002_I561885/d10-x01-y02
Title=$D^*$ production in DIS (H1) $0.25 < z_D < 0.5 $
LogY=0
XLabel=$\eta_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/d\eta_{D^*}$
END PLOT
BEGIN PLOT /H1_2002_I561885/d10-x01-y03
Title=$D^*$ production in DIS (H1) $0.5 < z_D < 1. $
LogY=0
XLabel=$\eta_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/d\eta_{D^*}$
END PLOT

BEGIN PLOT /H1_2002_I561885/d11-x01-y01
Title=$D^*$ production in DIS (H1) $1.5 < p_{tD^*} < 2.5$ GeV
LogY=0
XLabel=$\eta_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/d \eta $ [nb]
END PLOT
BEGIN PLOT /H1_2002_I561885/d11-x01-y02
Title=$D^*$ production in DIS (H1) $2.5 < p_{tD^*} < 4$ GeV
LogY=0
XLabel=$\eta_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/d \eta $ [nb]
END PLOT
BEGIN PLOT /H1_2002_I561885/d11-x01-y03
Title=$D^*$ production in DIS (H1) $4 < p_{tD^*} < 10$ GeV
LogY=0
XLabel=$\eta_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/d \eta $ [nb]
END PLOT

BEGIN PLOT /H1_2002_I561885/d12-x01-y01
Title=$D^*$ production in DIS (H1) $1.5 < p_{tD^*} < 2.5$ GeV
LogY=0
XLabel=$z_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/dz_{D^*}$
END PLOT
BEGIN PLOT /H1_2002_I561885/d12-x01-y02
Title=$D^*$ production in DIS (H1) $2.5 < p_{tD^*} < 4$ GeV
LogY=0
XLabel=$z_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/dz_{D^*}$
END PLOT
BEGIN PLOT /H1_2002_I561885/d12-x01-y03
Title=$D^*$ production in DIS (H1) $4 < p_{tD^*} < 10$ GeV
LogY=0
XLabel=$z_{D^*}$  
YLabel=$d\sigma (ep \rightarrow eD^* X)/dz_{D^*}$
END PLOT

# ... add more histograms as you need them ...
