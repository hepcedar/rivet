BEGIN PLOT /CLEO_2005_I679349/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^0\to \pi^+\pi^-\pi^0$
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2005_I679349/d01-x01-y03
Title=$\pi^-\pi^0$ mass distribution in $D^0\to \pi^-\pi^-\pi^0$
XLabel=$m^2_{\pi^-\pi^0}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^-\pi^0}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2005_I679349/d01-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $D^0\to \pi^+\pi^-\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /CLEO_2005_I679349/dalitz
Title=Dalitz plot for $D^0\to \pi^+\pi^-\pi^0$
YLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
XLabel=$m^2_{\pi^+\pi^0}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\pi^-}/{\rm d}m^2_{\pi^+\pi^0}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
