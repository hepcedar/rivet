BEGIN PLOT /LHCB_2015_I1392456/d[01,02,03,11,12,13]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 7 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d01-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d01-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d01-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 7 TeV ($4.0<y<4.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d11-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d11-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d11-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d11-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d11-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 7 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d02-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d02-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d02-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 7 TeV ($4.0<y<4.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d12-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d12-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d12-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d12-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d12-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d03-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d03-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 7 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d03-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 7 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d03-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 7 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d03-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 7 TeV ($4.0<y<4.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d13-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d13-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d13-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d13-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d13-x01-y05
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d[04,14]
XLabel=$p_\perp$ [GeV]
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}p_\perp$ [pb/GeV]
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d04-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d04-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d04-x01-y03
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d14-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d14-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d14-x01-y03
Title=$\mathrm{d}\sigma/\mathrm{d}p_\perp$ for $\Upsilon(3S)$ at 8 TeV
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d[05,15]
XLabel=$y$
YLabel=$\mathrm{Br}(\mu^+\mu^-)\mathrm{d}\sigma/\mathrm{d}y$ [nb]
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d05-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(1S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d05-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(2S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d05-x01-y03
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(3S)$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d15-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(1S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d15-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(2S)$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d15-x01-y03
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for $\Upsilon(3S)$ at 8 TeV
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d[06,16]
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d06-x01-y01
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d06-x01-y02
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d06-x01-y03
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d06-x01-y04
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d06-x01-y05
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($4.0<y<4.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d16-x01-y01
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d16-x01-y02
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d16-x01-y03
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d16-x01-y04
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d16-x01-y05
Title=Double differential ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d[07,17]
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d07-x01-y01
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d07-x01-y02
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d07-x01-y03
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d07-x01-y04
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d07-x01-y05
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 7 TeV ($4.0<y<4.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d17-x01-y01
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d17-x01-y02
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d17-x01-y03
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d17-x01-y04
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d17-x01-y05
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d[08,18]
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(2S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d08-x01-y01
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 7 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d08-x01-y02
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 7 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d08-x01-y03
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 7 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d08-x01-y04
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 7 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d08-x01-y05
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 7 TeV ($4.0<y<4.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d18-x01-y01
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 8 TeV ($2.0<y<2.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d18-x01-y02
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 8 TeV ($2.5<y<3.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d18-x01-y03
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 8 TeV ($3.0<y<3.5$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d18-x01-y04
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 8 TeV ($3.5<y<4.0$) 
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d18-x01-y05
Title=Double differential ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ at 8 TeV ($4.0<y<4.5$) 
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d[09,19]-x01-y01
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d[09,19]-x01-y02
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d[09,19]-x01-y03
XLabel=$p_\perp$ [GeV]
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(2S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d09-x01-y01
Title=Ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ w.r.t $p_\perp$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d09-x01-y02
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ w.r.t $p_\perp$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d09-x01-y03
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ w.r.t $p_\perp$ at 7 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d19-x01-y01
Title=Ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ w.r.t $p_\perp$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d19-x01-y02
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ w.r.t $p_\perp$ at 8 TeV
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d19-x01-y03
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ w.r.t $p_\perp$ at 8 TeV
END PLOT

BEGIN PLOT /LHCB_2015_I1392456/d10-x01-y01
Title=Ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ w.r.t $y$ at 7 TeV
XLabel=$y$
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d10-x01-y02
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ w.r.t $y$ at 7 TeV
XLabel=$y$
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d10-x01-y03
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ w.r.t $y$ at 7 TeV
XLabel=$y$
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(2S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d20-x01-y01
Title=Ratio of $\Upsilon(2S)$ w.r.t $\Upsilon(1S)$ w.r.t $y$ at 8 TeV
XLabel=$y$
YLabel=$N(\Upsilon(2S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d20-x01-y02
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(1S)$ w.r.t $y$ at 8 TeV
XLabel=$y$
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(1S)\to\mu^+\mu^-)$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2015_I1392456/d20-x01-y03
Title=Ratio of $\Upsilon(3S)$ w.r.t $\Upsilon(2S)$ w.r.t $y$ at 8 TeV
XLabel=$y$
YLabel=$N(\Upsilon(3S)\to\mu^+\mu^-)/N(\Upsilon(2S)\to\mu^+\mu^-)$
LogY=0
END PLOT
