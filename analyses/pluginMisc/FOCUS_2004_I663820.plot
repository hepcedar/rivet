BEGIN PLOT /FOCUS_2004_I663820/d01-x01-y01
Title=$K^+K^-$ mass distribution in $D^0\to K+K^-\pi^+\pi^-$
XLabel=$m_{K^+K^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2004_I663820/d01-x01-y02
Title=$\pi^+\pi^-$ mass distribution in $D^0\to K+K^-\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2004_I663820/d01-x01-y03
Title=$K^\pm\pi^\mp$ mass distribution in $D^0\to K+K^-\pi^+\pi^-$
XLabel=$m_{K^\pm\pi^\mp}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^\pm\pi^\mp}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /FOCUS_2004_I663820/d01-x01-y04
Title=$K^\pm\pi^\pm$ mass distribution in $D^0\to K+K^-\pi^+\pi^-$
XLabel=$m_{K^\pm\pi^\pm}$ [$\text{GeV}$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^\pm\pi^\pm}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
