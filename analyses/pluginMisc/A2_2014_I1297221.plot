BEGIN PLOT /A2_2014_I1297221/d01-x01
XLabel=$m^2_{\gamma\gamma}$ [$\text{GeV}^2$]
YLabel=$\text{d}\Gamma(\eta\to\pi^0\gamma\gamma)/\text{d}m^2_{\gamma\gamma}$ [$\text{eV}\text{GeV}^{-2}$]
END PLOT

BEGIN PLOT /A2_2014_I1297221/d01-x01-y01
Title=Partial Width for $\eta\to\pi^0\gamma\gamma$ w.r.t. $m^2_{\gamma\gamma}$ (2007 run)
END PLOT
BEGIN PLOT /A2_2014_I1297221/d01-x01-y02
Title=Partial Width for $\eta\to\pi^0\gamma\gamma$ w.r.t. $m^2_{\gamma\gamma}$ (2009 run)
END PLOT
BEGIN PLOT /A2_2014_I1297221/d01-x01-y03
Title=Partial Width for $\eta\to\pi^0\gamma\gamma$ w.r.t. $m^2_{\gamma\gamma}$ (2007+2009 runs)
END PLOT
