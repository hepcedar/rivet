BEGIN PLOT /CMS_2022_I1870319/d01-x01-y01
Title=Prompt J/$\psi$ fragmentation function 
XLabel=$z$
YLabel=$\frac1\sigma\mathrm{d}\sigma/\mathrm{d}z$
LogY=0
END PLOT
BEGIN PLOT /CMS_2022_I1870319/d02-x01-y01
Title=Prompt J/$\psi$ fragmentation function 
XLabel=$z$
YLabel=$\mathrm{d}\sigma/\mathrm{d}z$ [nb]
LogY=0
END PLOT