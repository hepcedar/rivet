BEGIN PLOT /BESIII_2023_I2721091/d01-x01-y01
Title=$\Sigma^+K^0_S$ mass distribution in $J/\psi\to \bar{p}\Sigma^+K^0_S$ +c.c.
XLabel=$m_{\Sigma^+K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^+K^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2721091/d01-x01-y02
Title=$\bar{p}K^0_S$ mass distribution in $J/\psi\to \bar{p}\Sigma^+K^0_S$ +c.c.
XLabel=$m_{\bar{p}K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}K^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2721091/d01-x01-y03
Title=$\bar{p}\Sigma^+$ mass distribution in $J/\psi\to \bar{p}\Sigma^+K^0_S$ +c.c.
XLabel=$m_{\bar{p}\Sigma^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\Sigma^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
