BEGIN PLOT /CLEOII_1996_I415408/d01-x01-y01
Title=$K^0_S\pi^-$ mass in $\tau\to K^0_S\pi^-\nu_\tau$
XLabel=$m_{K^0_S\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1996_I415408/d02-x01-y01
Title=$K^0_SK^-$ mass in $\tau\to K^0_SK^-\nu_\tau$
XLabel=$m_{K^0_SK^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1996_I415408/d03-x01-y01
Title=$K^0_S\pi^-\pi^0$ mass in $\tau\to K^0_S\pi^-\pi^0\nu_\tau$
XLabel=$m_{K^0_S\pi^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1996_I415408/d04-x01-y01
Title=$K^0_SK^-\pi^0$ mass in $\tau\to K^0_SK^-\pi^0\nu_\tau$
XLabel=$m_{K^0_SK^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1996_I415408/d05-x01-y01
Title=$K^-\pi^0$ mass in $\tau\to K^0_SK^-\pi^0\nu_\tau$
XLabel=$m_{K^-\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1996_I415408/d05-x01-y02
Title=$K^0_S\pi^0$ mass in $\tau\to K^0_SK^-\pi^0\nu_\tau$
XLabel=$m_{K^0_S\pi^0}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^0}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1996_I415408/d06-x01-y01
Title=$K^0_SK^0_S\pi^-$ mass in $\tau\to K^0_SK^0_S\pi^-\nu_\tau$
XLabel=$m_{K^0_SK^0_S\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_1996_I415408/d07-x01-y01
Title=$K^0_S\pi^-$ mass in $\tau\to K^0_SK^0_S\pi^-\nu_\tau$
XLabel=$m_{K^0_S\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
