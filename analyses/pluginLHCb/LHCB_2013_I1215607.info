Name: LHCB_2013_I1215607
Year: 2013
Summary: $B_s^0$ fraction at 7 TeV
Experiment: LHCB
Collider: LHC
InspireID: 1215607
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - JHEP 04 (2013) 001
RunInfo: bottom hadron production
Beams: [p+, p+]
Energies: [7000]
Description:
  'Measurement of the fraction of  $B_s^0$ production at 7 TeV by the LHCb collaboration.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: LHCb:2013vfg
BibTeX: '@article{LHCb:2013vfg,
    author = "Aaij, R and others",
    collaboration = "LHCb",
    title = "{Measurement of the fragmentation fraction ratio $f_{s}/f_{d}$ and its dependence on $B$ meson kinematics}",
    eprint = "1301.5286",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-PH-EP-2013-006, LHCB-PAPER-2012-037",
    doi = "10.1007/JHEP04(2013)001",
    journal = "JHEP",
    volume = "04",
    pages = "001",
    year = "2013"
}'
