BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y01
Title=$\omega\phi$ mass distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$m_{\omega\phi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\omega\phi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y02
Title=$\gamma\omega$ mass distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$m_{\gamma\omega}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\gamma\omega}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y03
Title=$\gamma\phi$ mass distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$m_{\gamma\phi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\gamma\phi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y04
Title=$\cos\theta_\gamma$ distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y05
Title=$\cos\theta_\omega$ distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$\cos\theta_\omega$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\omega$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y06
Title=$\cos\theta_\phi$ distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$\cos\theta_\phi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\phi$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y07
Title=$\cos\theta_K$ distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_K$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y08
Title=$\phi_\phi$ distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$\phi_\phi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_\phi$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1203841/d01-x01-y09
Title=$\chi$ distribution in $J/\psi\to\gamma\omega\phi$
XLabel=$\chi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\chi$
LogY=0
END PLOT
