BEGIN PLOT /BELLE_2021_I1851126/d01-x01-y01
Title=$\cos\theta$ for $\Xi_c^0\to\Xi^-\pi^+$ 
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1851126/d01-x01-y02
Title=$\cos\theta$ for $\bar\Xi_c^0\to\bar\Xi^+\pi^-$
XLabel=$\cos\theta$
YLabel=$1/N\mathrm{d}N/\mathrm{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1851126/d02-x01-y01
Title=$\alpha$ for $\Xi_c^0\to\Xi^-\pi^+$
YLabel=$\alpha$
YMax=0
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1851126/d02-x01-y02
Title=$\alpha$ for $\bar\Xi_c^0\to\bar\Xi^+\pi^-$
YLabel=$\alpha$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1851126/d02-x01-y03
Title=Average $|\alpha|$ for  $\Xi_c^0\to\Xi^-\pi^+$ and $\bar\Xi_c^0\to\bar\Xi^+\pi^-$
YLabel=$|\alpha|$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1851126/d02-x01-y04
Title=$\mathcal{A}_{\text{CP}}$  $\Xi_c^0\to\Xi^-\pi^+$ and $\bar\Xi_c^0\to\bar\Xi^+\pi^-$
YLabel=$\mathcal{A}_{\text{CP}}$
LogY=0
END PLOT


