BEGIN PLOT /BABAR_2005_I664717/d01-x01-y01
Title=$\omega$ helicity angle in $B^+\to\omega \rho^+$
XLabel=$|\cos\theta|$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}|\cos\theta|$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2005_I664717/d01-x01-y02
Title=$\rho$ helicity angle in $B^+\to\omega \rho^+$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
