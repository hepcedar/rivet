BEGIN PLOT /ATLAS_2024_I2809112/d71
Title=$e\mu+\geq 2b$
XLabel=$N_{b\mathrm{-jets}}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} N_{b\mathrm{-jet}}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d95
Title=$e\mu+\geq 3b+\geq 1 light$-jet
XLabel=$N_\mathrm{light-jets}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} N_\mathrm{light-jet}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d134
Title=$e\mu+\geq 4b+\geq 1 light$-jet
XLabel=$N_\mathrm{light-jets}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} N_\mathrm{light-jet}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d72
Title=$e\mu+\geq 3b$
XLabel=$H_\mathrm{T}^\mathrm{had}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} H_\mathrm{T}^\mathrm{had}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d73
Title=$e\mu+\geq 3b$
XLabel=$H_\mathrm{T}^\mathrm{all}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} H_\mathrm{T}^\mathrm{all}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d102
Title=$e\mu+\geq 4b$
XLabel=$H_\mathrm{T}^\mathrm{had}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} H_\mathrm{T}^\mathrm{had}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d103
Title=$e\mu+\geq 4b$
XLabel=$H_\mathrm{T}^\mathrm{all}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} H_\mathrm{T}^\mathrm{all}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d76
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(b_1)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_1)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d78
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d80
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(b_3)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_3)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d82
Title=$e\mu+\geq 3b$
XLabel=$|\eta(b_1)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_1)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d84
Title=$e\mu+\geq 3b$
XLabel=$|\eta(b_2)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_2)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d86
Title=$e\mu+\geq 3b$
XLabel=$|\eta_\mathrm{T}(b_3)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta_\mathrm{T}(b_3)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/106
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_1)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_1)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d108
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d110
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_3)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_3)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d112
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_4)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_4)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d114
Title=$e\mu+\geq 4b$
XLabel=$|\eta(b_1)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_1)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d116
Title=$e\mu+\geq 4b$
XLabel=$|\eta(b_2)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_2)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d118
Title=$e\mu+\geq 4b$
XLabel=$|\eta_\mathrm{T}(b_3)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta_\mathrm{T}(b_3)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d120
Title=$e\mu+\geq 4b$
XLabel=$|\eta_\mathrm{T}(b_4)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta_\mathrm{T}(b_4)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d77
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(b_1^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_1^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d79
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(b_2^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_2^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d81
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(b_1^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_1^\mathrm{add})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d83
Title=$e\mu+\geq 3b$
XLabel=$|\eta(b_1^\mathrm{top})|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_1^\mathrm{top})|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d85
Title=$e\mu+\geq 3b$
XLabel=$|\eta(b_2^\mathrm{top})|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_2^\mathrm{top})|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d87
Title=$e\mu+\geq 3b$
XLabel=$|\eta(b_1^\mathrm{add})|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_1^\mathrm{add})|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d107
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_1^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_1^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d109
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_2^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_2^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d111
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_1^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_1^\mathrm{add})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d113
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_2^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_2^\mathrm{add})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d115
Title=$e\mu+\geq 4b$
XLabel=$|\eta(b_1^\mathrm{top})|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_1^\mathrm{top})|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d117
Title=$e\mu+\geq 4b$
XLabel=$|\eta(b_2^\mathrm{top})|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_2^\mathrm{top})|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d119
Title=$e\mu+\geq 4b$
XLabel=$|\eta(b_1^\mathrm{add})|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_1^\mathrm{add})|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d121
Title=$e\mu+\geq 4b$
XLabel=$|\eta(b_2^\mathrm{add})|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(b_2^\mathrm{add})|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d88
Title=$e\mu+\geq 3b$
XLabel=$m(b_1b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m(b_1b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d89
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(b_1b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_1b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d94
Title=$e\mu+\geq 3b$
XLabel=$\Delta R(b_1,b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(b_1,b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d122
Title=$e\mu+\geq 4b$
XLabel=$m(b_1b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m(b_1b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d123
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(b_1b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(b_1b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d133
Title=$e\mu+\geq 4b$
XLabel=$\Delta R(b_1,b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(b_1,b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d128
Title=$e\mu+\geq 4b$
XLabel=$m (bb^{\mathrm{min}\Delta R})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m (bb^{\mathrm{min}\Delta R})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d129
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(bb^{\mathrm{min}\Delta R})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(bb^{\mathrm{min}\Delta R})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d132
Title=$e\mu+\geq 4b$
XLabel=min$\Delta R(bb)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \mathrm{min}\Delta R(bb)}$
END PLOT


BEGIN PLOT /ATLAS_2024_I2809112/d90
Title=$e\mu+\geq 3b$
XLabel=$m (bb^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m (bb^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d91
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T} (bb^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T} (bb^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d124
Title=$e\mu+\geq 4b$
XLabel=$m (bb^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m (bb^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d125
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T} (bb^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T} (bb^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d130
Title=$e\mu+\geq 4b$
XLabel=$m (bb^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m (bb^\mathrm{add})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d131
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T} (bb^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T} (bb^\mathrm{add})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d101
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(\mathrm{light-jet}_1)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(\mathrm{light-jet}_1)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d100
Title=$e\mu+\geq 3b$
XLabel=$|\eta(\mathrm{light-jet}_1)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(\mathrm{light-jet}_1)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d140
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(\mathrm{light-jet}_1)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T}(\mathrm{light-jet}_1)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d139
Title=$e\mu+\geq 4b$
XLabel=$|\eta(\mathrm{light-jet}_1)|$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} |\eta(\mathrm{light-jet}_1)|}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d75
Title=$e\mu+\geq 3b$
XLabel=$\Delta \eta_\mathrm{max}^{jj}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta \eta_\mathrm{max}^{jj}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d74
Title=$e\mu+\geq 3b$
XLabel=$\Delta R_\mathrm{avg}^{bb}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R_\mathrm{avg}^{bb}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d105
Title=$e\mu+\geq 4b$
XLabel=$\Delta \eta_\mathrm{max}^{jj}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta \eta_\mathrm{max}^{jj}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d104
Title=$e\mu+\geq 4b$
XLabel=$\Delta R_\mathrm{avg}^{bb}$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R_\mathrm{avg}^{bb}}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d92
Title=$e\mu+\geq 3b$
XLabel=$m(e\mu b_1b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m(e\mu b_1b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d93
Title=$e\mu+\geq 3b$
XLabel=$m (e\mu bb^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m (e\mu bb^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d96
Title=$e\mu+\geq 3b$
XLabel=$\Delta R(e\mu bb^\mathrm{top}, b_3)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(e\mu bb^\mathrm{top}, b_3)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d97
Title=$e\mu+\geq 3b$
XLabel=$\Delta R(e\mu bb^\mathrm{top}, b_1^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(e\mu bb^\mathrm{top}, b_1^\mathrm{add})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d98
Title=$e\mu+\geq 3b$
XLabel=$\Delta R (e\mu bb^\mathrm{top}, \mathrm{light-jet}_1)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R (e\mu bb^\mathrm{top}, \mathrm{light-jet}_1)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d99
Title=$e\mu+\geq 3b$
XLabel=$p_\mathrm{T}(\mathrm{light-jet}_1) - p_\mathrm{T}(b_1^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T} (\mathrm{light-jet}_1) - p_\mathrm{T}(b_1^\mathrm{add})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d126
Title=$e\mu+\geq 4b$
XLabel=$m(e\mu b_1b_2)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m(e\mu b_1b_2)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d127
Title=$e\mu+\geq 4b$
XLabel=$m (e\mu bb^\mathrm{top})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} m (e\mu bb^\mathrm{top})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d135
Title=$e\mu+\geq 4b$
XLabel=$\Delta R(e\mu bb^\mathrm{top}, b_3)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(e\mu bb^\mathrm{top}, b_3)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d136
Title=$e\mu+\geq 4b$
XLabel=$\Delta R(e\mu bb^\mathrm{top}, b_1^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R(e\mu bb^\mathrm{top}, b_1^\mathrm{add})}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d137
Title=$e\mu+\geq 4b$
XLabel=$\Delta R (e\mu bb^\mathrm{top}, \mathrm{light-jet}_1)$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} \Delta R (e\mu bb^\mathrm{top}, \mathrm{light-jet}_1)}$
END PLOT

BEGIN PLOT /ATLAS_2024_I2809112/d138
Title=$e\mu+\geq 4b$
XLabel=$p_\mathrm{T}(\mathrm{light-jet}_1) - p_\mathrm{T}(b_1^\mathrm{add})$
YLabel=$\frac{1000}{\sigma} \frac{\mathrm{d}\sigma}{\mathrm{d} p_\mathrm{T} (\mathrm{light-jet}_1) - p_\mathrm{T}(b_1^\mathrm{add})}$
END PLOT

