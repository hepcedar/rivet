BEGIN PLOT /BESIII_2023_I2634735/
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2634735/d01-x01-y01
Title=$\alpha_\psi$ for $\psi(2S)\to \Xi^0\bar\Xi^0$
YLabel=$\alpha_\psi$
END PLOT
BEGIN PLOT /BESIII_2023_I2634735/d01-x01-y02
Title=$\Delta\Phi$ for $\psi(2S)\to \Xi^0\bar\Xi^0$
YLabel=$\Delta\Phi$
END PLOT

BEGIN PLOT /BESIII_2023_I2634735/d01-x01-y03
Title=$\alpha_\Xi$ for $\Xi^0\to\Lambda\pi^0$
YLabel=$\alpha_\Xi$
YMax=0
YMin=-0.7
END PLOT
BEGIN PLOT /BESIII_2023_I2634735/d01-x01-y05
Title=$\bar{\alpha}_\Xi$ for $\bar\Xi^0\to\bar\Lambda\pi^0$
YLabel=$\bar{\alpha}_\Xi$
END PLOT
