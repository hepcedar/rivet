BEGIN PLOT /CLEOII_2004_I647288/d01-x01-y01
Title=$\langle E_\ell\rangle$ vs $E^\text{min}_\ell$
YLabel=$\langle E_\ell\rangle$ [$\text{GeV}^2$]
XLabel= $E^\text{min}_\ell$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647288/d01-x01-y02
Title=$\langle E^2_\ell\rangle$ vs $E^\text{min}_\ell$
YLabel=$\langle E^2_\ell\rangle$ [$\text{GeV}^2$]
XLabel= $E^\text{min}_\ell$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647288/d01-x01-y03
Title=$\langle E^2_\ell -\langle E_\ell\rangle\rangle$ vs $E^\text{min}_\ell$
YLabel=$\langle E^2_\ell-\langle E_\ell\rangle\rangle$ [$\text{GeV}^2$]
XLabel= $E^\text{min}_\ell$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647288/d02-x01-y01
Title=Primary lepton momentum spectrum
XLabel=$p_\ell$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p_\ell$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647288/d02-x01-y02
Title=Secondary lepton momentum spectrum
XLabel=$p_\ell$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p_\ell$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /CLEOII_2004_I647288/d03-x01-y01
Title=Lepton momentum spectrum for $B\to X_c\ell\nu$
XLabel=$p_\ell$ [GeV]
YLabel=$\text{d}\mathcal{B}/\text{d}p_\ell$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
