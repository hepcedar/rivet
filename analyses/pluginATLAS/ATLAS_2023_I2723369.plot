BEGIN PLOT /ATLAS_2023_I2723369/d01.*
XLabel=$m_{4\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} m_{4\ell}$ [fb/GeV]
END PLOT

BEGIN PLOT /ATLAS_2023_I2723369/d02.*
XLabel=$p_\text{T}^{4\ell}$ [GeV]
YLabel=$\text{d}\sigma / \text{d} p_\text{T}^{4\ell}$ [fb/GeV]
END PLOT
