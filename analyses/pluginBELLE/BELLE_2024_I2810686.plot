BEGIN PLOT /BELLE_2024_I2810686/d01-x01-y01
Title=Differential branching ratio for $B^0\to\pi^-\ell^+\nu_\ell$ vs $q^2$
XLabel=$q^2$~[Gev$^2$]
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}q^2\times10^{-4}$ [Gev$^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2024_I2810686/d01-x01-y02
Title=Differential branching ratio for $B^+\to\rho^0\ell^+\nu_\ell$ vs $q^2$
XLabel=$q^2$~[Gev$^2$]
YLabel=$\mathrm{d}\mathcal{B}/\mathrm{d}q^2\times10^{-4}$ [Gev$^{-1}$]
LogY=0
END PLOT
