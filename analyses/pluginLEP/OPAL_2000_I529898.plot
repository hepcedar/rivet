# BEGIN PLOT /OPAL_2000_I529898/d03-x01-y01
Title=$K^0$ scaled momentum
XLabel=$x_E$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}x_E$
# END PLOT
