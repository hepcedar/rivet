BEGIN PLOT /BESII_2007_I728304/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $\psi(2S)\to\pi^+\pi^-J/\psi$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2007_I728304/d01-x01-y02
Title=$\pi^+\pi^-$ polar angle in $e^+e^-\to\psi(2S)\to\pi^+\pi^-J/\psi$
XLabel=$\cos\theta_{\pi^+\pi^-}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{\pi^+\pi^-}$
LogY=0
END PLOT
BEGIN PLOT /BESII_2007_I728304/d01-x01-y03
Title=$\pi^+$ polar angle in $\pi^+\pi^-$ frame for $e^+e^-\to\psi(2S)\to\pi^+\pi^-J/\psi$
XLabel=$\cos\theta_{\pi^+}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{\pi^+}$
LogY=0
END PLOT
