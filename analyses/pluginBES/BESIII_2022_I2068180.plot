BEGIN PLOT /BESIII_2022_I2068180/d01-x01-y01
Title=$\sigma(e^+e^-\to K^+K^-J/\psi)$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to K^+K^-J/\psi)$/pb
ConnectGaps=1
END PLOT
