BEGIN PLOT /BABAR_2006_I723331/d01-x01-y01
Title=$D^{*\pm}K^0_S$ mass for $B^0\to D^{*+}D^{*-}K^0_S$
XLabel=$m_{D^{*\pm}K^0_S}$ [GeV]
YLabel=$\frac1/\Gamma\text{d}\mathcal{\Gamma}/\text{d}m_{D^{*\pm}K^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
