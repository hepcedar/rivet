BEGIN PLOT /BESIII_2011_I931195/
LogY=0
END PLOT

BEGIN PLOT /BESIII_2011_I931195/d01-x01-y01
Title=$x$ from  $\chi_{c2}$ production with $\chi_{c2}\to\pi^+\pi^-$
YLabel=$x$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d01-x01-y02
Title=$y$ from  $\chi_{c2}$ production with $\chi_{c2}\to\pi^+\pi^-$
YLabel=$y$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d02-x01-y01
Title=$x$ from  $\chi_{c2}$ production with $\chi_{c2}\to K^+K^-$
YLabel=$x$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d02-x01-y02
Title=$y$ from  $\chi_{c2}$ production with $\chi_{c2}\to K^+K^-$
YLabel=$y$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d03-x01-y01
Title=$x$ from  $\chi_{c2}$ production (both channels)
YLabel=$x$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d03-x01-y02
Title=$y$ from  $\chi_{c2}$ production (both channels)
YLabel=$y$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d04-x01-y01
Title=$M1$ from  $\chi_{c2}$ production (both channels)
YLabel=$M1$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d04-x01-y02
Title=$E2$ from  $\chi_{c2}$ production (both channels)
YLabel=$E2$
END PLOT

BEGIN PLOT /BESIII_2011_I931195/d0[5,6]-x0[1,2]-y01
XLabel=$\cos\theta_\gamma$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_\gamma$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d0[5,6]-x01-y02
XLabel=$\cos\theta_\pi$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_\pi$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d0[5,6]-x01-y03
XLabel=$\phi_\pi$ [rad]
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_\pi$ [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d0[5,6]-x02-y02
XLabel=$\cos\theta_K$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_K$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d0[5,6]-x02-y03
XLabel=$\phi_K$ [rad]
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_K$ [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d05-x01-y01
Title=$\cos\theta_\gamma$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to\pi^+\pi^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d05-x01-y02
Title=$\cos\theta_\pi$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to\pi^+\pi^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d05-x01-y03
Title=$\phi_\pi$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to\pi^+\pi^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d05-x02-y01
Title=$\cos\theta_\gamma$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to K^+K^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d05-x02-y02
Title=$\cos\theta_K$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to K^+K^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d05-x02-y03
Title=$\phi_K$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to K^+K^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d06-x01-y01
Title=$\cos\theta_\gamma$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to\pi^+\pi^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d06-x01-y02
Title=$\cos\theta_\pi$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to\pi^+\pi^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d06-x01-y03
Title=$\phi_\pi$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to\pi^+\pi^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d06-x02-y01
Title=$\cos\theta_\gamma$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to K^+K^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d06-x02-y02
Title=$\cos\theta_K$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to K^+K^-$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/d06-x02-y03
Title=$\phi_K$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to K^+K^-$
END PLOT



BEGIN PLOT /BESIII_2011_I931195/h_[1,2]_[1,2]_gamma
XLabel=$\cos\theta_\gamma$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_\gamma$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_[1,2]_1_meson
XLabel=$\cos\theta_\pi$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_\pi$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_[1,2]_1_phi
XLabel=$\phi_\pi$ [rad]
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_\pi$ [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_[1,2]_2_meson
XLabel=$\cos\theta_K$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta_K$
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_[1,2]_2_phi
XLabel=$\phi_K$ [rad]
YLabel=$1/\sigma\text{d}\sigma/\text{d}\phi_K$ [$\text{rad}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_1_1_gamma
Title=$\cos\theta_\gamma$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to\pi^+\pi^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_1_1_meson
Title=$\cos\theta_\pi$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to\pi^+\pi^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_1_1_phi
Title=$\phi_\pi$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to\pi^+\pi^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_1_2_gamma
Title=$\cos\theta_\gamma$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to K^+K^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_1_2_meson
Title=$\cos\theta_K$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to K^+K^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_1_2_phi
Title=$\phi_K$ distribution for $\chi_{c2}$ production with $\chi_{c2}\to K^+K^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_2_1_gamma
Title=$\cos\theta_\gamma$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to\pi^+\pi^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_2_1_meson
Title=$\cos\theta_\pi$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to\pi^+\pi^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_2_1_phi
Title=$\phi_\pi$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to\pi^+\pi^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_2_2_gamma
Title=$\cos\theta_\gamma$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to K^+K^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_2_2_meson
Title=$\cos\theta_K$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to K^+K^-$ (no cuts)
END PLOT
BEGIN PLOT /BESIII_2011_I931195/h_2_2_phi
Title=$\phi_K$ distribution for $\chi_{c0}$ production with $\chi_{c0}\to K^+K^-$ (no cuts)
END PLOT
