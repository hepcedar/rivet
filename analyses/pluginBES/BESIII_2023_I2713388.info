Name: BESIII_2023_I2713388
Year: 2023
Summary: $\psi(2S)\to \Sigma^+\bar\Sigma^-\omega$ and  $\Sigma^+\bar\Sigma^-\phi$
Experiment: BESIII
Collider: BEPC
InspireID: 2713388
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 108 (2023) 9, 092011
 - arXiv:2310.14585 [hep-ex]
RunInfo: Any process producing psi(2S) originally e+e-
Description:
  'Measurement of the mass distributions in the decays $\psi(2S)\to \Sigma^+\bar\Sigma^-\omega$ and  $\Sigma^+\bar\Sigma^-\phi$. The data were read from the plots in the paper. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2023vmv
BibTeX: '@article{BESIII:2023vmv,
    author = "Ablikim, M. and others",
    collaboration = "BESIII",
    title = "{Observation of the \ensuremath{\psi}(3686) decays into \ensuremath{\Sigma}+\ensuremath{\Sigma}\textasciimacron{}-\ensuremath{\omega} and \ensuremath{\Sigma}+\ensuremath{\Sigma}\textasciimacron{}-\ensuremath{\phi}}",
    eprint = "2310.14585",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.108.092011",
    journal = "Phys. Rev. D",
    volume = "108",
    number = "9",
    pages = "092011",
    year = "2023"
}'
