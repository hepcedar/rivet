BEGIN PLOT /BESIII_2019_I1749793/d01-x01-y01
Title=$\phi$ momentum spectrum in $D^+\to \phi X$
XLabel=$p$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p$ [$\text{GeV}^{-1}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1749793/d01-x01-y02
Title=$\phi$ momentum spectrum in $D^0\to \phi X$
XLabel=$p$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p$ [$\text{GeV}^{-1}$
LogY=0
END PLOT
