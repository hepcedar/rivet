BEGIN PLOT /CLEOIII_2004_I654639/d01-x01-y01
Title=$J/\psi$ scaled momentum spectrum in $\Upsilon(1S)$ decay (using $J/\psi\to\mu^+\mu^-$
XLabel=$x$
YLabel=$\text{d}\sigma/\text{d}x$ [pb]
LogY=0
END PLOT
BEGIN PLOT /CLEOIII_2004_I654639/d01-x01-y02
Title=$J/\psi$ scaled momentum spectrum in $\Upsilon(1S)$ decay (using $J/\psi\to e^+e^-$
XLabel=$x$
YLabel=$\text{d}\sigma/\text{d}x$ [pb]
LogY=0
END PLOT
