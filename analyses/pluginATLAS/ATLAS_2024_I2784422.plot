# BEGIN PLOT /ATLAS_2024_I2784422/d..
LogY=0
# END PLOT

##
## Kaon
##

BEGIN PLOT /ATLAS_2024_I2784422/d01-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle$N($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [AWAY]
END PLOT

BEGIN PLOT /ATLAS_2024_I2784422/d03-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle$N($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [TOW.]
END PLOT

BEGIN PLOT /ATLAS_2024_I2784422/d05-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle$N($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [TRA.]
END PLOT

#

BEGIN PLOT /ATLAS_2024_I2784422/d07-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [AWAY]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d09-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d11-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [TRA.]
END PLOT
#

## ##

BEGIN PLOT /ATLAS_2024_I2784422/d13-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($K^{0}_{S}$) / N(ch) [AWAY]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d15-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($K^{0}_{S}$) / N(ch) [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d17-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($K^{0}_{S}$) / N(ch) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d19-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) / $\Sigma$ $p_\mathrm{T}$(ch) [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d21-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) / $\Sigma$ $p_\mathrm{T}$(ch) [TOW.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d23-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) / $\Sigma$ $p_\mathrm{T}$(ch) [TRA.]
END PLOT
#
## ##

BEGIN PLOT /ATLAS_2024_I2784422/d25-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=mean $p_\mathrm{T}$($K^{0}_{S}$) [GeV] [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d27-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=mean $p_\mathrm{T}$($K^{0}_{S}$) [GeV] [TOW.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d29-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=mean $p_\mathrm{T}$($K^{0}_{S}$) [GeV] [TRA.]
END PLOT
#

##
## Lambda
##

BEGIN PLOT /ATLAS_2024_I2784422/d31-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle$N($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [AWAY]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d33-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle$N($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d35-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle$N($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d37-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [AWAY]
END PLOT

BEGIN PLOT /ATLAS_2024_I2784422/d39-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d41-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d43-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N(ch) [AWAY]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d45-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N(ch) [TOW.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d47-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N(ch) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d49-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$(ch) [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d51-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$(ch) [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d53-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$(ch) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d55-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N($K^{0}_{S}$) [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d57-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N($K^{0}_{S}$) [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d59-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N($K^{0}_{S}$) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d61-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d63-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d65-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d67-x01-y01
Title=Away region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=mean $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) [GeV] [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d69-x01-y01
Title=Towards region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=mean $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) [GeV] [TOW.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d71-x01-y01
Title=Transverse region
XLabel=Leading-jet $p_\mathrm{T}$ [GeV]
YLabel=mean $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) [GeV] [TRA.]
END PLOT
#

##
## Kaon
##

BEGIN PLOT /ATLAS_2024_I2784422/d73-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle$N($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d75-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle$N($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d77-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle$N($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d79-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [AWAY]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d81-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d83-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d85-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($K^{0}_{S}$) / N(ch) [AWAY]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d87-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($K^{0}_{S}$) / N(ch) [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d89-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($K^{0}_{S}$) / N(ch) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d91-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) / $\Sigma$ $p_\mathrm{T}$(ch) [AWAY]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d93-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) / $\Sigma$ $p_\mathrm{T}$(ch) [TOW.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d95-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) / $\Sigma$ $p_\mathrm{T}$(ch) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d97-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=mean $p_\mathrm{T}$($K^{0}_{S}$) [GeV] [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d99-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=mean $p_\mathrm{T}$($K^{0}_{S}$) [GeV] [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d101-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=mean $p_\mathrm{T}$($K^{0}_{S}$) [GeV] [TRA.]
END PLOT
#

##
## Lambda
##

BEGIN PLOT /ATLAS_2024_I2784422/d103-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle$N($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d105-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle$N($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d107-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle$N($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d109-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d111-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d113-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\langle\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$)$\rangle$/$\delta\eta$ $\delta\phi$ [GeV] [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d115-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N(ch) [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d117-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N(ch) [TOW.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d119-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N(ch) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d121-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$(ch) [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d123-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$(ch) [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d125-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$(ch) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d127-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N($K^{0}_{S}$) [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d129-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N($K^{0}_{S}$) [TOW.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d131-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=N($\Lambda$+$\bar{\Lambda}$) / N($K^{0}_{S}$) [TRA.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d133-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) [AWAY]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d135-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) [TOW.]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d137-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=$\Sigma$ $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) / $\Sigma$ $p_\mathrm{T}$($K^{0}_{S}$) [TRA.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d139-x01-y01
Title=Away region
XLabel=$N_\textrm{ch,trans}$
YLabel=mean $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) [GeV] [AWAY]
END PLOT
#
BEGIN PLOT /ATLAS_2024_I2784422/d141-x01-y01
Title=Towards region
XLabel=$N_\textrm{ch,trans}$
YLabel=mean $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) [GeV] [TOW.]
END PLOT
#

BEGIN PLOT /ATLAS_2024_I2784422/d143-x01-y01
Title=Transverse region
XLabel=$N_\textrm{ch,trans}$
YLabel=mean $p_\mathrm{T}$($\Lambda$+$\bar{\Lambda}$) [GeV] [TRA.]
END PLOT
#
