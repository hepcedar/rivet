BEGIN PLOT /LHCB_2021_I1888216/d01-x01-y01
Title=$\Lambda_b^0/\bar{\Lambda}_b^0$ Asymmetry at 7 TeV
YLabel=Asymmetry [$\%$]
XLabel=$y$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2021_I1888216/d01-x01-y02
Title=$\Lambda_b^0/\bar{\Lambda}_b^0$ Asymmetry at 8 TeV
YLabel=Asymmetry [$\%$]
XLabel=$y$
LogY=0
END PLOT
BEGIN PLOT /LHCB_2021_I1888216/d02-x01-y01
Title=$\Lambda_b^0/\bar{\Lambda}_b^0$ Asymmetry at 7 TeV
YLabel=Asymmetry [$\%$]
XLabel=$p_\perp$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2021_I1888216/d02-x01-y02
Title=$\Lambda_b^0/\bar{\Lambda}_b^0$ Asymmetry at 8 TeV
YLabel=Asymmetry [$\%$]
XLabel=$p_\perp$ [GeV]
LogY=0
END PLOT
