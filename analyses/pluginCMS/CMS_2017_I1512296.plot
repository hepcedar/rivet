BEGIN PLOT /CMS_2017_I1512296/d[01,02,14,15]
XLabel=$p_\perp$ [GeV]
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [$\mu$b/GeV]
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d[05,18]
XLabel=$y$
YLabel=Br(J/$\psi \rightarrow \mu^{+}\mu^{-}$)$\mathrm{d}\sigma/\mathrm{d}y$ [$\mu$b]
END PLOT

BEGIN PLOT /CMS_2017_I1512296/d02-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($-2.4<y<-1.93$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d02-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($-1.93<y<-1.5$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d02-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($-1.5<y<-0.9$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d02-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($-0.9<y<0$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d01-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($0<y<0.9$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d01-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($0.9<y<1.5$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d01-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($1.5<y<1.93$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d01-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for prompt J/$\psi$  ($1.93<y<2.4$) 
END PLOT

BEGIN PLOT /CMS_2017_I1512296/d15-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($-2.4<y<-1.93$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d15-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($-1.93<y<-1.5$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d15-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($-1.5<y<-0.9$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d15-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($-0.9<y<0$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d14-x01-y04
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($0<y<0.9$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d14-x01-y03
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($0.9<y<1.5$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d14-x01-y02
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($1.5<y<1.93$) 
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d14-x01-y01
Title=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ for non-prompt J/$\psi$  ($1.93<y<2.4$) 
END PLOT


BEGIN PLOT /CMS_2017_I1512296/d05-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for prompt J/$\psi$ ($6.5<p_\perp<10$\,GeV)
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d05-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for prompt J/$\psi$ ($10<p_\perp<30$\,GeV)
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d18-x01-y01
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for non-prompt J/$\psi$ ($6.5<p_\perp<10$\,GeV)
END PLOT
BEGIN PLOT /CMS_2017_I1512296/d18-x01-y02
Title=$\mathrm{d}\sigma/\mathrm{d}y$ for non-prompt J/$\psi$ ($10<p_\perp<30$\,GeV)
END PLOT
