BEGIN PLOT /BESIII_2024_I2739177/d01-x01-y01
Title=Cross section for $e^+e^-\to\Sigma^+\bar{\Sigma}^-$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to\Sigma^+\bar{\Sigma}^-)$ [pb]
ConnectGaps=1
LogY=0
END PLOT
