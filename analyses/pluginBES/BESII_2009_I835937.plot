BEGIN PLOT /BESII_2009_I835937
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma$ [pb]
ConnectGaps=1
LogY=0
END PLOT
BEGIN PLOT /BESII_2009_I835937/d01-x01-y01
Title=Cross section for $e^+e^-\to K^0_SK^-K^-K^+\pi^+$
END PLOT
BEGIN PLOT /BESII_2009_I835937/d01-x01-y02
Title=Cross section for $e^+e^-\to K^0_SK^-\pi^+\eta$
END PLOT
BEGIN PLOT /BESII_2009_I835937/d01-x01-y06
Title=Cross section for $e^+e^-\to K^0_S K^- \rho^+$
END PLOT
BEGIN PLOT /BESII_2009_I835937/d01-x01-y07
Title=Cross section for $e^+e^-\to K^0_SK^-\pi^+\rho^0$
END PLOT
