BEGIN PLOT /LHCB_2023_I2635083/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $B_s^0\to\pi^+\pi^- \psi(2S)$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2023_I2635083/d01-x01-y02
Title=$\pi^+\pi^-$ mass in $B_s^0\to\pi^+\pi^- \chi_{c1}(3872)$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
