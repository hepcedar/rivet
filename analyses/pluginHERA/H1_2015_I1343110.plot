# BEGIN PLOT /H1_2015_I1343110/d01-x01-y01
Title=Photoproduction
YLabel=$\sigma_{\gamma\mathrm{p}}$ [pb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d02-x01-y01
Title= DIS
YLabel=$\sigma_{\mathrm{DIS}}$ [pb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d03-x01-y01
Title=Photoproduction
YLabel=$\sigma_{\gamma\mathrm{p}}/\sigma_{\mathrm{DIS}}$
XLabel=$\sqrt{s}$ [GeV]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d04-x01-y01
Title=DIS
XLabel=$z_{\mathbb{P}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} z_{\mathbb{P}}$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d05-x01-y01
Title=DIS
XLabel=$x_{\mathbb{P}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} x_{\mathbb{P}}$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d06-x01-y01
Title=DIS
XLabel=$y$
YLabel=$\mathrm{d}\sigma / \mathrm{d} y$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d07-x01-y01
Title=DIS
XLabel=$Q^2$ [GeV$^2$]
YLabel=$\mathrm{d}\sigma / \mathrm{d} Q^2$ [pb/GeV$^2$]
LogY=1
LogX=1
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d08-x01-y01
Title=DIS
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} E_{\mathrm{T}}^{\mathrm{jet1}}$  [pb/GeV]
LogY=1
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d09-x01-y01
Title=DIS
XLabel=$M_X$ [GeV$^2$]
YLabel=$\mathrm{d}\sigma / \mathrm{d} M_X$ [pb/GeV$^2$]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d10-x01-y01
Title=DIS
XLabel=$|\Delta\eta|$
YLabel=$\mathrm{d}\sigma / \mathrm{d} |\Delta\eta|$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d11-x01-y01
Title=DIS
XLabel=$\left<\eta\right>$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \left<\eta\right>$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d12-x01-y01
Title=Photoproduction
XLabel=$z_{\mathbb{P}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} z_{\mathbb{P}}$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d13-x01-y01
Title=Photoproduction
XLabel=$x_{\mathbb{P}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} x_{\mathbb{P}}$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d14-x01-y01
Title=Photoproduction
XLabel=$y$
YLabel=$\mathrm{d}\sigma / \mathrm{d} y$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d15-x01-y01
Title=Photoproduction
XLabel=$x_{\gamma}$
YLabel=$\mathrm{d}\sigma / \mathrm{d} x_{\gamma}$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d16-x01-y01
Title=Photoproduction
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d} E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LogY=1
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d17-x01-y01
Title=Photoproduction
XLabel=$M_X$ [GeV$^2$]
YLabel=$\mathrm{d}\sigma / \mathrm{d} M_X$  [pb/GeV$^2$]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d18-x01-y01
Title=Photoproduction
XLabel=$|\Delta\eta|$
YLabel=$\mathrm{d}\sigma / \mathrm{d} |\Delta\eta|$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d19-x01-y01
Title=Photoproduction
XLabel=$\left<\eta\right>$
YLabel=$\mathrm{d}\sigma / \mathrm{d} \left<\eta\right>$ [pb]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d20-x01-y01
Title=Photoproduction/DIS
YLabel=$\sigma_{\gamma\mathrm{p}}/\sigma_{\mathrm{DIS}}$
XLabel=$|\Delta\eta|$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d21-x01-y01
Title=Photoproduction/DIS
YLabel=$\sigma_{\gamma\mathrm{p}}/\sigma_{\mathrm{DIS}}$
XLabel=$y$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d22-x01-y01
Title=Photoproduction/DIS
YLabel=$\sigma_{\gamma\mathrm{p}}/\sigma_{\mathrm{DIS}}$
XLabel=$z_{\mathbb{P}}$
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT

# BEGIN PLOT /H1_2015_I1343110/d23-x01-y01
Title=Photoproduction/DIS
YLabel=$\sigma_{\gamma\mathrm{p}}/\sigma_{\mathrm{DIS}}$
XLabel=$E_T^{\mathrm{jet1}}$ [GeV]
LogY=0
RatioPlotYMin=0.1
RatioPlotYMax=2.2
# END PLOT
