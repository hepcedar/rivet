Name: ATLAS_2024_I2771257
Year: 2024
Summary: Z+b(b) and Z+c at 13 TeV
Experiment: ATLAS
Collider: LHC
InspireID: 2771257
Status: VALIDATED
Authors:
 - Lucrezia Boccardo <lucrezia.boccardo@cern.ch>
 - Federico Sforza <fsforza@cern.ch>
Reentrant: true
References:
 - ATLAS-STDM-2018-43
 - arXiv:2403.15093 [hep-ex]
Keywords:
 - jets
 - zboson
 - wboson
 - vjets
RunInfo:
  pp -> ee/mumu + jets
Luminosity_fb: 140
Beams: [p+, p+]
Energies: [13000]
Options:
- LMODE=EL,MU
Description:
   'This paper presents a measurement of the production cross-section of a $Z$ boson in association with $b$- or $c$-jets,
   in proton–proton collisions at $\sqrt{s} =$13 TeV with the ATLAS experiment at the Large Hadron Collider using data
   corresponding to an integrated luminosity of 140 fb$^{-1}$. Inclusive and differential cross-sections are measured for
   events containing a $Z$ boson decaying into electrons or muons and produced in association with at least one $b$-jet,
   at least one $c$-jet, or at least two $b$-jets with transverse momentum $p_\text{T} > 20$GeV and rapidity $|y| < 2.5$.
   Predictions from several Monte Carlo generators based on next-to-leading-order matrix elements interfaced with a
   parton-shower simulation, with different choices of flavour schemes for initial-state partons, are compared with the
   measured cross-sections. The results are also compared with novel predictions, based on infrared and collinear safe
   jet flavour dressing algorithms. Selected $Z+\geq 1$ $c$-jet observables, optimized for sensitivity to intrinsic-charm,
   are compared with benchmark models with different intrinsic-charm fractions.'
BibKey: ATLAS:2024tnr
BibTeX: '@article{ATLAS:2024tnr,
    author = "Aad, Georges and others",
    collaboration = "ATLAS",
    title = "{Measurements of the production cross-section for a Z boson in association with b- or c-jets in proton\textendash{}proton collisions at $\sqrt{s} = 13$ TeV with the ATLAS detector}",
    eprint = "2403.15093",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    reportNumber = "CERN-EP-2024-081",
    doi = "10.1140/epjc/s10052-024-13159-w",
    journal = "Eur. Phys. J. C",
    volume = "84",
    number = "9",
    pages = "984",
    year = "2024"
}'
ReleaseTests:
 - $A LHC-13-Z-mu-jets
