BEGIN PLOT /BESIII_2023_I2696635/d01-x01-y01
Title=$K^0_s\pi^+$ mass distribution in $D^+\to \pi^+K_S^0\eta$
XLabel=$m_{K^0_s\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_s\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2696635/d01-x01-y02
Title=$K^0_s\eta$ mass distribution in $D^+\to \pi^+K_S^0\eta$
XLabel=$m_{K^0_s\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^0_s\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2696635/d01-x01-y03
Title=$\pi^+\eta$ mass distribution in $D^+\to \pi^+K_S^0\eta$
XLabel=$m_{\pi^+\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
