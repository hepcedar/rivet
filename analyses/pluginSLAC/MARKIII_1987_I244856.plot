BEGIN PLOT /MARKIII_1987_I244856/d01-x01-y01
Title=$e^+e^-\to D^{*+}_sD^-_s$ +c.c. cross section
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to D^{*+}_sD^-_s+\mathrm{c.c})*\mathcal{B}(D^+_s\to\phi\pi^+)$ [pb]
LogY=0
END PLOT
