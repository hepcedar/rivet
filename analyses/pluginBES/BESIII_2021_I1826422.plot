BEGIN PLOT /BESIII_2021_I1826422/d01-x01-y01
LogY=0
ConnectGaps=1
YLabel=$\sigma$ [pb]
XLabel=$\sqrt{s}$ [GeV]
Title=Cross Section for $e^+e^-\to \pi^+\pi^-\pi^0\eta_c$
END PLOT
