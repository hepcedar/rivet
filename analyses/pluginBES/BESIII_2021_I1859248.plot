BEGIN PLOT /BESIII_2021_I1859248/d01-x01-y01
Title=$\sigma(e^+e^-\to \phi\Lambda\bar{\Lambda})$
XLabel=$\sqrt{s}$/GeV
YLabel=$\sigma(e^+e^-\to \phi\Lambda\bar{\Lambda})$/pb
ConnectGaps=1
LogY=0
END PLOT
