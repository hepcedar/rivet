BEGIN PLOT /BESIII_2018_I1705754
LogY=0
END PLOT

BEGIN PLOT /BESIII_2018_I1705754/d01-x01-y01
Title=$\bar{K}^0\pi^-$ mass in $D^0\to \bar{K}^0\pi^- e^+\nu_e$
XLabel=$m_{\bar{K}^0\pi^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{\bar{K}^0\pi^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2018_I1705754/d01-x01-y02
Title=$e^+\nu_e$ mass squared in $D^0\to \bar{K}^0\pi^- e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2018_I1705754/d01-x01-y03
Title=$\cos\theta_e$ in $D^0\to \bar{K}^0\pi^- e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BESIII_2018_I1705754/d01-x01-y04
Title=$\cos\theta_K$ in $D^0\to \bar{K}^0\pi^- e^+\nu_e$
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT

BEGIN PLOT /BESIII_2018_I1705754/d01-x01-y05
Title=$\chi$ in $D^0\to \bar{K}^0\pi^- e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\chi$ [$\mathrm{rad}^{-1}$]
END PLOT
