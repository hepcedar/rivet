# BEGIN PLOT /ZEUS_2001_I568665/d01-x01-y01
Title=
XLabel=$|\cos(\theta^*)|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\cos(\theta^*)|$ [pb]
LogY=0
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d01-x01-y02
Title=
XLabel=$|\cos(\theta^*)|$
YLabel=$\mathrm{d}\sigma / \mathrm{d}|\cos(\theta^*)|$ [pb]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$
LogY=0
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d02-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$-1<\eta^{\mathrm{jet}1}<0$\\$-1<\eta^{\mathrm{jet}2}<0$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d03-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$0<\eta^{\mathrm{jet}1}<1$\\$-1<\eta^{\mathrm{jet}2}<0$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d04-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$0<\eta^{\mathrm{jet}1}<1$\\$0<\eta^{\mathrm{jet}2}<1$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d05-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$1<\eta^{\mathrm{jet}1}<2.4$\\$-1<\eta^{\mathrm{jet}2}<0$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d06-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$1<\eta^{\mathrm{jet}1}<2.4$\\$0<\eta^{\mathrm{jet}2}<1$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d07-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$1<\eta^{\mathrm{jet}1}<2.4$\\$1<\eta^{\mathrm{jet}2}<2.4$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d08-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$-1<\eta^{\mathrm{jet}1}<0$\\$-1<\eta^{\mathrm{jet}2}<0$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d09-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$0<\eta^{\mathrm{jet}1}<1$\\$-1<\eta^{\mathrm{jet}2}<0$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d10-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$0<\eta^{\mathrm{jet}1}<1$\\$0<\eta^{\mathrm{jet}2}<1$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d11-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$1<\eta^{\mathrm{jet}1}<2.4$\\$-1<\eta^{\mathrm{jet}2}<0$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d12-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$1<\eta^{\mathrm{jet}1}<2.4$\\$0<\eta^{\mathrm{jet}2}<1$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d13-x01-y01
Title=
XLabel=$E_{\mathrm{T}}^{\mathrm{jet1}}$ [GeV]
YLabel=$\mathrm{d}\sigma / \mathrm{d}E_{\mathrm{T}}^{\mathrm{jet1}}$ [pb/GeV]
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$1<\eta^{\mathrm{jet}1}<2.4$\\$1<\eta^{\mathrm{jet}2}<2.4$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d14-x01-y01
Title=
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta^{\mathrm{jet2}}$ [pb]
LogY=0
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$-1<\eta^{\mathrm{jet}1}<0$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d15-x01-y01
Title=
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta^{\mathrm{jet2}}$ [pb]
LogY=0
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$0<\eta^{\mathrm{jet}1}<1$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d16-x01-y01
Title=
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta^{\mathrm{jet2}}$ [pb]
LogY=0
LegendTitle=$x_{\gamma}^{\mathrm{obs}} > 0.75$\\$1<\eta^{\mathrm{jet}1}<2.4$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d17-x01-y01
Title=
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta^{\mathrm{jet2}}$ [pb]
LogY=0
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$-1<\eta^{\mathrm{jet}1}<0$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d18-x01-y01
Title=
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta^{\mathrm{jet2}}$ [pb]
LogY=0
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$0<\eta^{\mathrm{jet}1}<1$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d19-x01-y01
Title=
XLabel=$\eta^{\mathrm{jet2}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}\eta^{\mathrm{jet2}}$ [pb]
LogY=0
LegendTitle=$x_{\gamma}^{\mathrm{obs}} < 0.75$\\$1<\eta^{\mathrm{jet}1}<2.4$
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d20-x01-y01
Title=
XLabel=$x_{\gamma}^{\mathrm{obs}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\gamma}^{\mathrm{obs}}$ [pb]
LogY=0
LegendTitle=$14<E_{\mathrm{T}}^{\mathrm{jet1}}<17$ GeV
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d21-x01-y01
Title=
XLabel=$x_{\gamma}^{\mathrm{obs}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\gamma}^{\mathrm{obs}}$ [pb]
LogY=0
LegendTitle=$17<E_{\mathrm{T}}^{\mathrm{jet1}}<25$ GeV
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d22-x01-y01
Title=
XLabel=$x_{\gamma}^{\mathrm{obs}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\gamma}^{\mathrm{obs}}$ [pb]
LogY=0
LegendTitle=$25<E_{\mathrm{T}}^{\mathrm{jet1}}<35$ GeV
YLabelSep=5.5
# END PLOT

# BEGIN PLOT /ZEUS_2001_I568665/d23-x01-y01
Title=
XLabel=$x_{\gamma}^{\mathrm{obs}}$
YLabel=$\mathrm{d}\sigma / \mathrm{d}x_{\gamma}^{\mathrm{obs}}$ [pb]
LogY=0
LegendTitle=$35<E_{\mathrm{T}}^{\mathrm{jet1}}<90$ GeV
YLabelSep=5.5
# END PLOT
