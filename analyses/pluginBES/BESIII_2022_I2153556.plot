BEGIN PLOT /BESIII_2022_I2153556/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $\Lambda_c^+\to \Lambda\pi^+\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2153556/d01-x01-y02
Title=$\Lambda\pi^+$ mass distribution in $\Lambda_c^+\to \Lambda\pi^+\pi^0$
XLabel=$m_{\Lambda\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2153556/d01-x01-y03
Title=$\Lambda\pi^0$ mass distribution in $\Lambda_c^+\to \Lambda\pi^+\pi^0$
XLabel=$m_{\Lambda\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
