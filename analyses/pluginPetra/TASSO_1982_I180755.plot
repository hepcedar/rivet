BEGIN PLOT /TASSO_1982_I180755/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \rho^0\rho^0$
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
