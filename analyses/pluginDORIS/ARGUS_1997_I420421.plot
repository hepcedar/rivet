BEGIN PLOT /ARGUS_1997_I420421
YLabel=$\sigma$ [nb]
XLabel=$\sqrt{s}$ [MeV]
LogY=0
END PLOT
BEGIN PLOT /ARGUS_1997_I420421/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-\pi^0$
END PLOT
BEGIN PLOT /ARGUS_1997_I420421/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-\pi^0$ (non-resonance)
END PLOT
BEGIN PLOT /ARGUS_1997_I420421/d03-x01-y05
Title=Cross section for $\gamma\gamma\to \rho^\pm\pi^\mp$
END PLOT
BEGIN PLOT /ARGUS_1997_I420421/d04-x01-y03
Title=Cross section for $\gamma\gamma\to f_2(\to\pi^+\pi^-)\pi^0$
END PLOT
BEGIN PLOT /ARGUS_1997_I420421/d05-x01-y01
Title=Cross section for $\gamma\gamma\to f_0(980)(\to\pi^+\pi^-)\pi^0$
END PLOT
