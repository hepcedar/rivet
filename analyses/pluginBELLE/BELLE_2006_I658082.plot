BEGIN PLOT /BELLE_2006_I658082/d01-x01-y01
Title=$\bar{p}\pi^-$ mass in $B^-\to\Lambda_c^+\bar{p}\pi^-$ ($m_{\Lambda_c^+\pi^-}>2.6\,$GeV and $m_{\Lambda_c^+\bar{p}}>3.5\,$GeV)
XLabel=$m_{\bar{p}\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2006_I658082/d01-x01-y02
Title=$\Lambda_c^+\bar{p}$ mass in $B^-\to\Lambda_c^+\bar{p}\pi^-$ ($m_{\Lambda_c^+\pi^-}>2.6\,$GeV  $m_{\bar{p}\pi^-}>1.6\,$GeV)
XLabel=$m_{\Lambda_c^+\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2006_I658082/d02-x01-y01
Title=Helicity angle for $\Lambda_c^+\bar{p}$ in $B^-\to\Lambda_c^+\bar{p}\pi^-$  ($m_{\Lambda_c^+\bar{p}}<3.6\,$GeV)
XLabel=$\cos\theta_{\Lambda_c^+\bar{p}}$
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}\cos\theta_{\Lambda_c^+\bar{p}}$
LogY=0
END PLOT
