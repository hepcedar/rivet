BEGIN PLOT /BESIII_2023_I2702517/d01-x01-y01
Title=$\Lambda\eta^\prime(\bar\Lambda\eta^\prime)$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\eta^\prime$ using ($\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{\Lambda\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2702517/d01-x01-y02
Title=$\Lambda\eta^\prime(\bar\Lambda\eta^\prime)$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\eta^\prime$using ($\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{\Lambda\eta^\prime}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\eta^\prime}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2702517/d02-x01-y01
Title=$\Lambda\bar\Lambda$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\eta^\prime$ using ($\eta^\prime\to\gamma\pi^+\pi^-$)
XLabel=$m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2023_I2702517/d02-x01-y02
Title=$\Lambda\bar\Lambda$ mass distribution in $\psi(2S)\to \Lambda\bar{\Lambda}\eta^\prime$using ($\eta^\prime\to\eta\pi^+\pi^-$)
XLabel=$m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda\bar\Lambda}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
