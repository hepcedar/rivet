# BEGIN PLOT /LHCB_2019_I1720413/d0[1-3]-x01-y0[1-4]
XLabel=$p_\mathrm{T}$ [$\mathrm{GeV}/c$]
YLabel=$\mathrm{d}^2\sigma/(\mathrm{d}p_\mathrm{T}\,\mathrm{d}y)$ [$\mu\mathrm{b}/(\mathrm{GeV}/c)$]
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d01-x01-y01
Title=$B^+$, $-4.5 < y < -3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d01-x01-y02
Title=$B^+$, $-3.5 < y < -2.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d01-x01-y03
Title=$B^+$, $1.5 < y < 2.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d01-x01-y04
Title=$B^+$, $2.5 < y < 3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d02-x01-y01
Title=$B^0$, $-4.5 < y < -3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d02-x01-y02
Title=$B^0$, $-3.5 < y < -2.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d02-x01-y03
Title=$B^0$, $1.5 < y < 2.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d02-x01-y04
Title=$B^0$, $2.5 < y < 3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d03-x01-y01
Title=$\mathit{\Lambda}_b^0$, $-4.5 < y < -3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d03-x01-y02
Title=$\mathit{\Lambda}_b^0$, $-3.5 < y < -2.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d03-x01-y03
Title=$\mathit{\Lambda}_b^0$, $1.5 < y < 2.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d03-x01-y04
Title=$\mathit{\Lambda}_b^0$, $2.5 < y < 3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d04-x01-y0[1-3]
XLabel=$y$
YLabel=$\mathrm{d}\sigma/\mathrm{d}y$ [$\mu\mathrm{b}$]
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d04-x01-y01
Title=$B^+$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d04-x01-y02
Title=$B^0$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d04-x01-y03
Title=$\mathit{\Lambda}_b^0$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d05-x01-y0[1-3]
XLabel=$p_\mathrm{T}$ [$\mathrm{GeV}/c$]
YLabel=$R_\mathrm{FB}$
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d05-x01-y01
Title=$B^+$, $2.5 < |y| < 3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d05-x01-y02
Title=$B^0$, $2.5 < |y| < 3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d05-x01-y03
Title=$\mathit{\Lambda}_b^0$, $2.5 < |y| < 3.5$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d06-x01-y0[1-3]
XLabel=$y$
YLabel=$R_\mathrm{FB}$
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d06-x01-y01
Title=$B^+$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $\sqrt{s_\mathrm{NN}}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d06-x01-y02
Title=$B^0$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d06-x01-y03
Title=$\mathit{\Lambda}_b^0$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d0(7|8)-x01-y0[1-2]
XLabel=$p_\mathrm{T}$ [$\mathrm{GeV}/c$]
YLabel=$R_{p\mathrm{Pb}}^{\mathit{\Lambda}^{0}_{b}/B^0}$
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d07-x01-y01
Title=$R_{p\mathrm{Pb}}^{\mathit{\Lambda}_b^0/B^0}$, $2.5 < |y| < 3.5$, $p$Pb at $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d07-x01-y02
Title=$R_{p\mathrm{Pb}}^{\mathit{\Lambda}_b^0/B^0}$, $2.5 < |y| < 3.5$, Pb$p$ at $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d08-x01-y01
Title=$R_{p\mathrm{Pb}}^{\mathit{\Lambda}_b^0/B^0}$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $2.5 < |y| < 3.5$, $p$Pb at $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d08-x01-y02
Title=$R_{p\mathrm{Pb}}^{\mathit{\Lambda}_b^0/B^0}$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $2.5 < |y| < 3.5$, Pb$p$ at $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d0(9|10)-x01-y0[1-2]
XLabel=$p_\mathrm{T}$ [$\mathrm{GeV}/c$]
YLabel=$R_{p\mathrm{Pb}}^{B^+}$
LogY=0
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d09-x01-y01
Title=$R_{p\mathrm{Pb}}^{B^+}$, $2.5 < |y| < 3.5$, $p$Pb at $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d09-x01-y02
Title=$R_{p\mathrm{Pb}}^{B^+}$, $2.5 < |y| < 3.5$, Pb$p$ at $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d10-x01-y01
Title=$R_{p\mathrm{Pb}}^{B^+}$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $2.5 < |y| < 3.5$, $p$Pb at $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

# BEGIN PLOT /LHCB_2019_I1720413/d10-x01-y02
Title=$R_{p\mathrm{Pb}}^{B^+}$, $2 < p_\mathrm{T} < 20\,\mathrm{GeV}/c$, $2.5 < |y| < 3.5$, Pb$p$ at $\sqrt{s_\mathrm{NN}} = 8.16\,\mathrm{TeV}$
# END PLOT

