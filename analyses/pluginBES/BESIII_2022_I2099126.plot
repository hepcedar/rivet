BEGIN PLOT /BESIII_2022_I2099126/d01-x01-y01
Title=$\mu$ for $\bar{\Lambda}\to \bar{p}^-\pi^+$ (N.B. data not corrected)
YLabel=$\mu$
LogY=0
XLabel=$\cos\theta_{\bar{\Lambda}}$
END PLOT

BEGIN PLOT /BESIII_2022_I2099126/d01-x01-y02
Title=$\mu$ for $\Lambda\to n \gamma$ (N.B. data not corrected)
YLabel=$\mu$
LogY=0
XLabel=$\cos\theta_{\Lambda}$
END PLOT

BEGIN PLOT /BESIII_2022_I2099126/d01-x01-y03
Title=$\mu$ for $\Lambda\to p^+\pi^-$ (N.B. data not corrected)
YLabel=$\mu$
LogY=0
XLabel=$\cos\theta_{\Lambda}$
END PLOT

BEGIN PLOT /BESIII_2022_I2099126/d01-x01-y04
Title=$\mu$ for $\bar{\Lambda}\to \bar{n} \gamma$ (N.B. data not corrected)
YLabel=$\mu$
LogY=0
XLabel=$\cos\theta_{\bar{\Lambda}}$
END PLOT

BEGIN PLOT /BESIII_2022_I2099126/d02-x01-y01
Title=$\alpha_\gamma$ for $\Lambda\to n \gamma$
YLabel=$\alpha_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2099126/d02-x01-y02
Title=$\alpha_\gamma$ for $\bar{\Lambda}\to \bar{n} \gamma$
YLabel=$\alpha_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2099126/d02-x01-y03
Title=$\alpha_\gamma$ for $\Lambda\to n \gamma$ and $\bar{\Lambda}\to \bar{n} \gamma$
YLabel=$\alpha_\gamma$
LogY=0
END PLOT