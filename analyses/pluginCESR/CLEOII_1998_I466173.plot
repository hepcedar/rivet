BEGIN PLOT /CLEOII_1998_I466173/d01-x01-y01
Title=Helicity angle in $D_s^+\to \rho^+\eta^\prime$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
