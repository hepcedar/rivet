BEGIN PLOT /CMS_2012_I944755/d01-x01-y01
Title=Prompt $J\/\psi$ transverse momentum $0 < |y| < 0.9$
XLabel=$p^{J\/\psi}_\perp$ [GeV]
YLabel=Br($J\/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d02-x01-y01
Title=Prompt $J/\psi$ transverse momentum $0.9 < |y| < 1.2$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d03-x01-y01
Title=Prompt $J/\psi$ transverse momentum $1.2 < |y| < 1.6$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d04-x01-y01
Title=Prompt $J/\psi$ transverse momentum $1.6 < |y| < 2.1$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d05-x01-y01
Title=Prompt $J/\psi$ transverse momentum $2.1 < |y| < 2.4$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2012_I944755/d01-x01-y02
Title=Non-prompt $J\/\psi$ transverse momentum $0 < |y| < 0.9$
XLabel=$p^{J\/\psi}_\perp$ [GeV]
YLabel=Br($J\/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d02-x01-y02
Title=Non-prompt $J/\psi$ transverse momentum $0.9 < |y| < 1.2$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d03-x01-y02
Title=Non-prompt $J/\psi$ transverse momentum $1.2 < |y| < 1.6$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d04-x01-y02
Title=Non-prompt $J/\psi$ transverse momentum $1.6 < |y| < 2.1$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d05-x01-y02
Title=Non-prompt $J/\psi$ transverse momentum $2.1 < |y| < 2.4$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2012_I944755/d06-x01-y01
Title=Prompt $\psi(2S)$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d07-x01-y01
Title=Prompt $\psi(2S)$ transverse momentum $1.2 < |y| < 1.6$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d08-x01-y01
Title=Prompt $\psi(2S)$ transverse momentum $1.6 < |y| < 2.4$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT

BEGIN PLOT /CMS_2012_I944755/d06-x01-y02
Title=Non-prompt $\psi(2S)$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d07-x01-y02
Title=Non-prompt $\psi(2S)$ transverse momentum $1.2 < |y| < 1.6$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT
BEGIN PLOT /CMS_2012_I944755/d08-x01-y02
Title=Non-prompt $\psi(2S)$ transverse momentum $1.6 < |y| < 2.4$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [nb/GeV]
END PLOT


BEGIN PLOT /CMS_2012_I944755/d17-x01-y01
Title=Ratio of prompt $\psi(2S)$ to $J/\psi$ vs$p_\perp$ $0 < |y| < 1.2$
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d18-x01-y01
Title=Ratio of prompt $\psi(2S)$ to $J/\psi$ vs$p_\perp$ $1.2 < |y| < 1.6$
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d19-x01-y01
Title=Ratio of prompt $\psi(2S)$ to $J/\psi$ vs$p_\perp$ $1.6 < |y| < 2.4$
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d20-x01-y01
Title=Ratio of prompt $\psi(2S)$ to $J/\psi$ vs $p_\perp$ $0 < |y| < 2.4$
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d17-x01-y02
Title=Ratio of non-prompt $\psi(2S)$ to $J/\psi$ vs $p_\perp$ $0 < |y| < 1.2$
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d18-x01-y02
Title=Ratio of non-prompt $\psi(2S)$ to $J/\psi$ vs $p_\perp$ $1.2 < |y| < 1.6$
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d19-x01-y02
Title=Ratio of non-prompt $\psi(2S)$ to $J/\psi$ vs $p_\perp$ $1.6 < |y| < 2.4$
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d20-x01-y02
Title=Ratio of non-prompt $\psi(2S)$ to $J/\psi$ vs $p_\perp$ $0 < |y| < 2.4$
XLabel=$p_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT

BEGIN PLOT /CMS_2012_I944755/d21-x01-y01
Title=Non-prompt $J/\psi$ fraction vs $p_\perp$ $0 < |y| < 0.9$
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d22-x01-y01
Title=Non-prompt $J/\psi$ fraction vs $p_\perp$ $0.9 < |y| < 1.2$
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d23-x01-y01
Title=Non-prompt $J/\psi$ fraction vs $p_\perp$ $1.2 < |y| < 1.6$
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d24-x01-y01
Title=Non-prompt $J/\psi$ fraction vs $p_\perp$ $1.6 < |y| < 2.1$
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d25-x01-y01
Title=Non-prompt $J/\psi$ fraction vs $p_\perp$ $2.1 < |y| < 2.4$
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT


BEGIN PLOT /CMS_2012_I944755/d26-x01-y01
Title=Non-prompt $\psi(2S)$ fraction vs $p_\perp$ $0 < |y| < 1.2$
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d27-x01-y01
Title=Non-prompt $\psi(2S)$ fraction vs $p_\perp$ $1.2 < |y| < 1.6$
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT
BEGIN PLOT /CMS_2012_I944755/d28-x01-y01
Title=Non-prompt $\psi(2S)$ fraction vs $p_\perp$ $1.6 < |y| < 2.4$
XLabel=$p_\perp$ [GeV]
YLabel=Non-prompt fraction
LogY=0
END PLOT