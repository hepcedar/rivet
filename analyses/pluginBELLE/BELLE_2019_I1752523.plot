BEGIN PLOT /BELLE_2019_I1752523
LogY=0
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[1,2]-x0[1,2,3,4,5,6]-y01
YLabel=$A^{UL}_{12}$ [\%]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[3,5,6,8]
YLabel=$A^{\pi^0}_{12}$ [\%]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d[10,12]
YLabel=$A^{\pi^0}_{12}$ [\%]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[4,7]
YLabel=$A^{\eta}_{12}$ [\%]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[4,7]-x11
YLabel=$A^{\eta}_{12}$ [\%]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[1,2]-x0[1,2,3,4,5,6]-y02
YLabel=$A^{UC}_{12}$ [\%]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[1,2]-x09
YLabel=$A^{UC}_{12}$ [\%]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[1,6,7,8]
XLabel=$p_{T2}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[1,6,7,8]-x01
XLabel=$p_{T1}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[2,3,4,5]
XLabel=$z_{2}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d0[2,3,4,5]-x01
XLabel=$z_{1}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d09
XLabel=$p_{T1}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d[10,11,12]
XLabel=$p_{T1}$ [GeV]
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x01-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $p_{T1}$
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x01-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T1}$
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x02-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $p_{T2}$ ($p_{T1}<0.15$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x02-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T2}$ ($p_{T1}<0.15$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x03-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $p_{T2}$ ($0.15<p_{T1}<0.3$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x03-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T2}$ ($0.15<p_{T1}<0.3$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x04-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $p_{T2}$ ($0.3<p_{T1}<0.5$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x04-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T2}$ ($0.3<p_{T1}<0.5$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x05-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $p_{T2}$ ($0.5<p_{T1}<3$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d01-x05-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T2}$ ($0.5<p_{T1}<3$\,GeV)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d02-x01-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $z_{1}$
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x01-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $z_{1}$
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x02-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $z_2$ ($0.1<z_1<0.2$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x02-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $z_2$ ($0.1<z_1<0.2$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x03-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $z_2$ ($0.2<z_1<0.3$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x03-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $z_2$ ($0.2<z_1<0.3$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x04-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $z_2$ ($0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x04-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $z_2$ ($0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x05-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $z_2$ ($0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x05-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $z_2$ ($0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x06-y01
Title=Charged pion asymmetry $A^{UL}_{12}$ vs. $z_2$ ($0.7<z_1<1$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d02-x06-y02
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $z_2$ ($0.7<z_1<1$)
END PLOT


BEGIN PLOT /BELLE_2019_I1752523/d03-x01-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_{1}$
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d03-x02-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_2$ ($0.1<z_1<0.2$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d03-x03-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_2$ ($0.2<z_1<0.3$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d03-x04-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_2$ ($0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d03-x05-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_2$ ($0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d03-x06-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_2$ ($0.7<z_1<1$)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d04-x01-y01
Title=$\eta$ pion asymmetry $A^{\eta}_{12}$ vs. $z_{1}$
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d04-x02-y01
Title=$\eta$ pion asymmetry $A^{\eta}_{12}$ vs. $z_2$ ($0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d04-x03-y01
Title=$\eta$ pion asymmetry $A^{\eta}_{12}$ vs. $z_2$ ($0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d04-x04-y01
Title=$\eta$ pion asymmetry $A^{\eta}_{12}$ vs. $z_2$ ($0.7<z_1<1$)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d04-x01-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_{1}$ ($z>0.3$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d05-x02-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_2$ ($z>0.3$, $0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d05-x03-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_2$ ($z>0.3$, $0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d05-x04-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $z_2$ ($z>0.3$, $0.7<z_1<1$)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d06-x01-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d06-x02-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T2}$ ($p_{T1}<0.15$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d06-x03-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T2}$ ($0.15<p_{T1}<0.3$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d06-x04-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T2}$ ($0.3<p_{T1}<0.5$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d06-x05-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T2}$ ($0.5<p_{T1}<3$\,GeV)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d07-x01-y01
Title=$\eta$ asymmetry $A^{\eta}_{12}$ vs. $p_{T1}$
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d07-x02-y01
Title=$\eta$ asymmetry $A^{\eta}_{12}$ vs. $p_{T2}$ ($p_{T1}<0.15$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d07-x03-y01
Title=$\eta$ asymmetry $A^{\eta}_{12}$ vs. $p_{T2}$ ($0.15<p_{T1}<0.3$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d07-x04-y01
Title=$\eta$ asymmetry $A^{\eta}_{12}$ vs. $p_{T2}$ ($0.3<p_{T1}<0.5$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d07-x05-y01
Title=$\eta$ asymmetry $A^{\eta}_{12}$ vs. $p_{T2}$ ($0.5<p_{T1}<3$\,GeV)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d08-x01-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$ ($z>0.3$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d08-x02-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T2}$ ($z>0.3$, $p_{T1}<0.15$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d08-x03-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T2}$ ($z>0.3$, $0.15<p_{T1}<0.3$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d08-x04-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T2}$ ($z>0.3$, $0.3<p_{T1}<0.5$\,GeV)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d08-x05-y01
Title=$\pi^0$ asymmetry $A^{\pi^0}_{12}$ vs. $p_{T2}$ ($z>0.3$, $0.5<p_{T1}<3$\,GeV)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d09-x01-y01
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T1}$ ($0.2<z_1<0.3$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d09-x02-y01
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T1}$ ($0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d09-x03-y01
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T1}$ ($0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d09-x04-y01
Title=Charged pion asymmetry $A^{UC}_{12}$ vs. $p_{T1}$ ($0.7<z_1<1$)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d10-x01-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$ ($0.2<z_1<0.3$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d10-x02-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$ ($0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d10-x03-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$ ($0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d10-x04-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$ ($0.7<z_1<1$)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d11-x01-y01
Title=$\eta$ pion asymmetry $A^{\eta}_{12}$ vs. $p_{T1}$ ($0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d11-x02-y01
Title=$\eta$ pion asymmetry $A^{\eta}_{12}$ vs. $p_{T1}$ ($0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d11-x03-y01
Title=$\eta$ pion asymmetry $A^{\eta}_{12}$ vs. $p_{T1}$ ($0.7<z_1<1$)
END PLOT

BEGIN PLOT /BELLE_2019_I1752523/d12-x01-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$ ($z>0.3$, $0.3<z_1<0.5$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d12-x02-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$ ($z>0.3$, $0.5<z_1<0.7$)
END PLOT
BEGIN PLOT /BELLE_2019_I1752523/d12-x03-y01
Title=$\pi^0$ pion asymmetry $A^{\pi^0}_{12}$ vs. $p_{T1}$ ($z>0.3$, $0.7<z_1<1$)
END PLOT
