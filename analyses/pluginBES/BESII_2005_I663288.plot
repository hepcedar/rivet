BEGIN PLOT /BESII_2005_I663288/d01-x01-y01
Title=$\pi^+\pi^-$ mass in $J/\psi\to\phi \pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma\text{d}m_{\pi^+\pi^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2005_I663288/d01-x01-y02
Title=$\phi \pi$ mass in $J/\psi\to\phi \pi^+\pi^-$
XLabel=$m_{\phi \pi}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma\text{d}m_{\phi \pi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /BESII_2005_I663288/d02-x01-y01
Title=$K^+K^-$ mass in $J/\psi\to\phi K^+K^-$
XLabel=$m_{K^+K^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma\text{d}m_{K^+K^-}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2005_I663288/d02-x01-y02
Title=$\phi K$ mass in $J/\psi\to\phi K^+K^-$
XLabel=$m_{\phi K}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma\text{d}m_{\phi K}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
