BEGIN PLOT /BELLE_2010_I841003/d01-x01-y01
Title=$\omega J/\psi$ mass in $\gamma\gamma\to \omega J/\psi$
XLabel=$m_{\omega J/\psi}$ [GeV]
YLabel=$1/\sigma\text{d}\sigma/\text{d}m_{\omega J/\psi}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
