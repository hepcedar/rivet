# BEGIN PLOT /ATLAS_2011_I919017/d0[1-4]
XLabel=Charged jet $p_\perp$ [GeV]
YLabel=$\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [$\mu$b/GeV]
FullRange=1
RatioPlotYMax=2.0
RatioPlotYMin=0.5
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d01-x01-y01
Title=Charged jet cross section vs. $p_\perp$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d02-x01-y01
Title=Charged jet cross section vs. $p_\perp$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d03-x01-y01
Title=Charged jet cross section vs. $p_\perp$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d04-x01-y01
Title=Charged jet cross section vs. $p_\perp$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9)
# END PLOT


# BEGIN PLOT /ATLAS_2011_I919017/d0[5-9]
XLabel=$N$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}N$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I919017/d[10-29]
XLabel=$N$
YLabel=$1/\sigma \, \mathrm{d}\sigma/\mathrm{d}N$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d05-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d06-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d07-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d08-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d09-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d10-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d11-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d12-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d13-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d14-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d15-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d16-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d17-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d18-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d19-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d20-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d21-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d22-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d23-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d24-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d25-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d26-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d27-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d28-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d29-x01-y01
Title=Charged jet multiplicity (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 24.0-40.0)
# END PLOT


# BEGIN PLOT /ATLAS_2011_I919017/d[30-54]
XLabel=Charged particle $z$
YLabel=$1/N_\mathrm{jet} \, \mathrm{d}N/\mathrm{d}z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d30-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d31-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d32-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d33-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d34-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d35-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d36-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d37-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d38-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d39-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d40-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d41-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d42-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d43-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d44-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d45-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d46-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d47-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d48-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d49-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d50-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d51-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d52-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d53-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d54-x01-y01
Title=Charged jet $z$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 24.0-40.0)
# END PLOT


# BEGIN PLOT /ATLAS_2011_I919017/d[55-79]
XLabel=Charged particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=$1/N_\mathrm{jet} \, \mathrm{d}N/\mathrm{d}p_\perp^\mathrm{rel}$ [GeV$^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d55-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d56-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d57-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d58-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d59-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d60-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d61-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d62-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d63-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d64-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d65-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d66-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d67-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d68-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d69-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d70-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d71-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d72-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d73-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d74-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d75-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d76-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d77-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d78-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d79-x01-y01
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d[80-99]
XLabel=Charged Particle $r$
# END PLOT
# BEGIN PLOT /ATLAS_2011_I919017/d[100-104]
XLabel=Charged Particle $r$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d80-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d81-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d82-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d83-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d84-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-1.9, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d85-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d86-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d87-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d88-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d89-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.0-0.5, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d90-x01-y10
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d91-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d92-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d93-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d94-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 0.5-1.0, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d95-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d96-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d97-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d98-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d99-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.0-1.5, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d100-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d101-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d102-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d103-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d104-x01-y01
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.4, $y$ 1.5-1.9, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle r
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d01-x01-y02
Title=Charged jet cross section vs. $p_\perp$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5)
YLabel=d$\sigma$/d$p_\perp$d$y$ [$\mu$b/GeV]
XLabel=Charged Jet $p_\perp$ [GeV]
FullRange=1
RatioPlotYMax=2.0
RatioPlotYMin=0.5
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d02-x01-y02
Title=Charged jet cross section vs. $p_\perp$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0)
YLabel=d$\sigma$/d$p_\perp$d$y$ [$\mu$b/GeV]
XLabel=Charged Jet $p_\perp$ [GeV]
FullRange=1
RatioPlotYMax=2.0
RatioPlotYMin=0.5
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d03-x01-y02
Title=Charged jet cross section vs. $p_\perp$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5)
YLabel=d$\sigma$/d$p_\perp$d$y$ [$\mu$b/GeV]
XLabel=Charged Jet $p_\perp$ [GeV]
FullRange=1
RatioPlotYMax=2.0
RatioPlotYMin=0.5
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d04-x01-y02
Title=Charged jet cross section vs. $p_\perp$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9)
YLabel=d$\sigma$/d$p_\perp$d$y$ [$\mu$b/GeV]
XLabel=Charged Jet $p_\perp$ [GeV]
FullRange=1
RatioPlotYMax=2.0
RatioPlotYMin=0.5
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d05-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 4.0-6.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d06-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 6.0-10.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d07-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 10.0-15.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d08-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 15.0-24.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d09-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 24.0-40.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d10-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 4.0-6.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d11-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 6.0-10.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d12-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 10.0-15.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d13-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 15.0-24.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d14-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 24.0-40.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d15-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 4.0-6.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d16-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 6.0-10.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d17-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 10.0-15.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d18-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 15.0-24.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d19-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 24.0-40.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d20-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 4.0-6.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d21-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 6.0-10.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d22-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 10.0-15.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d23-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 15.0-24.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d24-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 24.0-40.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d25-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 4.0-6.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d26-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 6.0-10.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d27-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 10.0-15.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d28-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 15.0-24.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d29-x01-y02
Title=Charged jet multiplicity (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 24.0-40.0)
XLabel=N
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d30-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d31-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d32-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d33-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d34-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d35-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d36-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d37-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d38-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d39-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d40-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d41-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d42-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d43-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d44-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d45-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d46-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d47-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d48-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d49-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d50-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d51-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d52-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d53-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d54-x01-y02
Title=Charged jet $z$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $z$
YLabel=(1/Njet)dN/d$z$
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d55-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d56-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d57-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d58-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d59-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d60-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d61-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d62-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d63-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d64-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d65-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d66-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d67-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d68-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d69-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d70-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d71-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d72-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d73-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d74-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d75-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 4.0-6.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d76-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 6.0-10.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d77-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 10.0-15.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d78-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 15.0-24.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d79-x01-y02
Title=Charged jet $p_\perp^\mathrm{rel}$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 24.0-40.0)
XLabel=Charged Particle $p_\perp^\mathrm{rel}$ [GeV]
YLabel=(1/Njet)dN/d$p_\perp^\mathrm{rel}$ [$\mathrm{GeV}^{-1}$]
# END PLOT



# BEGIN PLOT /ATLAS_2011_I919017/d80-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d81-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d82-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d83-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d84-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-1.9, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d85-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d86-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d87-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d88-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d89-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.0-0.5, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d90-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d91-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d92-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d93-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d94-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 0.5-1.0, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d95-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d96-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d97-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d98-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d99-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.0-1.5, $p_\perp$ 24.0-40.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d100-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 4.0-6.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d101-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 6.0-10.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d102-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 10.0-15.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d103-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 15.0-24.0)
# END PLOT

# BEGIN PLOT /ATLAS_2011_I919017/d104-x01-y02
Title=Charged jet $\rho_\mathrm{ch}(r)$ (anti-$k_t$, R = 0.6, $y$ 1.5-1.9, $p_\perp$ 24.0-40.0)
# END PLOT
