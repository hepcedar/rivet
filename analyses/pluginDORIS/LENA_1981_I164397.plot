BEGIN PLOT /LENA_1981_I164397/d03-x01-y01
Title=Average charged particle multiplicity
XLabel=$\sqrt{s}$ GeV
YLabel=$\langle N_{\mathrm{charged}}\rangle$
LogY=0
ConnectGaps=1
XCustomMajorTicks=1 $7.35-7.49$, 2 $8.629-9.142$ 3 $9.15-9.41$ 4 $\Upsilon(1S)$(off) 5 $\Upsilon(2S)$(off) 6 $\Upsilon(1S)$(on) 7 $\Upsilon(2S)$(on)
END PLOT
BEGIN PLOT /LENA_1981_I164397/d04
XLabel=$T^\prime$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}T^\prime$
LogY=0
END PLOT
BEGIN PLOT /LENA_1981_I164397/d04-x01-y01
Title=$T^\prime$ distribution in continuum near near $\Upsilon(1S)$
END PLOT
BEGIN PLOT /LENA_1981_I164397/d04-x01-y02
Title=$T^\prime$ distribution in continuum near near $\Upsilon(2S)$
END PLOT
BEGIN PLOT /LENA_1981_I164397/d04-x01-y03
Title=$T^\prime$ distribution in $\Upsilon(1S)$ decays
END PLOT
BEGIN PLOT /LENA_1981_I164397/d04-x01-y04
Title=$T^\prime$ distribution in $\Upsilon(2S)$ decays
END PLOT
