BEGIN PLOT /BELLE_2004_I653673/d01
XLabel=$\sqrt{s}$ [GeV]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d0[1,2]
YLabel=$\sigma$ [fb]
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03
YLabel=$\alpha$
LogY=0
YMin=-1
YMax=1
END PLOT

BEGIN PLOT /BELLE_2004_I653673/d01-x01-y01
Title=Cross section for $e^+e^-\to J/\psi \eta_c$ ($\eta_c\to>2$tracks)
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d01-x01-y02
Title=Cross section for $e^+e^-\to J/\psi \chi_{c0}$ ($\chi_{c0}\to>2$tracks)
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d01-x01-y03
Title=Cross section for $e^+e^-\to J/\psi \eta_c(2S)$ ($\eta_c(2S)\to>2$tracks)
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d02-x01-y01
Title=Cross section for $e^+e^-\to \psi(2S) \eta_c$ ($\eta_c\to>0$tracks)
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d02-x01-y02
Title=Cross section for $e^+e^-\to \psi(2S) \chi_{c0}$ ($\chi_{c0}\to>0$tracks)
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d02-x01-y03
Title=Cross section for $e^+e^-\to \psi(2S) \eta_c(2S)$ ($\eta_c(2S)\to>0$tracks)
END PLOT

BEGIN PLOT /BELLE_2004_I653673/d03-x01-y01
Title=$\alpha_\text{prod}$ for $e^+e^-\to J/\psi \eta_c$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03-x01-y02
Title=$\alpha_\text{prod}$ for $e^+e^-\to J/\psi \chi_{c0}$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03-x01-y03
Title=$\alpha_\text{prod}$ for $e^+e^-\to J/\psi \eta_c(2S)$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03-x02-y01
Title=$\alpha_\text{hel}$ for $e^+e^-\to J/\psi \eta_c$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03-x02-y02
Title=$\alpha_\text{hel}$ for $e^+e^-\to J/\psi \chi_{c0}$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03-x02-y03
Title=$\alpha_\text{hel}$ for $e^+e^-\to J/\psi \eta_c(2S)$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03-x03-y01
Title=$\alpha_\text{hel}$ for $e^+e^-\to J/\psi \eta_c$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03-x03-y02
Title=$\alpha_\text{hel}$ for $e^+e^-\to J/\psi \chi_{c0}$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d03-x03-y03
Title=$\alpha_\text{hel}$ for $e^+e^-\to J/\psi \eta_c(2S)$
END PLOT

BEGIN PLOT /BELLE_2004_I653673/d04
XLabel=$\cos\theta$
YLabel=$1/\sigma\text{d}\sigma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d04-x01-y01
Title=Production angle for  $e^+e^-\to J/\psi \eta_c$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d04-x01-y02
Title=Production angle for  $e^+e^-\to J/\psi \chi_{c0}$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d04-x01-y03
Title=Production angle for  $e^+e^-\to J/\psi \eta_c(2S)$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d04-x02-y01
Title=Helicity angle for  $e^+e^-\to J/\psi \eta_c$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d04-x02-y02
Title=Helicity angle for  $e^+e^-\to J/\psi \chi_{c0}$
END PLOT
BEGIN PLOT /BELLE_2004_I653673/d04-x02-y03
Title=Helicity angle for  $e^+e^-\to J/\psi \eta_c(2S)$
END PLOT
