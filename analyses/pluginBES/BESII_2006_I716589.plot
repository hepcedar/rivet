BEGIN PLOT /BESII_2006_I716589/d01-x01-y01
Title=$p\pi^-(\bar{p}\pi^+)$ mass distribution in $\psi(2S)\to p\bar{n}\pi^-$+c.c.
XLabel=$m_{p\pi^-(\bar{p}\pi^+)}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{p\pi^-(\bar{p}\pi^+)}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I716589/d01-x01-y02
Title=$\bar{n}\pi^-(n\pi^+)$ mass distribution in $\psi(2S)\to p\bar{n}\pi^-$+c.c.
XLabel=$m_{\bar{n}\pi^-(n\pi^+)}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{n}\pi^-(n\pi^+)}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I716589/dalitz_1
Title=Dalitz plot for  $\psi(2S)\to p\bar{n}\pi^-$
XLabel=$m^2_{p\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{n}\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{p\pi^-}/{\rm d}m^2_{\bar{n}\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
BEGIN PLOT /BESII_2006_I716589/dalitz_2
Title=Dalitz plot for  $\psi(2S)\to n\bar{p}\pi^+$
XLabel=$m^2_{\bar{p}\pi^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{n\pi^+}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\bar{p}\pi^+}/{\rm d}m^2_{n\pi^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
