BEGIN PLOT /BESIII_2023_I2633025/d0
XLabel=$X$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}X/\mathrm{d}Y$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y01
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-1.0<Y<-0.9$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y02
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.9<Y<-0.8$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y03
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.8<Y<-0.7$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y04
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.7<Y<-0.6$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y05
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.6<Y<-0.5$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y06
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.5<Y<-0.4$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y07
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.4<Y<-0.3$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y08
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.3<Y<-0.2$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y09
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.2<Y<-0.1$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y10
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($-0.1<Y< 0.0$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y11
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.0<Y<0.1$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y12
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.1<Y<0.2$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y13
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.2<Y<0.3$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y14
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.3<Y<0.4$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y15
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.4<Y<0.5$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y16
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.5<Y<0.6$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y17
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.6<Y<0.7$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y18
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.7<Y<0.8$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d03-x01-y19
Title=$X$ distribution for $\eta\to\pi^+\pi^-\pi^0$ ($0.8<Y<0.9$)
END PLOT

BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y01
Title=$X$ distribution for $\eta\to3\pi^0$ ($-1.0<Y<-0.9$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y02
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.9<Y<-0.8$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y03
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.8<Y<-0.7$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y04
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.7<Y<-0.6$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y05
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.6<Y<-0.5$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y06
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.5<Y<-0.4$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y07
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.4<Y<-0.3$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y08
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.3<Y<-0.2$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y09
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.2<Y<-0.1$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y10
Title=$X$ distribution for $\eta\to3\pi^0$ ($-0.1<Y< 0.0$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y11
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.0<Y<0.1$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y12
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.1<Y<0.2$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y13
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.2<Y<0.3$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y14
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.3<Y<0.4$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y15
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.4<Y<0.5$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y16
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.5<Y<0.6$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y17
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.6<Y<0.7$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y18
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.7<Y<0.8$)
END PLOT
BEGIN PLOT /BESIII_2023_I2633025/d04-x01-y19
Title=$X$ distribution for $\eta\to3\pi^0$ ($0.8<Y<0.9$)
END PLOT
