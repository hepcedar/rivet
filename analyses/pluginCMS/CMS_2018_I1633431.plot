BEGIN PLOT /CMS_2018_I1633431/.*
FullRange=1
LogY=1
END PLOT

BEGIN PLOT /CMS_2018_I1633431/d01-x01-y01
Title=$J/\psi$ transverse momentum $0 < |y| < 0.3$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d01-x01-y02
Title=$J/\psi$ transverse momentum $0.3 < |y| < 0.6$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d01-x01-y03
Title=$J/\psi$ transverse momentum $0.6 < |y| < 0.9$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d01-x01-y04
Title=$J/\psi$ transverse momentum $0.9 < |y| < 1.2$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d01-x01-y05
Title=$J/\psi$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=Br($J/\psi \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /CMS_2018_I1633431/d02-x01-y01
Title=$\psi(2S)$ transverse momentum $0 < |y| < 0.3$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d02-x01-y02
Title=$\psi(2S)$ transverse momentum $0.3 < |y| < 0.6$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d02-x01-y03
Title=$\psi(2S)$ transverse momentum $0.6 < |y| < 0.9$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d02-x01-y04
Title=$\psi(2S)$ transverse momentum $0.9 < |y| < 1.2$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d02-x01-y05
Title=$\psi(2S)$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\psi(2S)}_\perp$ [GeV]
YLabel=Br($\psi(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /CMS_2018_I1633431/d03-x01-y01
Title=$\Upsilon(1S)$ transverse momentum $0 < |y| < 0.6$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d03-x01-y02
Title=$\Upsilon(1S)$ transverse momentum $0.6 < |y| < 1.2$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d03-x01-y03
Title=$\Upsilon(1S)$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon(1S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(1S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /CMS_2018_I1633431/d04-x01-y01
Title=$\Upsilon(2S)$ transverse momentum $0 < |y| < 0.6$
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d04-x01-y02
Title=$\Upsilon(2S)$ transverse momentum $0.6 < |y| < 1.2$
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d04-x01-y03
Title=$\Upsilon(2S)$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon(2S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(2S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /CMS_2018_I1633431/d05-x01-y01
Title=$\Upsilon(3S)$ transverse momentum $0 < |y| < 0.6$
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d05-x01-y02
Title=$\Upsilon(3S)$ transverse momentum $0.6 < |y| < 1.2$
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d05-x01-y03
Title=$\Upsilon(3S)$ transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon(3S)}_\perp$ [GeV]
YLabel=Br($\Upsilon(3S) \rightarrow \mu^{+}\mu^{-}$)$\sigma\mathrm{d}^2\sigma/\mathrm{d}p_\perp\mathrm{d}y$ [pb/GeV]
END PLOT

BEGIN PLOT /CMS_2018_I1633431/d11-x01-y01
Title=Ratio of $\psi(2S)$ to $J/\psi$ vs transverse momentum $0 < |y| < 1.2$
XLabel=$p^{J/\psi}_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT


BEGIN PLOT /CMS_2018_I1633431/d12-x01-y01
Title=Ratio of $\Upsilon(2S)$ to $\Upsilon(1S)$ vs transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon}_\perp$ [GeV]
YLabel=$R_{21}$
LogY=0
END PLOT
BEGIN PLOT /CMS_2018_I1633431/d12-x01-y02
Title=Ratio of $\Upsilon(3S)$ to $\Upsilon(1S)$ vs transverse momentum $0 < |y| < 1.2$
XLabel=$p^{\Upsilon}_\perp$ [GeV]
YLabel=$R_{31}$
LogY=0
END PLOT
