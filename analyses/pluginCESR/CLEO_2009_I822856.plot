BEGIN PLOT /CLEO_2009_I822856/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $D^0\to \pi^+\pi^0\omega$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
