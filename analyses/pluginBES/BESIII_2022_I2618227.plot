BEGIN PLOT /BESIII_2022_I2618227/d01-x01-y01
Title=$D_s^+\to \pi^+\pi^+\pi^-+X$ branching ratio
YLabel=$\mathcal{B}(D_s^+\to \pi^+\pi^+\pi^-+X)$   [$\%$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2618227/d02-x01-y01
Title=$D_s^+\to \pi^+\pi^+\pi^-+X$ branching ratio vs $m_{\pi^+\pi^+\pi^-}$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [GeV]
YLabel=$\text{d}\mathcal{B}(D_s^+\to \pi^+\pi^+\pi^-+X)/\text{d}m_{\pi^+\pi^+\pi^-}$   [$\%/\text{GeV}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2618227/d03-x01-y01
Title=$\pi^-$ momentum in $D_s^+\to \pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^-}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2618227/d03-x01-y02
Title=Harder $\pi^+$ momentum in $D_s^+\to \pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^+}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2618227/d03-x01-y03
Title=Softer $\pi^+$ momentum in $D_s^+\to \pi^+\pi^+\pi^-+X$
XLabel=$p_{\pi^+}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}p_{\pi^+}$   [$\text{GeV}^{-1}$]
LogY=0
END PLOT
