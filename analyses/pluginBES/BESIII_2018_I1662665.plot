BEGIN PLOT /BESIII_2018_I1662665/d01-x01-y01
Title=$\pi^0\eta$ mass distribution in $D^0\to \pi^0\eta\eta$
XLabel=$m_{\pi^0\eta}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^0\eta}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2018_I1662665/dalitz
Title=Dalitz plot forn $D^0\to \pi^0\eta\eta$
XLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^0\eta}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^0\eta}/{\rm d}m^2_{\pi^0\eta}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
