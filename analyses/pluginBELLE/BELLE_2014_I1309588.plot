BEGIN PLOT /BELLE_2014_I1309588/d01-x01-y01
Title=Cross section for $e^+e^-\to \pi^+\pi^-\pi^0 \chi_{b1}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\pi^0 \chi_{b1})$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1309588/d01-x01-y02
Title=Cross section for $e^+e^-\to \pi^+\pi^-\pi^0 \chi_{b2}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\pi^0 \chi_{b2})$ [pb]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2014_I1309588/d02-x01-y01
Title=Cross section for $e^+e^-\to \omega \chi_{b1}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \omega \chi_{b1})$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1309588/d02-x01-y02
Title=Cross section for $e^+e^-\to \omega \chi_{b2}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \omega \chi_{b2})$ [pb]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2014_I1309588/d03-x01-y01
Title=Cross section for $e^+e^-\to \pi^+\pi^-\pi^0(\text{non}-\omega) \chi_{b1}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\pi^0(\text{non}-\omega) \chi_{b1})$ [pb]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1309588/d03-x01-y02
Title=Cross section for $e^+e^-\to \pi^+\pi^-\pi^0(\text{non}-\omega) \chi_{b2}$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to \pi^+\pi^-\pi^0(\text{non}-\omega) \chi_{b2})$ [pb]
LogY=0
END PLOT
