BEGIN PLOT /MARKII_1988_I250899/d01-x01-y01
Title=Energy-energy correlation with $E_{\mathrm{CMS}}=29$ GeV (PEP5)
XLabel=$\theta$ [degrees]
YLabel=EEC [$\text{rad}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I250899/d02-x01-y01
Title=Asymmetry of the energy-energy correlation with $E_{\mathrm{CMS}}=29$ GeV  (PEP5)
XLabel=$\theta$ [degrees]
YLabel=AEEC [$\text{rad}^{-1}$]
FullRange=1
END PLOT

BEGIN PLOT /MARKII_1988_I250899/d03-x01-y01
Title=Energy-energy correlation with $E_{\mathrm{CMS}}=29$ GeV (upgrade)
XLabel=$\theta$ [degrees]
YLabel=EEC [$\text{rad}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I250899/d04-x01-y01
Title=Asymmetry of the energy-energy correlation with $E_{\mathrm{CMS}}=29$ GeV (upgrade)
XLabel=$\theta$ [degrees]
YLabel=AEEC [$\text{rad}^{-1}$]
FullRange=1
END PLOT

BEGIN PLOT /MARKII_1988_I250899/d05-x01-y01
Title=Energy-energy correlation with $E_{\mathrm{CMS}}=29$ GeV (combined)
XLabel=$\theta$ [degrees]
YLabel=EEC [$\text{rad}^{-1}$]
FullRange=1
END PLOT
BEGIN PLOT /MARKII_1988_I250899/d06-x01-y01
Title=Asymmetry of the energy-energy correlation with $E_{\mathrm{CMS}}=29$ GeV (combined)
XLabel=$\theta$ [degrees]
YLabel=AEEC [$\text{rad}^{-1}$]
FullRange=1
END PLOT
