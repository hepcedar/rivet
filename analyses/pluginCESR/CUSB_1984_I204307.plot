BEGIN PLOT /CUSB_1984_I204307/d01-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(1S)$ decay
XLabel=$x_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_\gamma\times10^2$
LogY=0
END PLOT
BEGIN PLOT /CUSB_1984_I204307/d02-x01-y01
Title=Direct Photon Spectrum in $\Upsilon(2S)$ decay
XLabel=$x_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}x_\gamma\times10^2$
LogY=0
END PLOT